<?php 
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);

$CourseId = $_GET['courseid'];
$courseChapters = $this->db->query("SELECT * FROM chapters WHERE status = 1  AND course_id = '$CourseId'  ORDER BY 1 ASC")->result();
?>
<nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <center><img src="<?php echo base_url() ?>assets/course_images/<?php echo $courseImage ?>" height="150px;" width="180px;"></center>
                                </li>
                                
                                <?php
                                foreach($courseChapters as $getcourseChapters){ 
                                $chapterName = $getcourseChapters->chapter_name;
                                $ChapterId = $getcourseChapters->id;
                                
                                
                                ?>
                                
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                            <span class="pcoded-mtext"><?php echo $chapterName ?></span> 
                                    <ul class="pcoded-submenu">
                                        
                                        
                                        <?php 
                                        $chapterItems = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND chapterid = '$ChapterId' ORDER BY 1 ASC")->result();
                                        foreach($chapterItems as $getchapterItems){ 
                                        $subChapterName = $getchapterItems->subchapter_name;
                                        ?>
                                        <li class="">
                                                <a href="<?php echo base_url() ?>EditItem?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $getcourseChapters->id ?>&itemid=<?php echo $getchapterItems->id ?>">
                                                    <span class="pcoded-mtext"><?php echo $subChapterName ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>EditItem/editcitem?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $getcourseChapters->id ?>&itemid=<?php echo $getchapterItems->id ?>">
                                                    <span class="pcoded-mtext">Delete</span>
                                                </a>
                                            </li>
                                        <li class="">
                                                <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $getcourseChapters->id ?>">
                                                    <span class="pcoded-mtext">Add an Item</span>
                                                </a>
                                            </li>
                                    </ul>
                                </li>
                                <!--<li class="">
                                        
                                            &nbsp;
                                            <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>" >
                                            <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                            <span class="pcoded-mtext"><?php echo $chapterName ?></span> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            </a>

                                            
                                        
                                </li>-->
                                <?php } ?>
                                <?php 
                                        $chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND chapterid = 0 ORDER BY 1 ASC")->result();
                                     
                                        foreach($chapterItems_1 as $getchapterItems_1){ 
                                        $subChapterName_1 = $getchapterItems_1->subchapter_name;
                                        ?>
                                
                                <li class="">
                                    <a href="<?php echo base_url() ?>EditItem?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo 0 ?>&itemid=<?php echo $getchapterItems_1->id ?>">
                                        <?php if($getchapterItems_1->type == 'Live Class') {?>
                                        <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Pdf' && $getchapterItems_1->sub_type == 'upload'){ ?>
                                        <span class="pcoded-micon"><i class="feather icon-book"></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Pdf' && $getchapterItems_1->sub_type == 'public_url'){ ?>
                                        <span class="pcoded-micon"><i class="feather icon-link"></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Heading'){ ?>
                                        <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                        <?php } ?> 
                                        
                                        <span class="pcoded-mtext"><?php echo $subChapterName_1 ?></span>
                                    </a>
                                </li>
                                <?php } ?>
                                
                                
                                
                                <li class="active">
                                    <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>&chapterid=nil">
                                        <span class="pcoded-micon"><i class="feather icon-plus"></i></span>
                                        <span class="pcoded-mtext">Add New Chapter</span>
                                    </a>
                                </li>
                                
                                <li class="active">
                                    <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>&chapterid=0">
                                        <span class="pcoded-micon"><i class="feather icon-plus"></i></span>
                                        <span class="pcoded-mtext">Add New Item</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?php echo base_url() ?>ICourses">
                                        <span class="pcoded-micon"><i class=" feather icon-arrow-left"></i></span>
                                        <span class="pcoded-mtext">Back</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>