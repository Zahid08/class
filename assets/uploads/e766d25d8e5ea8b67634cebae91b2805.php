<?php 
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);
$courseChapters = $_GET['chapterid'];
$CourseId = $_GET['courseid'];
$courseChapters = $this->db->query("SELECT * FROM chapters WHERE status = 1  AND course_id = '$CourseId'  ORDER BY 1 ASC")->result();
$liveTest = $this->db->query("SELECT * FROM live_test WHERE status = 1  AND course_id = '$CourseId' ORDER BY 1 ASC")->result();
$quizTest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1  AND course_id = '$CourseId' ORDER BY 1 ASC")->result();

?>
<nav class="pcoded-navbar" style="width:25%;height:100%">
                        <div class="pcoded-inner-navbar main-menu" style="width:100%;height:100%">
                            <div class="pcoded-navigatio-lavel">Navigation</div>
                            <ul class="pcoded-item pcoded-center-item">
                               <li class="">
                                   
                                 <center>   <img src="<?php echo base_url() ?>assets/course_images/<?php echo $courseImage ?>"style="border:2px solid #000000 " height="190px;" width="95%;">
                                <br>
                                 <p class="text-white"><?php echo $courseTitle ?></p>
                                 </center> 
                                </li>
                                <li class="" class="active" >
                                    <a href="<?php echo base_url() ?>Courses">
                                        <span class="pcoded-micon"><i class=" feather icon-arrow-left" ></i></span>
                                        <span class="pcoded-mtext" >Back</span>
                                    </a>
                                </li>
                       
                                <?php
                                foreach($courseChapters as $getcourseChapters){ 
                                $chapterName = $getcourseChapters->chapter_name;
                                $ChapterId = $getcourseChapters->id;
                                
                                
                                ?>
                                
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                            <span class="pcoded-mtext"><?php echo $chapterName ?></span> 
                                    <ul class="pcoded-submenu">
                                        
                                        
                                        <?php 
                                        $chapterItems = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND chapterid = '$ChapterId' ORDER BY 1 ASC")->result();
                                        foreach($chapterItems as $getchapterItems){ 
                                        $subChapterName = $getchapterItems->subchapter_name;
                                        ?>
                                        <li class="">
                                                <a href="<?php echo base_url() ?>EditItem?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $getcourseChapters->id ?>&itemid=<?php echo $getchapterItems->id ?>">
                                                    <span class="pcoded-mtext"><?php echo $subChapterName ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        
                                            <li class="">
                                                <a href="#" onclick ="alert_del_chapters_msg('<?= $_GET['courseid'] ?>','<?= $getcourseChapters->id ?>','<?= $getchapterItems->id ?>')">
                                                <span class="pcoded-mtext">Delete</span>
                                                </a>
                                            </li>
                                            
                                        <li class="">
                                                <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $getcourseChapters->id ?>">
                                                    <span class="pcoded-mtext">Add an Item</span>
                                                </a>
                                            </li>
                                    </ul>
                                </li>
                                <!--<li class="">
                                        
                                            &nbsp;
                                            <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>" >
                                            <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                            <span class="pcoded-mtext"><?php echo $chapterName ?></span> 
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            </a>

                                            
                                        
                                </li>-->
                                <?php } ?>
                                <?php 
                                        $chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND chapterid = 0 ORDER BY 1 ASC")->result();
                                     
                                        foreach($chapterItems_1 as $getchapterItems_1){ 
                                        $subChapterName_1 = $getchapterItems_1->subchapter_name;
                                        ?>
                                
                                <li class="">
                                    <a href="<?php echo base_url() ?>EditItem?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo 0 ?>&itemid=<?php echo $getchapterItems_1->id ?>">
                                        <?php if($getchapterItems_1->type == 'Live Class') {?>
                                        <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Inbuilt Live Class') {?>
                                        <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Jitsi Live Class') {?>
                                        <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Pdf' && $getchapterItems_1->sub_type == 'upload'){ ?>
                                        <span class="pcoded-micon"><i class="feather icon-book" ></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Pdf' && $getchapterItems_1->sub_type == 'public_url'){ ?>
                                        <span class="pcoded-micon"><i class="feather icon-link" ></i></span>
                                        <?php }else if($getchapterItems_1->type == 'Heading'){ ?>
                                        <span class="pcoded-micon"><i class="feather icon-bookmark" ></i></span>
                                        <?php } ?> 
                                        
                                        <span class="pcoded-mtext" ><?php echo $subChapterName_1 ?></span>
                                    </a>
                                </li>
                                <?php } ?>
                                <?php 
                                         
                                        $liveTest = $this->db->query("SELECT * FROM live_test WHERE status = 1  AND course_id = '$CourseId' ORDER BY 1 ASC")->result();
                                     
                                        foreach($liveTest as $getliveTest){ 
                                        $liveTest = $getliveTest->title;
                                        
                                        ?>
                                
                                <li class="">
                                    <a href="<?php echo base_url() ?>Newlivetest?courseid=<?php echo $_GET['courseid'] ?>&id=<?php echo $getliveTest->id ?>">
                                       
                                        <span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
                                        <span class="pcoded-mtext" ><?php echo  $getliveTest->title?></span>
                                    </a>
                                </li>
                                <?php }?>
                               <?php 
                                         
                                        $quizTest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1  AND course_id = '$CourseId' ORDER BY 1 ASC")->result();
                                     
                                        foreach($quizTest as $getquizTest){ 
                                        $quizTest = $getquizTest->title;
                                        
                                        ?>
                                
                                <li class="">
                                    <a href="<?php echo base_url() ?>Newquiztest?courseid=<?php echo $_GET['courseid'] ?>&id=<?php echo $getquizTest->id ?>">
                                       
                                        <span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
                                        <span class="pcoded-mtext" ><?php echo  $getquizTest->title?></span>
                                    </a>
                                </li>
                                <?php }?>
                                
                                
                               <li class="col-lg-12 features" style="background-color:#d6d6c2;border:1px solid #80bfff;">
                                    
                                    <a href="<?php echo base_url()?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>&chapterid=nil">
                                        <span class="pcoded-micon"><i class="feather icon-plus" ></i></span>
                                        <span class="pcoded-mtext"  >Add New Chapter</span>
                                    </a>
                                   
                                </li>
                               
                                <li class="col-lg-12 features" style="background-color:#d6d6c2;border:1px solid #80bfff;">
                                    <a href="<?php echo base_url() ?>Newchapter?courseid=<?php echo $_GET['courseid'] ?>&chapterid=0">
                                        <span class="pcoded-micon"><i class="feather icon-plus" ></i></span>
                                        <span class="pcoded-mtext"  >Add New Item</span>
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </nav>
         	<script>
		    function alert_del_chapters_msg(courseId,chapterId,ItemId)
		    {
                swal({
                  title: "Are you sure?",
                  text: "You wants to delete this Chapter,Even Sub-chapters will get deleted !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willEdit) => {
                  if (willEdit) {
                    /*swal("Poof! Your Course has been deleted!", {
                      icon: "success",
                    });*/
                    location.href = '<?php echo base_url() ?>EditItem/editchapters?courseid='+courseId+'&chapterid='+chapterId+'&itemid='+ItemId;
                  } else {
                    swal("You haven't deleted the Chapter!");
                  }
                });
            }
		</script>