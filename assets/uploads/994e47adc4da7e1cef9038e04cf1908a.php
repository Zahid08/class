<?php
$totalTransactions = $this->db->query("SELECT count(1) as totaltransactions  FROM transactions  ")->result();
foreach($totalTransactions as $gettotalTransactions){
$totalTrans = $gettotalTransactions->totaltransactions;
}


$totalPaidTransactions=$this->db->query("SELECT count(1) as totalpaidtransactions FROM transactions t WHERE status='success' ")->result();
foreach($totalPaidTransactions as $gettotalPaidTransactions){
$totalPaidTrans = $gettotalPaidTransactions->totalpaidtransactions;
}

$failedTransactions=$totalTrans-$totalPaidTrans;

$totalSales=$this->db->query("SELECT SUM(amount) as totalsales FROM transactions WHERE status='success' ")->result();
foreach($totalSales as $gettotalSales){
$Sales = $gettotalSales->totalsales;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Adminty - Premium Admin Template by Colorlib </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    
    <!-- animation nifty modal window effects css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/component.css">
</head>

<body>
 <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <?php include("includes/header.php"); ?>

            
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <?php include("includes/sidenav.php"); ?>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-8">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Overview</h4>
                                                    <span>Sales Report</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <center><b style="color:green;"><?php echo $this->session->flashdata('smsg'); ?></b></center>
                                <!-- Page-header end -->
                                <div class="row">
                                <!-- statustic-card start -->
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-yellow text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Total Transaction</p>
                                                                <h4 class="m-b-0"><?php echo $totalTrans ;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-user f-50 text-c-yellow"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-green text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Paid Transaction</p>
                                                                <h4 class="m-b-0"><?php echo $totalPaidTrans ;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-credit-card f-50 text-c-green"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-pink text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Failed Transaction</p>
                                                                <h4 class="m-b-0"><?php echo $failedTransactions;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-book f-50 text-c-pink"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-blue text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Total Sales</p>
                                                                <h4 class="m-b-0"><?php echo $Sales;?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-shopping-cart f-50 text-c-blue"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <!-- statustic-card start -->
                                </div>
                                

                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <form action="<?php base_url('Salesreport/index') ?>" method="post">
                                        <div class="form-row">
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Date Range:</h4>
                                                <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                                    <span></span> <b class="caret"></b>
                                                </div>
                                            </div>            
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Status:</h4>
                                                <select name="status" class="form-control">
                                                    <option value='1'>Success</option>
                                                    <option value='2'>Failed</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Type:</h4>
                                                <select name="trans_type" class="form-control">
                                                    <option value='paid'>Paid</option>
                                                    <option value='free'>Free</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-8">
                                            </div>
                                            <div class="col-sm-1">
                                                <button name="search" type="submit"  class="btn btn-primary btn-outline-primary">Search</button>
                                            </div>
                                            </form>
                                            <div class="col-sm-1">
                                                <button type="button" class="btn btn-primary btn-outline-primary waves-effect md-trigger" data-modal="modal-8">Create</button>
                                                
                                                <div class="md-modal md-effect-8" id="modal-8">
                                                    
                                                                <div class="md-content" style="border:1px solid grey;border-radius:5px;">
                                                                    <h3>Add a Payment</h3>
                                                                    
                                                                    <div class="container">
                                                                        <button type="button" class=" md-close"><span aria-hidden="true">&times;</span></button>
                                                                      <?php  if(count($sales_report) > 0){
                                                                        
                                                                    ?>
                                                                    
                                                                    <form method="post" name="Mform" id="form" action="<?php base_url('Salesreport/index') ?>">
                                                                        <div class="form-group">
                                                                            <label>E-mail ID :</label>
                                                                            <select name="email_id" class="form-control" id="email_id" >
                                                                            <?php 
                                                                            foreach ($luser as $l){
                                                                            ?>
                                                                            <option value="<?php echo $l['email_id'] ; ?>"><?php echo $l['email_id'] ;  ?></option>
                                                                        <?php } ?>
                                                                             </select>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Course Name :</label>
                                                                            <select name="course_id" class="form-control" id="course" onchange="courseName()">
                                                                            <?php    foreach($sales_report as $sales){
                                                                               
                                                                            ?>
                                                                           
                                                                            <option placeholder="Choose a Value" value="<?php echo $sales['course_id'] ; ?>"><?php echo $sales['title'] ;  ?></option>
                                                                        <?php } }  ?>
                                                                             </select>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Payment Mode :</label>
                                                                            <select name="payment_mode" class="form-control"required>
                                                                            <option value='paid'>Paid</option>
                                                                            <option value='free'>Free</option>
                                                                            </select>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Amount :</label>
                                                                            <input type="number" class="form-control" id="amount" name="amount" required>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Transaction Code :</label>
                                                                            <input name="tran_code" id="tran_code" class="form-control" type="text" size="40">
                                                                            <input type="button" class="btn btn-sm btn-primary" value="Generate" onclick="transectioncode(4);" >
                                                                         </div>
                                                                       <div class="form-group">
                                                                            
                                                                            <button type="submit"  class="btn btn-primary" name="submit">Submit</button>
                                                                         </div>
                                                                        
                                                                    </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type = "clear" class="btn btn-primary btn-outline-primary">Clear</button>
                                            </div>
                                            <div class="col-sm-12">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                
                                                                <thead>
                                                                    <tr>
                                                                        <th>Date</th>
                                                                        <th>Status</th>
                                                                        <th>Amount</th>
                                                                        <th>Channel/OrderId/TaxId</th>
                                                                        <th>User</th>
                                                                        <th>Course</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    
                                                                    if(count($transaction) > 0 ){
                                                                    foreach($transaction as $t) {
                                                                    ?>
                                                                    <tr> 
                                                                        
                                                                        <td><?= $t['created_date']; ?></td>
                                                                        <?php if($t['status']=='success'){ $status="Success"; }else {$status="Failed";} ?>
                                                                        <td class="text-success"><?= $status; ?></td>
                                                                        <?php if(!empty($t['admin_comments'])) { ?>
                                                                        
                                                                        <?php } ?>
                                                                        <td>&#8377; <?= $t['amount']; ?></td>
                                                                        <td><?= $t['transaction_code']; ?></td>
                                                                        <?php foreach($user as $u){
                                                                        if($t['transaction_id'] == $u['id']) { ?>
                                                                        <td><?= $u['username']; ?></td>
                                                                        <?php } } ?>
                                                                        <?php foreach($sales_report as $s){
                                                                            if($t['course_id'] == $s['course_id']){
                                                                        ?>
                                                                        <td><?= $s['title']; ?></td>
                                                                        <?php } } ?>
                                                                        <td>
                                                                            <button type="button" class="btn btn-success btn-outline-success btn-icon md-trigger waves-effect" data-toggle="modal" data-target="#exampleModalLong"  data-placement="top" title="Comment"><i class="icofont icofont-comment"></i></button>
                                                                            <!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <?php }  ?>
                                                                </tbody>
                                                                <?php }  ?>
                                                               <?php if(count($search) > 0 ){
                                                                    foreach($search as $t) {
                                                                    ?>
                                                                    <tr> 
                                                                        
                                                                        <td><?= $t['created_date']; ?></td>
                                                                        <?php if($t['status']=='success'){ $status="Success"; }else {$status="Failed";} ?>
                                                                        <td class="text-success"><?= $status; ?></td>
                                                                        <?php if(!empty($t['admin_comments'])) { ?>
                                                                        <td><?= $t['admin_comments']; ?></td>
                                                                        <?php } else{ ?>
                                                                        <td>N/A</td>
                                                                        <?php } ?>
                                                                        <td>&#8377; <?= $t['amount']; ?></td>
                                                                        <td><?= $t['transaction_code']; ?></td>
                                                                        <?php foreach($user as $u){
                                                                        if($t['user_id'] == $u['id']) { ?>
                                                                        <td><?= $u['username']; ?></td>
                                                                        <?php } } ?>
                                                                        <?php foreach($sales_report as $s){
                                                                            if($t['course_id'] == $s['course_id']){
                                                                        ?>
                                                                        <td><?= $s['category']; ?></td>
                                                                        <?php } } ?>
                                                                        <td>
                                                                            <button class="btn btn-success btn-outline-success btn-icon md-trigger waves-effect" data-modal="modal-9" data-toggle="tooltip" data-placement="top" title="Comment"><i class="icofont icofont-comment"></i></button>
                                                                            <button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Refund"><i class="icofont icofont-match-review"></i></button>
                                                                             
                                                                          </td>
                                                                          <div class="md-modal md-effect-9" id="modal-9">
                                                                            <div class="md-content" style="border:1px solid grey;border-radius:5px;">
                                                                    <h3>Comment</h3>
                                                                    <div class="container">
                                                                        <?php $id =  $t['transaction_id'] ;  ?>
                                                                        <button type="button" class=" md-close"><span aria-hidden="true">&times;</span></button>
                                                                        <form method="post" name="form" action="<?php base_url('Salesreport/index')?>?id=<?php echo $id ?>"enctype="multipart/form-data">
                                                                            <div class="form-group">
																			<div class="form-row">
																	    <div class="col-sm-12 col-xl-10">
																		    <b>Comment:</b>
																			<textarea class="form-control max-textarea"  name="comment" maxlength="255" rows="4" placeholder="Type Comment Here..." 
																		
																			required></textarea>
																		</div>
																	</div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                            <button type="button"  class="btn btn-primary" name="comm_submit">Submit</button>
                                                                         </div>
                                                                        </form>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                        
                                                                        
                                                                    </tr>
                                                                    <?php }  ?>
                                                                </tbody>
                                                            <?php }  ?> 
                                                            </table>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



     <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
                <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script>
function courseName(){
    var x = document.getElementById("course").value;
    <?php foreach($course_price as $price) { ?>
    if(x == <?php echo $price['course_id'];?>){
        document.getElementById("amount").value = <?php echo $price['payable_price'];  ?>;
    }
    
    <?php } ?>
    
}
function transectioncode(length){
var chars = "1234567890";
    var code = "";
    var str = "COMPETIT";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        code += chars.charAt(i);
    }
    var oricode = str.concat(code);
    Mform.tran_code.value = oricode;
}
</script>
</body>

</html>