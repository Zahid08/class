<?php
/*========================Pau U Info===============================*/
$payuMerchantKey = $this->db->query("SELECT * FROM setting WHERE name = 'PayU_MerchantKey' ORDER BY 1 DESC")->result();
foreach($payuMerchantKey as $payuMerchantKeyData){
$MerchantKey = $payuMerchantKeyData->value;
}
$payuMerchantSalt = $this->db->query("SELECT * FROM setting WHERE name = 'PayU_MerchantSalt' ORDER BY 1 DESC")->result();
foreach($payuMerchantSalt as $payuMerchantKeySalt){
$MerchantSalt = $payuMerchantKeySalt->value;
}
/*===================================================================*/
/*========================Credit Info===============================*/
$credit = $this->db->query("SELECT * FROM setting WHERE name = 'Credit_Value' ORDER BY 1 DESC")->result();
foreach($credit as $creditData){
$creditValue = $creditData->value;
}

/*========================Device Count Info===============================*/
$devices = $this->db->query("SELECT * FROM setting WHERE name = 'Devices_Value' ORDER BY 1 DESC")->result();
foreach($devices as $devicesData){
$devicesValue = $devicesData->value;
}
/*===================================================================*/
/*===================Referred Credits Validity info=====================*/
$referredCreditsValidity = $this->db->query("SELECT * FROM setting WHERE name = 'Referred_Credits_Validity' ORDER BY 1 DESC")->result();
foreach($referredCreditsValidity as $referredCreditsValidityData){
$referredCreditsValidity = $referredCreditsValidityData->value;
}
/*======================================================================*/
/*===================Max Referral Count info=====================*/
$maxReferralCount = $this->db->query("SELECT * FROM setting WHERE name = 'Max_Referral_Count' ORDER BY 1 DESC")->result();
foreach($maxReferralCount as $maxReferralCountData){
$maxReferralCount = $maxReferralCountData->value;
}
/*======================================================================*/
/*===================Referrer Credits Signup info=====================*/
$referrerCreditsSignup = $this->db->query("SELECT * FROM setting WHERE name = 'Referrer_Credits_Signup' ORDER BY 1 DESC")->result();
foreach($referrerCreditsSignup as $referrerCreditsSignupData){
$referrerCreditsSignup = $referrerCreditsSignupData->value;
}
/*======================================================================*/
/*===================Referree Credits Signup info=====================*/
$referreeCreditsSignup = $this->db->query("SELECT * FROM setting WHERE name = 'Referree_Credits_Signup' ORDER BY 1 DESC")->result();
foreach($referreeCreditsSignup as $referreeCreditsSignupData){
$referreeCreditsSignup = $referreeCreditsSignupData->value;
}
/*======================================================================*/
/*===================Referrer Credits First Txn info=====================*/
$referrerCreditsFirstTxn = $this->db->query("SELECT * FROM setting WHERE name = 'Referrer_Credits_First_Txn' ORDER BY 1 DESC")->result();
foreach($referrerCreditsFirstTxn as $referrerCreditsFirstTxnData){
$referrerCreditsFirstTxn = $referrerCreditsFirstTxnData->value;
}
/*======================================================================*/
/*===================Referree Credits First Txn info=====================*/
$referreeCreditsFirstTxn = $this->db->query("SELECT * FROM setting WHERE name = 'Referree_Credits_First_Txn' ORDER BY 1 DESC")->result();
foreach($referreeCreditsFirstTxn as $referreeCreditsFirstTxnData){
$referreeCreditsFirstTxn = $referreeCreditsFirstTxnData->value;
}
/*======================================================================*/

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Setting </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="#">
		<meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		<!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		
		<!-- animation nifty modal window effects css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/component.css">
	</head>
	<body>
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<?php include("includes/sidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="main-body">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-8">
													<div class="page-header-title">
														<div class="d-inline">
															<h4>Setting</h4>
															<!--<span>Sales Report</span>-->
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<!-- Page-header end -->
										
										
										<!-- Page-body start -->
										<div class="page-body">
										    <!--================PayU============-->
											<form  method="post" action="<?php echo base_url(); ?>Setting/payu" autocomplete="off">
												<div class="card">
													<div class="card-body">
														<div class="card-title">
															<h4>PayU Integration</h4>
															<b style="color:green;"><?php echo $this->session->flashdata('payment_msg').'<br>'; ?></b>
															</br>
														</div>
														<div class="form-row">
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Merchant Key</h4>
																<input type="text" name="merchantkey" id="merchantkey" class="form-control" value="<?php echo $MerchantKey ?>" readonly>
															</div>
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Merchant Salt</h4>
																<input type="text" name="merchantsalt" id="merchantsalt" class="form-control" value="<?php echo $MerchantSalt ?>" readonly>
															</div>
															<div class="col-sm-12">
																<small>&nbsp;</small>
															</div>
															<div class="col-sm-1">
																<button type="button" name="edit_payment" id="edit_payment"  class="btn btn-primary btn-outline-primary">Edit</button>
																<button type="submit" name="save_payment" id="save_payment" onclick="alert('Are you sure, You want to Modify this Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											</form>
											<!--==============Credits===============-->
											<form  method="post" action="<?php echo base_url(); ?>Setting/credit" autocomplete="off">
												<div class="card">
													<div class="card-body">
														<div class="card-title">
															<h4>Credit</h4>
															<b style="color:green;"><?php echo $this->session->flashdata('credit_msg').'<br>'; ?></b>
															</br>
														</div>
														<div class="form-row">
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">1 Credit is: (In Rs.)</h4>
																<input type="text" name="credit" id="credit" class="form-control" value="<?php echo $creditValue ?>" readonly>
															</div>
															<div class="col-sm-12">
																<small>&nbsp;</small>
															</div>
															<div class="col-sm-1">
																<button type="button" name="edit_credit" id="edit_credit"  class="btn btn-primary btn-outline-primary">Edit</button>
																<button type="submit" name="save_credit" id="save_credit" onclick="alert('Are you sure, You want to Modify Credit Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											</form>
											<!--==============Max Devices===============-->
											<form  method="post" action="<?php echo base_url(); ?>Setting/devices" autocomplete="off">
												<div class="card">
													<div class="card-body">
														<div class="card-title">
															<h4>Devices</h4>
															<span style="color:grey;">Indicates max number of Devices Users can Login.</span>
															<b style="color:green;"><?php echo '<br>'. $this->session->flashdata('devices_msg').'<br>'; ?></b>
															</br>
														</div>
														<div class="form-row">
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Device Count:</h4>
																<input type="text" name="devices" id="devices" class="form-control" value="<?php echo $devicesValue ?>" readonly>
															</div>
															<div class="col-sm-12">
																<small>&nbsp;</small>
															</div>
															<div class="col-sm-1">
																<button type="button" name="edit_devices" id="edit_devices"  class="btn btn-primary btn-outline-primary">Edit</button>
																<button type="submit" name="save_devices" id="save_devices" onclick="alert('Are you sure, You want to Modify Device Count Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											</form>
											<!--==========================Refer and Earn==================================-->
											<form  method="post" action="<?php echo base_url(); ?>Setting/referandEarn" autocomplete="off">
												<div class="card">
													<div class="card-body">
														<div class="card-title">
															<h4>Refer & Earn</h4>
															<b style="color:green;"><?php echo $this->session->flashdata('referandEarn_msg').'<br>'; ?></b>
															</br>
														</div>
														<div class="form-row">
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Referred Credits Validity</h4>
																<input type="text" name="referredCreditsValidity" id="referredCreditsValidity" class="form-control" value="<?php echo $referredCreditsValidity ?>" readonly>
															</div>
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Max Referral Count</h4>
																<input type="text" name="maxReferralCount" id="maxReferralCount" class="form-control" value="<?php echo $maxReferralCount ?>" readonly>
															</div>
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Referrer Credits Signup</h4>
																<input type="text" name="referrerCreditsSignup" id="referrerCreditsSignup" class="form-control" value="<?php echo $referrerCreditsSignup ?>" readonly>
															</div>
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Referree Credits Signup</h4>
																<input type="text" name="referreeCreditsSignup" id="referreeCreditsSignup" class="form-control" value="<?php echo $referreeCreditsSignup ?>" readonly>
															</div>
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Referrer Credits First Txn</h4>
																<input type="text" name="referrerCreditsFirstTxn" id="referrerCreditsFirstTxn" class="form-control" value="<?php echo $referrerCreditsFirstTxn ?>" readonly>
															</div>
															<div class="col-sm-12 col-xl-4 m-b-30">
																<h4 class="sub-title">Referree Credits First Txn</h4>
																<input type="text" name="referreeCreditsFirstTxn" id="referreeCreditsFirstTxn" class="form-control" value="<?php echo $referreeCreditsFirstTxn ?>" readonly>
															</div>
															<div class="col-sm-12">
																<small>&nbsp;</small>
															</div>
															<div class="col-sm-1">
																<button type="button" name="edit_referandEarn" id="edit_referandEarn"  class="btn btn-primary btn-outline-primary">Edit</button>
																<button type="submit" name="save-referandEarn" id="save_referandEarn" onclick="alert('Are you sure, You want to Modify this Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											</form>
									     	
										</div>
									</div>
									<!-- Page-body end -->
								</div>
							</div>
						</div>
						<!-- Main-body end -->
						 </div >
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/chart.js/js/Chart.js"></script>
    <!-- amchart js -->
    <script src="<?php echo base_url() ?>assets/files/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/widget/amchart/serial.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/widget/amchart/light.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/SmoothScroll.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/dashboard/analytic-dashboard.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-23581568-13');
	</script>
	<script>
	/*=========remove readonly attribute payU===============*/
	document.getElementById('edit_payment').onclick = function() {
	document.getElementById('merchantkey').removeAttribute('readonly');
	document.getElementById('merchantsalt').removeAttribute('readonly');
	document.getElementById('edit_payment').style.display = "none";
	document.getElementById('save_payment').style.display = "block";
	};
	/*=================================================*/
	/*=========remove readonly attribute Credits===============*/
	document.getElementById('edit_credit').onclick = function() {
	document.getElementById('credit').removeAttribute('readonly');
	document.getElementById('edit_credit').style.display = "none";
	document.getElementById('save_credit').style.display = "block";
	};
	/*=================================================*/
	/*=========remove readonly attribute Devices===============*/
	document.getElementById('edit_devices').onclick = function() {
	document.getElementById('devices').removeAttribute('readonly');
	document.getElementById('edit_devices').style.display = "none";
	document.getElementById('save_devices').style.display = "block";
	};
	/*=================================================*/
	/*=========remove readonly attribute Refer and Earn===============*/
	document.getElementById('edit_referandEarn').onclick = function() {
	document.getElementById('referredCreditsValidity').removeAttribute('readonly');
	document.getElementById('maxReferralCount').removeAttribute('readonly');
	document.getElementById('referrerCreditsSignup').removeAttribute('readonly');
	document.getElementById('referreeCreditsSignup').removeAttribute('readonly');
	document.getElementById('referrerCreditsFirstTxn').removeAttribute('readonly');
	document.getElementById('referreeCreditsFirstTxn').removeAttribute('readonly');
	document.getElementById('edit_referandEarn').style.display = "none";
	document.getElementById('save_referandEarn').style.display = "block";
	};
	
	</script>
	    
</body>
</html>