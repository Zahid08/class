<?php 
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);
?>


<!--<style>
    .pcoded-mtext{
        color:black;
    }
    .pcoded .pcoded-navbar[navbar-theme="theme1"] .pcoded-item>li.active>a {
    background: #88cae1;
    color: #dcdcdc;
    border-bottom-color: #88cae1;
    }
    .pcoded[theme-layout="vertical"] .pcoded-navbar .pcoded-item[item-border="true"]>li>a {
        border-bottom-width: 1px;
    }
    .pcoded .pcoded-navbar[navbar-theme="theme1"] .pcoded-item>li>a {
        border-bottom-color: #88cae1;
    }
    .pcoded .pcoded-navbar[navbar-theme="theme1"] {
    background: #88cae1;
}
    
</style>
-->
<nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <?php if($activateLink == 'Home'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>Home">
                                            <span class="pcoded-micon"><i class="feather icon-home" ></i></span>
                                            <span class="pcoded-mtext">Home</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>Home">
                                            <span class="pcoded-micon"><i class="feather icon-home" ></i></span>
                                            <span class="pcoded-mtext">Home</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class="">
                                    <a href="<?php echo base_url() ?>Store">
                                        <span class="pcoded-micon"><i class="feather icon-book" ></i></span>
                                        <span class="pcoded-mtext">Courses</span>
                                    </a>
                                </li>
                               <?php if($activateLink == 'LCourses'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>LCourses">
                                            <span class="pcoded-micon"><i class="feather icon-book" ></i></span>
                                            <span class="pcoded-mtext">My Courses</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>LCourses">
                                            <span class="pcoded-micon"><i class="feather icon-book" ></i></span>
                                            <span class="pcoded-mtext">My Courses</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                                
                                
                                
                                
                                
                               <!-- <?php if($activateLink == 'LNotification'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>LNotification">
                                            <span class="pcoded-micon"><i class="feather icon-bell" ></i></span>
                                            <span class="pcoded-mtext">Notification</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>LNotification">
                                            <span class="pcoded-micon"><i class="feather icon-bell" ></i></span>
                                            <span class="pcoded-mtext">Notification</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                -->
                                
                                <?php if($activateLink == 'LProfile' || $activateLink == 'LWallet' || $activateLink == 'LWishlist' || $activateLink == 'LPurchasehistory'){ ?>
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                <?php }else{ ?>
                                <li class="pcoded-hasmenu">
                                <?php } ?>
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-user" ></i></span>
                                        <span class="pcoded-mtext">Account</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'LProfile'){ ?>
                                            <li class="active">
                                                <a href="<?php echo base_url() ?>LProfile" >
                                                    <span class="pcoded-mtext">Profile</span>
                                                </a>
                                            </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>LProfile" >
                                                <span class="pcoded-mtext">Profile</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'LWallet'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>LWallet" >
                                                <span class="pcoded-mtext" >My Wallet</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>LWallet" >
                                                    <span class="pcoded-mtext">My Wallet</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        
                                        <?php if($activateLink == 'LPurchasehistory'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>LPurchasehistory" >
                                                <span class="pcoded-mtext">Purchase History</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>LPurchasehistory" >
                                                <span class="pcoded-mtext">Purchase History</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                
                                
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>