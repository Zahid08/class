<?php 
include("./iview.php");
   echo $google_analytic;
    
?>
<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();
foreach($metadata as $getmetadata){
$meta_description = $getmetadata->description;
$meta_keywords = $getmetadata->keywords;
}
?>

<?php
date_default_timezone_set("Asia/Calcutta");
$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
$courseTitle = $getcourseData->title;
$courseImage = $getcourseData->image;
}
$user =$this->db->query("SELECT * FROM users")->result();
foreach($user as $getuser){
$userlastlogin = $getuser->last_login;
}
$CourseId = $_GET['courseid'];
$quizTestid =$_GET['quizid'];
$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();
foreach($quiztest as $getquiztest){
$quizName = $getquiztest->title;
$time_limit = $getquiztest->time_limit;
$retake_attempts = $getquiztest->re_take_attempts;
$available_from = $getquiztest->available_from;
$available_from  = date('Y-m-d h:i:s a', strtotime($available_from ));
$available_till = $getquiztest->available_till;
$available_till = date('Y-m-d h:i:s a', strtotime($available_till));
$instructions = $getquiztest->instructions;
$post_msg = $getquiztest->post_msg;
$is_instructions = $getquiztest->is_instructions;
$is_scientific_calc = $getquiztest->is_scientific_calc;
$show_leaderboard = $getquiztest->show_leaderboard;
$show_rank = $getquiztest->show_rank;
$results_mode = $getquiztest->results_mode;
$arrange_by_topic = $getquiztest->arrange_by_topic;
$min_time_pre_submit = $getquiztest->min_time_pre_submit;
$quizTestid = $getquiztest->id;
}
$quizquestion=$this->db->query("SELECT * FROM quiz_test_questions WHERE quiz_test_id='$quizTestid' AND status=1  ")->result();

$tolatquizquestion=$this->db->query("SELECT count(1) as tolatquizquestion FROM quiz_test_questions WHERE quiz_test_id='$quizTestid' AND status=1 ")->result();
foreach($tolatquizquestion as $gettolatquizquestion){
$tolatquizquestion=$gettolatquizquestion->tolatquizquestion;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Competitive Exam Guide || Course Details</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    
    <style>
    
    .modal-dialog{
    overflow-y: initial !important
    }
    .modal-body{
    height: 72vh;
    overflow-y: auto;
    }
    </style>
  </head>
  <body>
    <?php if($this->session->flashdata('cm_msg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <!-- Pre-loader start -->
    <div class="theme-loader">
      <div class="ball-scale">
        <div class='contain'>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
          <div class="pcoded-wrapper">
            <?php include("includes/coursesidenav.php"); ?>
            <div class="pcoded-content">
              <div class="pcoded-inner-content">
                <!-- Main-body start -->
                <div class="main-body">
                  <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                      <div class="row align-items-end">
                        <div class="col-lg-10">
                          <div class="page-header-title">
                            <div class="d-inline">
                              <h4><?php echo $courseTitle ?></h4>
                              <!--<span>23 Course(s)</span>-->
                            </div>
                          </div>
                        </div>
                        
                        
                      </div>
                    </div>
                    <!-- Page-header end -->
                    <!-- Page-body start -->
                    <div class="page-body" >
                      <div class="card">
                        <div class="card-body" >
                          
                          <a href="#!" onclick="toggleFullScreen()">
                            <i class="feather icon-maximize full-screen float-left" ></i>
                          </a>
                          <b class="float-right" id="demo" style="background-color:#88cae1;"></b>
                          <br>
                          <div class="row" >
                            <?php $i=1;
                            $subject=$this->db->query("SELECT DISTINCT subject FROM quiz_test_questions where quiz_test_id = '$quizTestid' ")->result();
                            foreach($subject as $getsubject) {
                               
                            ?>
                            <button type="button" style="margin:2px" data-toggle="tab" data-target="#questions<?php echo  $i?>" role="tab" class="btn btn-primary btn-outline-primary "><?php echo $getsubject->subject ?></button>
                           <?php $i++;}?>
                           
                            <button type="button" style="margin:2px" id="startquiz" name="startquiztest" data-toggle="modal" data-target="#startquiztest_modal" class="btn btn-primary btn-outline-primary "><i class="feather icon-file-text"  ></i>Question[<?php echo $tolatquizquestion?>]</button>
                            
                          </div>
                          
                          <div class="row">
                            <div class="col-sm-8">
                              
                              <!-- Tab panes -->
                              
                              <form method="post"  action="<?php echo base_url(); ?>Lquiztest/result?courseid=<?php echo $CourseId ?>&quizid=<?php echo $quizTestid ?>" autocomplete="off" enctype="multipart/form-data">

                              <div class="tab-content tabs-content card-block "style="height:60vh;overflow: initial ;overflow: auto;">
                                <?php
                                $txt_no = 0;
                                foreach($quizquestion as $getquizquestion_txt){
                                $txt_no++;
                                ?>
                                <div class="tab-pane <?php $a=1; if($txt_no == 1){?> active <?php } ?>" id="questions<?php echo  $getquizquestion_txt->id?>" role="tabpanel">
                                  <p class="m-0"><?php echo $txt_no ?>. <?php echo $getquizquestion_txt->question_text?></p>
                                  <?php if($getquizquestion_txt->type == 1 ){  ?>
                                    <?php
                                    $i=1;
                                    $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getquizquestion_txt->id." ")->result();
                                    foreach($newQuestionOptions as $getnewQuestionOptions){?>
                                    <div  class=" row col-sm-12" >
                                      
                                      <div class="col-1">
                                        <br><input type="radio" name="option" id="option<?php echo $i ?>" value="<?php if($getnewQuestionOptions->is_answer == '1'){?>
                                        <?php echo $getnewQuestionOptions->option_text ?>
                                        <?php }?>"  class="form-control" >
                                      </div>
                                      
                                      <div class="col-11">
                                        <br> <input  name="optionText" id="optionText<?php echo $i ?>" placeholder="Option" value="<?php echo $i.' . '. $getnewQuestionOptions->option_text ?>"class="form-control max-textarea" disabled>
                                        <br>
                                       
                                      </div>
                                      
                                      
                                    </div>
                                    <?php $i++; }?>
                                    <?php }else if($getquizquestion_txt->type == 2){?>
                                    <?php
                                    $i=1;
                                    $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getquizquestion_txt->id." ")->result();
                                    foreach($newQuestionOptions as $getnewQuestionOptions){?>
                                    <div  class=" row col-sm-12" >
                                      
                                      <div class="col-1">
                                        <br><input type="checkbox"  name="option" id="option<?php echo $i.$getquizquestion_txt ?>"  value="<?php echo $getnewQuestionOptions->option_text ?>"  class="form-control" >
                                      </div>
                                      
                                      <div class="col-11">
                                        <br> <input  name="optionText" id="optionText<?php echo $i ?>" placeholder="Option" value="<?php echo $i.' . '.$getnewQuestionOptions->option_text ?>"class="form-control max-textarea" disabled>
                                        <br>
                                        
                                      </div>
                                      
                                      
                                    </div>
                                    <?php $i++; }?>
                                    <?php }else if($getquizquestion_txt->type == 3 ){?>
                                    
                                    <div class="col-12">
                                      <br> <input type="text" name="optionText7'" id="optionText7" placeholder="Answer" class="form-control max-textarea" >
                                      <br>
                                      
                                        
                                    </div>
                                    <?php }else if($getquizquestion_txt->type == 4 ){?>
                                    
                                    <div class="col-12">
                                      <br> <textarea type="text" name="optionText8'" id="optionText8" placeholder="Answer" class="form-control max-textarea" ></textarea>
                                      <br>
                                      
                                    </div>
                                    <?php }?>
                                        <div class="row">
                                          <div class="col-sm-12">
                                            <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-sm-12">
                                             
                                            <input  type="hidden" name="quizquestionsid" id="quizquestionsid" value="<?php echo $getquizquestion_txt->id ?>" class="form-control">
                                          
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-sm-12">
                                            <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $quizTestid ?>" class="form-control">
                                          </div>
                                        </div>
                                        
                                </div>
                                
                                   
                                <?php } ?>
                                
                              </div>
                              <div class="row col-sm-12" >
                                <button type="submit" style="margin:2px" class="btn btn-primary btn-outline-primary float-right" ><b>Submit</b></button>
                                
                                
                                <button type="button" id="prev" style="margin:2px" onclick="clickPreButton(<?php echo $i?>,<?php echo $tolatquizquestion?>)" class="btn btn-primary btn-outline-primary " ><b >Previous</b></button>
                                
                               
                                <?php $i=1;
                                    {
                              $quizquestionid=$getquizquestion->id;
                                $i++;
                                ?>
                                
                                <button type="button" id="next"  style="margin:2px" onclick="clickNextButton(<?php echo $i?>,<?php echo $tolatquizquestion?>)" class="btn btn-primary btn-outline-primary " ><b >Next</b></button>
                                
                                <?php }?>
                                
                              </div>
                            </form>
                            </div>
                            
                            
                            <div class="col-sm-4" style="height:70vh;margin:10px 0px 0px 0px;overflow-y: initial ;overflow-y: auto;" >
                              <div style="height:20vh;overflow-y: initial ;overflow-y: auto;">
                                <p><i class="icofont icofont-square " style="color: green;"></i>  Answered</p>
                                <p><i class="icofont icofont-square " style="color: red;"></i>  Not Answered</p>
                                <p><i class="icofont icofont-square " style="color: #e0e0e0;"></i>  Not Visited</p>
                                <p><i class="icofont icofont-square " style="color: voilet;"></i>  Marked For Review</p>
                                <p><i class="icofont icofont-square " style="color: blue;"></i>  Answered & Marked For Review</p>
                              </div>
                              <div class="tabs-right b-none row" role="tablist" >
                                <!-- Nav tabs -->
                                
                                <?php
                                $btn_no = 0;
                                foreach($quizquestion as $getquizquestion_btn){
                                $btn_no++;
                                ?>
                                <button type="button" style="width:20%;height:5vh;margin:3px;color:#e0e0e0;" data-toggle="tab" href="#questions<?php echo  $getquizquestion_btn->id?>" role="tab" >
                                <b style="color:black;height:40%;">
                                <?php echo $btn_no?>
                                </b>
                                </button>
                                
                                <?php
                                }
                                ?>
                                
                                
                                
                                
                              </div>
                            </div>
                            
                          </div>
                          
                        </div>
                      </div>
                    </div>
                    
                    <!-- Page-body end -->
                  </div>
                </div>
              </div>
              <!-- Main-body end -->
              <!-- <div id="styleSelector">
              </div>-->
            </div>
            <a id="back-top" class="back-to-top page-scroll" href="#main">
              <i class="ion-ios-arrow-thin-up"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!--====================Question Paper Popup====================-->
    <!--===model start===-->
    <div class="modal fade" id="startquiztest_modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row col-sm-12">
              <div class="col-sm-4"></div>
              <div class="col-sm-8">
                <h4><b><i class="feather icon-file-text"></i>
                <lable class="modal-title">Question Paper</lable></b></h4>
                
              </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            <span aria-hidden="true">&times;</span>
          </div>
          
          
          <div class="modal-body">
            <?php
            foreach($quizquestion as $getquizquestion){?>
            <div style="background-color:white;border-bottom:1px solid black ;">
              <b>Question:<?php echo '  '.$getquizquestion->id?></b><br>
              <?php echo $getquizquestion->question_text ?><br><br>
              <ul class="list-group list-group-flush" >
                <?
                $f = 'A';
                $quizquestionoptions=$this->db->query("SELECT * FROM quiz_test_question_options WHERE quiz_test_question_id=".$getquizquestion->id."  AND status=1")->result();
                foreach($quizquestionoptions as $getquizquestionoptions){
                ?>
                
                <?php if($getquizquestion->type == 1 || $getquizquestion->type == 2 ){  ?>
                
                <li class="list-group-item"  ><?php echo  $f.". ".$getquizquestionoptions->option_text?></li>
                
                
                <?php } ?>
                
                
                <?php $f++; } ?>
              </ul>
              <br>
            </div>
            <?php }?>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-outline-primary " data-dismiss="modal">Close</button>
            
          </div>
          
          
        </div>
        
      </div>
    </div>
    <!--===model end===-->
    <!--===============================================================-->
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
    <!-- sweet alert js -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- sweet alert modal.js intialize js -->
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=G-GP3L5V71SR"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
    </script>
    <script>
    var countdown = <?php echo $time_limit ?>*60*1000;
    var timerId = setInterval(function(){
    countdown -= 1000;
    var min = Math.floor(countdown / (60 * 1000));
    var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);
    if (countdown <= 0) {
    clearInterval(timerId);
    location.href='<?php echo base_url(); ?>Lquiztest/result?courseid='+<?php echo $CourseId ?>+'&quizid='+<?php echo $quizTestid ?>;
    } else {
    $("#demo").html(min+" " +"min"+ " : " + sec+" "+"sec"+" "+"Remaining Time");
    }
    }, 1000);
    </script>
    <script>
    function clickNextButton(i,tolatquizquestion){
    a=tolatquizquestion;
    for(n=i;n<=a;n++){
    document.getElementById("questions"+n).style.display = "block";
    }
    }
    </script>
  </body>
</html>