<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
 date_default_timezone_set("Asia/Calcutta"); 

$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
	$courseTitle = $getcourseData->title;
	$courseImage = $getcourseData->image;
}
$user =$this->db->query("SELECT * FROM users")->result();
foreach($user as $getuser){
	$userlastlogin = $getuser->last_login;
	
}
$CourseId = $_GET['courseid'];
$quizTestid =$_GET['quizid'];
$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();
foreach($quiztest as $getquiztest){
$quizName = $getquiztest->title;
$time_limit = $getquiztest->time_limit;
$retake_attempts = $getquiztest->re_take_attempts;
$available_from = $getquiztest->available_from;
$available_from  = date('Y-m-d h:i:s a', strtotime($available_from ));
$available_till = $getquiztest->available_till;
$available_till = date('Y-m-d h:i:s a', strtotime($available_till));
$instructions = $getquiztest->instructions;
$post_msg = $getquiztest->post_msg;
$is_instructions = $getquiztest->is_instructions;
$is_scientific_calc = $getquiztest->is_scientific_calc;
$show_leaderboard = $getquiztest->show_leaderboard;
$show_rank = $getquiztest->show_rank;
$results_mode = $getquiztest->results_mode;
$arrange_by_topic = $getquiztest->arrange_by_topic;
$min_time_pre_submit = $getquiztest->min_time_pre_submit;
$quizTestid = $getquiztest->id;

}
$quizTestid =$_GET['quizid'];
$quizquestion=$this->db->query("SELECT * FROM quiz_test_questions WHERE quiz_test_id='$quizTestid' AND status=1 ORDER BY 1 DESC ")->result();
foreach($quizquestion as $getquizquestion){
$id=$getquizquestion->id;
$quiz_test_id=$getquizquestion->quiz_test_id;
$type=$getquizquestion->type;
$question=$getquizquestion->question_text;
$subject=$getquizquestion->subject;
$topic=$getquizquestion->topic;
}
$quizquestions=$this->db->query("SELECT * FROM quiz_test_question_options WHERE status=1")->result();
foreach($quizquestions as $getquizquestions){
}
$tolatquizquestion=$this->db->query("SELECT count(1) as tolatquizquestion FROM quiz_test_questions WHERE quiz_test_id='$quizTestid' AND status=1 ")->result();
foreach($tolatquizquestion as $gettolatquizquestion){
    $tolatquizquestion=$gettolatquizquestion->tolatquizquestion;
}                                
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Course Details</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<style>
		   
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 60vh;
    overflow-y: auto;
}
		</style>
	</head>
	<body>
	    <?php if($this->session->flashdata('cm_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>						         
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<?php include("includes/coursesidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="main-body">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-10">
													<div class="page-header-title">
														<div class="d-inline">
															<h4><?php echo $courseTitle ?></h4>
															<!--<span>23 Course(s)</span>-->
														</div>
													</div>
												</div>
												
												
											</div>
										</div>
										<!-- Page-header end -->
										<!-- Page-body start -->
										<div class="page-body">
										    <div class="card">
											    <div class="card-body" style="background-color:#88cae1;">
													<center>
													  <h4><b><?php echo $getquiztest->title?></b></h4><br>
													  <b>Total Question : <?php echo $tolatquizquestion?></b><br>
													  <b>Total Time : <?php echo $time_limit.'  '.minutes?></b><br><br>
													  
													  <button type="submit" id="startquiz" name="startquiz" data-toggle="modal" data-target="#startquiz_modal" class="btn btn-primary btn-outline-primary ">Start Quiz</button>
													</center>
													<!--===model start===-->
													<div class="modal fade" id="startquiz_modal" tabindex="-1" role="dialog">
                                                           <div class="modal-dialog modal-lg" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <div class="row col-sm-12">
                                                                            <div class="col-sm-4"></div>
                                                                                <div class="col-sm-8">
                                                                                  <h4><b><i class="icofont icofont-info-circle"></i>
                                                                                 <lable class="modal-title">Instruction</lable></b></h4>
                                                                                </div>
                                                                            </div>
                                                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                              <span aria-hidden="true">&times;</span>
                                                                        </div>
                                                                
                                                                        <form  method="post" action="<?php echo base_url(); ?>Lquizquestions?courseid=<?php echo $CourseId ?>&quizid=<?php echo $quizTestid ?>" autocomplete="off" enctype="multipart/form-data">
                                                                        <div class="modal-body"  >
                                                                            <div  style="margin:0% 5% 0% 5%;">
                                                                            <lable><u><b>General Instructions:</b></u></lable>
                                                                               
                                                                                <p>1.The Question Palette displayed on the right side of screen will show the status of each question using one of the following symbols:</p>
                                                                        		<p><i class="icofont icofont-square " style="color: #e0e0e0;"></i>  You have not visited the question yet.</p>
                                                                        		<p><i class="icofont icofont-square " style="color: red;"></i>  You have not answered the question.</p>
                                                                        		<p><i class="icofont icofont-square " style="color: green;"></i>  You have  answered the question.</p>
                                                                        		<p><i class="icofont icofont-square " style="color: voilet;"></i>  You have NOT answered the question, but have marked the question for review.</p>
                                                                        	    <p>2.The Marked for Review status for a question simply indicates that you would like to look at that question again.&nbsp;<em style="color:red">If a question is answered and Marked for Review, your answer for that question will be considered in the evaluation.</em></p>
                                                                        	<lable><u><b>Navigating to a Question:</b></u></lable>
                                                                        	    <p>3.To answer a question, do the following:
                                                                        	       
                                                                                <li>Click on the question number in the Question Palette to go to that numbered question directly.</li>
                                    											<li>Click on&nbsp;<strong>Save &amp;Next</strong> to save your answer for the current question and then go to the next question.</li>
                                    											<li>Click on&nbsp;<strong>Mark for Review &amp; Next</strong> to save your answer for the current question, mark it for review, and then go to the next question.</li>
                                    											<li>Caution: Note that your answer for the current question will not be saved, if you navigate to another question directly by clicking on its question number.</li>
                                                                                    
                                                                                </p>
                                                                                <p>4.You can view all the questions by clicking on the Question Paper button. Note that the options for multiple choice type questions will not be shown.</p>
                                                                        	<lable><u><b>Answering a Question:</b></u></lable>    
                                                                        	    <p>5.Procedure for answering a multiple choice type question:
                                                                                <li>To select your answer, click on the button of one of the options.</li>
                                    											<li>To deselect your chosen answer, click on the button of the chosen option again or click on the&nbsp;<strong>Clear Response</strong> button.</li>
                                    											<li>To change your chosen answer, click on the button of another option.</li>
                                    											<li>To save your answer, you MUST click on the&nbsp;<strong>Save &amp; Next</strong> button.</li>
                                    											<li>To mark the question for review, click on the&nbsp;<strong>Mark for Review &amp; Next</strong> button.</li>
                                    											<li>If an answer is selected for a question that is &#39;Marked for Review&#39;, that answer will be considered in the evaluation even if it is not marked as &#39;Save &amp; Next&#39;, at the time of final submission.</li>
                                                                        	    </p>
                                                                        	    <p>6.To change your answer to a question that has already been answered, first select that question for answering and then follow the procedure for answering that type of question.</p>
									                                            <p>7.Note that questions for which option has been chosen and answers are saved or marked for review will be considered for evaluation.</p>
                                                                        	</div>
                                                                        	</div>
                                                                            <div class="modal-footer  ">
                                                                                <div class="row col-sm-12">
                                                                                    <div class="col-sm-5">
                                                                                       
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        
                                                                                           <button type="submit" class="btn btn-primary btn-outline-primary " >Start </button>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                            
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                    </div>    
													<!--===model end===-->
												</div>
											</div>	
										   
										</div>
										<!-- Page-body end -->
									</div>
								</div>
							</div>
							<!-- Main-body end -->
							<!-- <div id="styleSelector">
							</div>-->
						</div>
						<a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
					</div>
				</div>
			</div>
		</div>
			<!-- Recorded Vdo Link -->

		<!-- Warning Section Starts -->
		<!-- Older IE warning message -->
		
		<!-- Warning Section Ends -->
		<!-- Required Jquery -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
		<!-- jquery slimscroll js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
		<!-- modernizr js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
		<!-- data-table js -->
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
		<!-- Bootstrap date-time-picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Date-dropper js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
		<!-- Color picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
		<!-- Mini-color js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
		<!-- i18next.min.js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
		<!-- Custom js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
		
        <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
        <!-- modalEffects js nifty modal window effects -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-23581568-13');
		</script>
		<script>
		    function alert()
		    {
                swal({
                  title: "Live Class has Ended?",
                  text: "Try to attend next class in-time",
                  icon: "warning",
                  button: true,
                  dangerMode: true,
                })
                
            }
		</script>
		
	</body>
</html>