<?php 
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);
?>
<nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                
                                
                                
                                <?php if($activateLink == 'Courses' || $activateLink == 'Assetlibrary' || $activateLink == 'Liveclass'){ ?>
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                <?php }else{ ?>
                                <li class="pcoded-hasmenu">
                                <?php } ?>
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                        <span class="pcoded-mtext">Content</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'ICourses'){ ?>
                                            <li class="active">
                                                <a href="<?php echo base_url() ?>ICourses">
                                                    <span class="pcoded-mtext">Courses</span>
                                                </a>
                                            </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>ICourses">
                                                <span class="pcoded-mtext">Courses</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'IAssetlibrary'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>IAssetlibrary">
                                                <span class="pcoded-mtext">Asset Library</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>IAssetlibrary">
                                                    <span class="pcoded-mtext">Asset Library</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'ILiveclass'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>ILiveclass">
                                                <span class="pcoded-mtext">Live Class</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>ILiveclass">
                                                <span class="pcoded-mtext">Live Class</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                
                               <!-- <?php if($activateLink == 'IAccount'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>IAccount">
                                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                            <span class="pcoded-mtext">My Account</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>IAccount">
                                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                            <span class="pcoded-mtext">My Account</span>
                                        </a>
                                    </li>
                                <?php } ?>-->
                                <li class="">
                                        <a href="<?php echo base_url() ?>Iprofile">
                                            <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                                            <span class="pcoded-mtext">Profile</span>
                                        </a>
                                    </li>
                                
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>