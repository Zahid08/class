<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();
foreach($metadata as $getmetadata){
$meta_description = $getmetadata->description;
$meta_keywords = $getmetadata->keywords;
}
?>
<?php
date_default_timezone_set("Asia/Calcutta"); 
$CourseId = $_GET['courseid'];

$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
$courseTitle = $getcourseData->title;
$courseImage = $getcourseData->image;
}
$CourseId = $_GET['courseid'];
$quizTestid =$_GET['id'];
$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();
foreach($quiztest as $getquiztest){
$quizName = $getquiztest->title;
$time_limit = $getquiztest->time_limit;
$retake_attempts = $getquiztest->re_take_attempts;
$available_from = $getquiztest->available_from;
$available_from  = date('Y-m-d h:i:s a', strtotime($available_from ));
$available_till = $getquiztest->available_till;
$available_till = date('Y-m-d h:i:s a', strtotime($available_till));
$instructions = $getquiztest->instructions;
$post_msg = $getquiztest->post_msg;
$is_instructions = $getquiztest->is_instructions;
$is_scientific_calc = $getquiztest->is_scientific_calc;
$show_leaderboard = $getquiztest->show_leaderboard;
$show_rank = $getquiztest->show_rank;
$results_mode = $getquiztest->results_mode;
$arrange_by_topic = $getquiztest->arrange_by_topic;
$min_time_pre_submit = $getquiztest->min_time_pre_submit;
$quizTestid = $getquiztest->id;

}
$quizTestid =$_GET['id'];
$quizquestion=$this->db->query("SELECT * FROM quiz_test_questions WHERE quiz_test_id='$quizTestid' AND status=1 ORDER BY 1 DESC ")->result();
foreach($quizquestion as $getquizquestion){
$id=$getquizquestion->id;
$quiz_test_id=$getquizquestion->quiz_test_id;
$type=$getquizquestion->type;
$question=$getquizquestion->question_text;
$subject=$getquizquestion->subject;
$topic=$getquizquestion->topic;
}
$quizquestions=$this->db->query("SELECT * FROM quiz_test_question_options WHERE status=1")->result();
foreach($quizquestions as $getquizquestions){
}
$tolatquizquestion=$this->db->query("SELECT count(1) as tolatquizquestion FROM quiz_test_questions WHERE quiz_test_id='$quizTestid'  ")->result();
foreach($tolatquizquestion as $gettolatquizquestion){
    $tolatquizquestion=$gettolatquizquestion->tolatquizquestion;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Competitive Exam Guide || Question Paper </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
     <style>
        .modal-lg {
         max-width: 90%;
         
         }
         .modal-body{
              max-width: 80%;
              margin:2%;
             
            
         }
         
    </style>
  </head>
  <body>
    <?php if($this->session->flashdata('endmsg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('endmsg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if($this->session->flashdata('delmsg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('delmsg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
   
    <?php if($this->session->flashdata('amsg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('amsg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
     <?php if($this->session->flashdata('msg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('msg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <!-- Pre-loader start -->
    <div class="theme-loader">
      <div class="ball-scale">
        <div class='contain'>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
          <div class="ring">
            <div class="frame"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">
        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
          <div class="pcoded-wrapper" style="width:100%;">
				<?php include("includes/coursesidenav.php"); ?>
            <div class="pcoded-content">
              <div class="pcoded-inner-content">
                <!-- Main-body start -->
               <div class="row" >
								    <div class="col-sm-1"></div>
								<div class="main-body col-sm-11">
                  <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                      <div class="row align-items-end">
                        <div class="col-lg-10">
                          <div class="page-header-title">
                            <div class="d-inline">
                              <h4><?php echo $courseTitle ?></h4>
                              <!--<span>23 Course(s)</span>-->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="page-body">
                      <div class="card">
                        <div class="card-body">
                          
                          <div class="row">
                            <div class="col-lg-9">
                              
                            </div>
                            <div class="col-lg-2">
                              <button type="button" name="addquiztestQuestion" id="addquiztestQuestion" data-toggle="modal" data-target="#addquiztestQuestion_modal"  class="btn btn-primary btn-outline-primary ">Add New Question</button>
                              <br><br>
                            </div>
                           </div> 
                            
                            <!--==========================Add questions Model===============================================-->
                            <div class="modal fade" id="addquiztestQuestion_modal" tabindex="-1" role="dialog">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                   <lable> <h4 class="modal-title">Add Quiz test Question</h4></lable>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form  method="post" action="<?php echo base_url(); ?>Newquiztest/addquiztestQuestion" autocomplete="off" enctype="multipart/form-data">
                                   <center> <div class="modal-body">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <div class="row">
                                            <div class="col-2">
                                              <lable class="float-left">Type</lable>
                                            </div>
                                            <div class="col-10">
                                              <select class="form-control" name="type" id="type" onchange="OptionType(this.value)" required>
                                                <option name="type" value="1">Single Correct Option</option>
                                                <option name="type" value="2">Multiple Correct Options</option>
                                                <option name="type" value="3">Fill in the Blank</option>
                                                <option name="type" value="4">Paragraph</option>
                                                
                                              </select>
                                              
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row" >
                                        <div class="col-2">
                                          <br><lable class="float-left">Subject</lable>
                                        </div>
                                        <div class="col-10">
                                          <br>  <input type="text" name="subjectname" id="subjectname" placeholder="subject" class="form-control" required>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-2">
                                          <br><lable class="float-left">Topic</lable> 
                                        </div>
                                        <div class="col-10">
                                          <br>  <input type="text" name="Topic" id="Topic" placeholder="Topic" class="form-control" required>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-2">
                                          <br> <lable class="float-left">Question Text</lable>
                                        </div>
                                        <div class="col-10">
                                          <br>  <textarea type="text" name="QuestionText" rows="4"id="QuestionText" placeholder="QuestionText" class="form-control max-textarea" required></textarea>
                                        </div>
                                      </div>
                                      
                                      <div id="single" class="row"  >
                                        
                                        <div class="col-sm-12">
                                          <br><h4 class="btn btn-primary  btn-md btn-block   text-left m-b-20 ">Options</h4>
                                        </div>
                                        <div  class=" row col-sm-12" >
                                          <div class="col-1">
                                            <br><input type="checkbox" name="option1" id="option1" value="1" class="form-control" >
                                          </div>
                                          <div class="col-3">
                                            <br>  <lable class="float-left">Correct Option 1</lable>
                                          </div>
                                          <div class="col-7">
                                            <br> <input type="text" name="optionText1" id="optionText1" placeholder="Option" class="form-control max-textarea" >
                                            <br>
                                          </div>
                                          <div class="col-1" id="single" >
                                            <br>   <button type="button" name="add" id="add" class="btn btn-primary btn-outline-primary btn-icon" title="Add Options"><h3>+</h3></button><br>
                                          </div>
                                        </div>
                                        
                                      </div>
                                      <!--for Blank-->
                                      <div id="blank" class="row" style="display:none;">
                                        <div  class=" row col-sm-12" >
                                          <div class="col-2">
                                            <br> <lable class="float-left">Answer</lable>
                                          </div>
                                          <div class="col-10">
                                            <br>  <input type="text" name="optionText7" id="optionText7" placeholder="Answer" class="form-control" >
                                          </div>
                                        </div>
                                      </div>
                                      <!--for Paragraph-->
                                      <div class="row " id="para" style="display:none">
                                        <div  class=" row col-sm-12" >
                                          <div class="col-2">
                                            <br><lable class="float-left">Answer</lable>
                                          </div>
                                          <div class="col-10">
                                            <br>  <textarea type="text" name="optionText8" rows="4" id="optionText8" placeholder="Enter Your Text..." class="form-control max-textarea" ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        <div class="col-2">
                                          <br> <lable class="float-left">Explaination</lable>
                                        </div>
                                        <div class="col-10">
                                          <br>  <textarea type="text" name="explaination" rows="4" id="explaination" placeholder="QuestionText" class="form-control max-textarea" required></textarea>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <br><button type="button" data-toggle="collapse" data-target="#showadvancedoptions"  class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 " >Show Advanced Options</button>
                                          <br>
                                        </div>
                                      </div>
                                      
                                      
                                      <div  class="collapse"id="showadvancedoptions">
                                        <div class="row">
                                          <div class="col-2">
                                            <br> <lable class="float-left">Order</lable>
                                          </div>
                                          <div class="col-10">
                                            <br>  <input type="number" name="Order" id="Order" value="<?php echo $tolatquizquestion+1 ?>"placeholder="Order"  class="form-control" >
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-2">
                                            <br> <lable class="float-left">Marks</lable>
                                          </div>
                                          <div class="col-10">
                                            <br>  <input type="number" name="Marks" id="Marks" placeholder="Mark(s)"  class="form-control" >
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-2">
                                            <br> <lable class="float-left">Penalty</lable> 
                                          </div>
                                          <div class="col-10">
                                            <br>  <input type="number" name="Penalty" id="Penalty" placeholder="Penalty" class="form-control" >
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $_GET['courseid'] ?>" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $_GET['id'] ?>" class="form-control">
                                      </div>
                                    </div>
                                    
                                    
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary btn-outline-primary ">Save </button>
                                    </div>
                                    </div></center>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!--=============================================================================================-->
                            
                            <!--=============================================================================================-->
                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/addquiztest" autocomplete="off" enctype="multipart/form-data">
                              
                            <div class="row">
                              <div class="col-lg-12">
                                <button type="button" data-toggle="collapse" data-target="#quiz_setting" class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 "><i class="feather icon-chevrons-down"></i>Quiz Setting</button>
                              </div>
                            </div>
                            <div  class="collapse"id="quiz_setting">
                              <div class="row">
                                <div class="col-lg-10">
                                  
                                </div>
                                <div class="col-lg-2">
                                  <button type="button" name="edit_item" id="edit_item" onclick="edititem_test()" value="<?= $quizTestid ?>"  class="btn btn-primary btn-outline-primary ">Edit</button>
                                  <button type="submit" name="save_item" id="save_item" onclick="alert('Are you sure, You want to Modify Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save & Submit</button>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12"><br>
                                  <input type="text" name="chaptername" id="chaptername" value="<?php echo $quizName ?>" placeholder="Title" class="form-control"  readonly required>
                                  <br>
                                </div>
                                <br><br>
                                <div class="col-sm-12">
                                  <b>Time Limit (in minutes)</b>
                                </div>
                                <br><br>
                                <div class="col-sm-12">
                                  <input class="form-control"  type="number" min="0" name="time_limit" id="time_limit" value="<?php echo $time_limit ?>" placeholder="Total Time (in minutes)"readonly required>
                                </div>
                                <br>
                                <div class="col-sm-12">
                                <br>    
                                 <b>Re-take Attempts</b> 
                               </div>
                                <br>
                               
                                <div class="col-sm-12">
                                                                                    
								<input class="form-control"  type="number" min="0" name="re_take_attempts" id="re_take_attempts" value="<?php echo $retake_attempts ?>"  placeholder="Re-take Attempts" readonly required>
                                </div>
                                
                                
                                <br>
                                
                                <div class='col-sm-12'>
                                  <b>Test Open Window</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                  <br><br>
                                  <div class="row">
                                    <div class="col-sm-6">
                                      <input type="text" name="available_from" id="available_from" min="<?php echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_from ?>"  onfocus="this.type='datetime-local'" placeholder="Available From" class="form-control" readonly required>
                                    </div>
                                    <div class="col-sm-6">
                                      <input type="text" name="available_till" id="available_till" min="<?php  echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_till ?>" onfocus="this.type='datetime-local'" placeholder="Available Till" class="form-control"  readonly required>
                                    </div>
                                  </div>
                                </div>
                                <br><br>
                                <div class="col-sm-12">
                                  <br>
                                  <b>Show Default Instruction</b>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                                  <label class="radio-inline">
                                    <input type="radio" name="ShowDefaultInstruction" id="no" value="0"<?php if($is_instructions == '0'){ ?> checked <?php } ?>readonly required> No
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio" name="ShowDefaultInstruction" id="yes" value="1" <?php if($is_instructions == '1'){ ?> checked <?php } ?>readonly required> Yes
                                  </label>
                                  <br>
                                  <textarea class="form-control max-textarea"  name="message" id="message" maxlength="500" rows="4" value="<?php echo $getquiztest->instructions ?>" placeholder="Instruction" readonly required ><?php echo $getquiztest->instructions ?></textarea>
                                  
                                </div>
                                <div class="col-sm-12">
                                  <br>
                                  <b>Show Scientific Calculator</b>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                                  <label class="radio-inline">
                                    <input type="radio" name="ShowScientificCalculator" id="n" value="0" <?php if($is_scientific_calc == '0'){ ?> checked <?php } ?>readonly required> No
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio"  name="ShowScientificCalculator" id="y" value="1" <?php if($is_scientific_calc == '1'){ ?> checked <?php } ?>readonly required> Yes
                                  </label>
                                  <br>
                                </div>
                                <div class="col-sm-12" id="results" >
                                  <br>
                                  <b>Generate Results</b>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                                  <label class="radio-inline">
                                    <input type="radio" name="GenerateResults"  value="1" <?php if($results_mode == '1'){ ?> checked <?php } ?>readonly required> Manual
                                  </label>
                                  
                                  <label class="radio-inline"  >
                                    <input type="radio"  name="GenerateResults"  value="2" <?php if($results_mode == '2'){ ?> checked <?php } ?>readonly required> At a particular time
                                  </label>
                                  
                                  <br>
                                  <div class="col-sm-12" id="available_result_till">
                                    <input type="text" name="available_result_till" id="available_result_till" min="<?php  echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_till ?>" onfocus="this.type='datetime-local'" placeholder="Available Till" class="form-control"  readonly required>
                                  </div>
                                </div>
                                <div class="col-sm-12"><br>
                                  <b>Post Submit Msg</b> <br><br>
                                  <textarea class="form-control max-textarea"  name="post_message" id="post_message" maxlength="500" rows="4" value="<?php echo $getquiztest->post_msg ?>" placeholder="msg" readonly required><?php echo $getquiztest->post_msg ?></textarea>
                                </div>
                                <div class="col-sm-12">
                                  <br>
                                  <b>Show Leaderboard</b>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                                  <label class="radio-inline">
                                    <input type="radio" name="ShowLeaderboard" id="n"value="0" <?php if($show_leaderboard == '0'){ ?> checked <?php } ?>readonly required > No
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio"  name="ShowLeaderboard" id="y"value="1" <?php if($show_leaderboard == '1'){ ?> checked <?php } ?>readonly required> Yes
                                  </label>
                                  <br>
                                </div>
                                <div class="col-sm-12">
                                  <br>
                                  <b>Show Rank</b>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                                  <label class="radio-inline">
                                    <input type="radio" name="ShowRank" id="n" value="0" <?php if($show_rank == '0'){ ?> checked <?php } ?>readonly required> No
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio"  name="ShowRank" id="y" value="1"<?php if($show_rank == '1'){ ?> checked <?php } ?> readonly required> Yes
                                  </label>
                                  <br>
                                </div>
                                <div class="col-sm-12">
                                  <br>
                                  <b>Minimum Time Before Submit</b><br><br>
                                </div>
                                <div class="col-sm-12">
                                  <input class="form-control"  type="number" min="0" name="available_time" id="available_time" value="<?php echo $getquiztest->min_time_pre_submit ?>" placeholder="Total Time (in minutes)" readonly required >
                                </div>
                                <br>
                                
                                <div class="col-sm-12">
                                  <br>
                                  <b>Arrange questions by topic</b>&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                                  <label class="radio-inline">
                                    <input type="radio" name="Arrangequestionsbytopic" id="n"   value="0"<?php if($arrange_by_topic == '0'){ ?> checked <?php } ?>readonly required > No
                                  </label>
                                  <label class="radio-inline">
                                    <input type="radio"  name="Arrangequestionsbytopic" id="y"  value="1"<?php if($arrange_by_topic == '1'){ ?> checked <?php } ?>readonly required> Yes
                                  </label>
                                  <br>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $_GET['id'] ?>" class="form-control">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <br><br>
                            <div class="row">
                              <div class="col-lg-12">
                                <button type="button" data-toggle="collapse" data-target="#questions" class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 "><i class="feather icon-chevrons-down"></i>Questions[<?php echo $tolatquizquestion?>]</button>
                              </div>
                            </div>
                            <div  class=""id="questions">
                                <div class="row">
                                    <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width:5%">#</th>
                                                                        <th style="width:98%">Questions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                    $newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." ORDER BY id DESC ")->result();
                                                                    $i = 0;
                                                                        foreach($newQuestions as $getnewQuestions){
                                                                            $i++;
                                                                            $questionid = $getnewQuestions->id;
                                                                        ?>
                                                                    <tr>
                                                                        <td><b><?php echo $i.'.' ?></b></td>
                                                                        <td><?php echo $getnewQuestions->question_text ?>
                                                                        <p>&nbsp;</p>
                                                                        <div class="card">
                                                                          <ul class="list-group list-group-flush" >
                                                                              <?
                                                                              
                                                                               $f = 'A';
                                                                               
                                                                                $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                                                                                foreach($newQuestionOptions as $getnewQuestionOptions){
                                                                                    
                                                                                ?>
                                                                                
                                                                               <?php if($getnewQuestionOptions->is_answer == 1 || $getnewQuestionOptions->option_no == 7 || $getnewQuestionOptions->option_no == 8){  ?>
                                                                               
                                                                            <li class="list-group-item" style="background-color:#32B643;color:white;" ><?php echo  $f.". ".$getnewQuestionOptions->option_text?></li>
                                                                            <?php }else{ ?>
                                                                            <li class="list-group-item" ><?php echo $f.". ".$getnewQuestionOptions->option_text?></li>
                                                                           
                                                                            <?php } ?>
                                                                            
                                                                            
                                                                            <?php $f++; } ?>
                                                                            </ul>
                                                                            
                                                                        </div>
                                                                        
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-sm-3">
                                                                                 <p>Topic: <b><?=' '.''.$getnewQuestions->topic .' '?></b></p>
                                                                                
                                                                                <p>Order:<b><?=' '.''.$getnewQuestions->order .' '?></b></p>
                                                                               
                                                                                <p>Penalty:<b><?=' '.''.$getnewQuestions->penalty .' '?></b></p>
                                                                               
                                                                                <p>Marks:<b><?=' '.''.$getnewQuestions->mark .' '?></b></p>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <p>Created: <b><?=''.'  '.date('Y-m-d h:i:s a  ', strtotime($getnewQuestions->created_date));'  '?></b></p>
                                                                                <p>Modified: <b><?=''.'  '.date('Y-m-d h:i:s a  ', strtotime($getnewQuestionOptions->updated_date));'  '?></b></p>
                                                                                <p>Subject: <b><?=' '.''.$getnewQuestions->subject .' '?></b></p>
                                                                            </div>
                                                                            
                                                                            <div class="col-sm-2">
                                                                                
                                                                            </div>
                                                                            
                                                                            
                                                                            
                                                                            
                                                                             <div class="col-sm-2">
                                                                                <button type="button" value="<?= $questionid ?>,<?=$CourseId?>,<?= $quizTestid ?>" data-toggle="modal" data-target="#editquiztestquestion_modal<?=  $getnewQuestions->id ?>"   class="btn btn-primary btn-outline-primary">Edit</button>
																                
																                <button onclick ="alert_del_msg('<?= $quizTestid ?>', '<?= $questionid?>', '<?=$CourseId?>')"value="<?= $questionid ?>,<?=$CourseId?>,<?= $quizTestid ?>" type="button" name="deleteitem" id="deleteitem"  class="btn btn-primary btn-outline-primary">Delete</button>
                                                                                
                                                                                <!--=====edit===-->
                                                                                <div class="modal fade" id="editquiztestquestion_modal<?=  $getnewQuestions->id ?>" tabindex="-1" role="dialog">
                                                                                     <div class="modal-dialog modal-lg" role="document">
                                                                                         <div class="modal-content">
                                                                                            <div class="modal-header">
                                                                                                <lable><h4 class="modal-title">Edit Question</h4></lable>
                                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                      <span aria-hidden="true">&times;</span>
                                                                                                    </button>
                                                                                            </div>
                                                                                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/editquiztestquestions" autocomplete="off" enctype="multipart/form-data">
                                                                                    <center><div class="modal-body">
                                                                                                <div class="row">
                                                                                                    <div class="col-sm-12">
                                                                                                        <div class="row">
                                                                                                            <div class="col-1">
                                                                                                                   <lable class="float-left">Type</lable> 
                                                                                                            </div>
                                                                                                        <div class="col-9">

                                                                                                            <select class="form-control" name="type" id="type" onchange="OptionType1(this.value)" required>
                                                                                                                <option name="type" value="1"<?php if($getnewQuestions->type == '1'){ ?> <?php echo $getnewQuestions->type.'Single Correct Option' ?> <?php } ?>>Single Correct Option</option>
                                                                                                                <option name="type" value="2"<?php if($getnewQuestions->type == '2'){ ?> <?php echo $getnewQuestions->type.'Multiple Correct Options' ?> <?php } ?>>Multiple Correct Options</option>
                                                                                                                <option name="type" value="3"<?php if($getnewQuestions->type == '3'){ ?> <?php echo $getnewQuestions->type.'Fill In the Blank' ?> <?php } ?>>Fill In the Blank</option>
                                                                                                                <option name="type" value="4"<?php if($getnewQuestions->type == '4'){ ?> <?php echo $getnewQuestions->type.'Multiple Correct Options' ?> <?php } ?>>Multiple Correct Options</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row" >
                                                                                                <div class="col-1">
                                                                                                    <br><lable class="float-left">Subject</lable>
                                                                                                </div>
                                                                                                <div class="col-9">
                                                                                                    <br>  <input type="text" name="subjectname" id="subjectname" value="<?php echo $getnewQuestions->subject  ?>" placeholder="subject" class="form-control" required>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-1">
                                                                                                     <br> <lable class="float-left">Topic</lable>
                                                                                                </div>
                                                                                                <div class="col-9">
                                                                                                     <br>  <input type="text" name="Topic" id="Topic" placeholder="Topic" value="<?php echo $getnewQuestions->topic ?>" class="form-control" required>
                                                                                                </div>
                                                                                            </div>
                                                                                          <div class="row">
                                                                                            <div class="col-1">
                                                                                              <br> <lable class="float-left">Question Text</lable>
                                                                                            </div>
                                                                                            <div class="col-9">
                                                                                              <br>  <textarea type="text" name="QuestionText" rows="4"id="QuestionText" placeholder="QuestionText" value="<?php echo $getnewQuestions->question_text ?>" class="form-control max-textarea" required><?php echo $getnewQuestions->question_text ?></textarea>
                                                                                            </div>
                                                                                          </div>
                                                                                          <?php 
                                                                                        
                                                                                           $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                                                                                                                                    foreach($newQuestionOptions as $getnewQuestionOptions){}?>
                                                                                                                                 
                                                                                          <div id="editsingle" class="row"  >
                                                                                            
                                                                                            <div class="col-lg-11">
                                                                                              <br><h4 class="btn btn-primary  btn-md btn-block   text-left m-b-20 ">Options</h4>
                                                                                            </div>
                                                                                             <?php 
                                                                                              $i=1;
                                                                                           $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                                                                                                                                    foreach($newQuestionOptions as $getnewQuestionOptions){?>
                                                                                            <div  class=" row col-sm-12" >
                                                                                                
                                                                                              <div class="col-1">
                                                                                                <br><input type="checkbox" name="option'<?php echo $i ?>'" id="option'<?php echo $i ?>'" value="1" <?php if($getnewQuestionOptions->is_answer=='1'){ ?> checked <?php } ?> class="form-control" >
                                                                                              </div>
                                                                                              <div class="col-2">
                                                                                                <br> <lable class="float-left">Correct Option<?php echo $i ?></lable> 
                                                                                              </div>
                                                                                              <div class="col-6">
                                                                                                <br> <input type="text" name="optionText'<?php echo $i ?>'" id="optionText'<?php echo $i ?>'" placeholder="Option" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>"class="form-control max-textarea" >
                                                                                                <br>
                                                                                              </div>
                                                                                              
                                                                                              <div class="col-1" id="single1" >
                                                                                                <br>   <button type="button" name="edit" id="edit" class="btn btn-primary btn-outline-primary btn-icon" title="Add Options"><h3>+</h3></button><br>
                                                                                              </div>
                                                                                            </div>
                                                                                            <?php $i++; }?>
                                                                                          </div>
                                                                                          <!--for Blank-->
                                                                                          <div id="editblank" class="row" style="display:none;">
                                                                                            <div  class=" row col-sm-12" >
                                                                                              <div class="col-1">
                                                                                                <br> <lable class="float-left">Answer</lable>
                                                                                              </div>
                                                                                              <div class="col-9">
                                                                                                <br>  <input type="text" name="optionText7" id="optionText7" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>" class="form-control" >
                                                                                              </div>
                                                                                            </div>
                                                                                          </div>
                                                                                           
                                                                                          <!--for Paragraph-->
                                                                                          <div class="row " id="editpara" style="display:none">
                                                                                            <div  class=" row col-sm-12" >
                                                                                              <div class="col-2">
                                                                                                <br> <lable class="float-left">Answer</lable>
                                                                                              </div>
                                                                                              <div class="col-10">
                                                                                                <br>  <textarea type="text" name="optionText8" rows="4" id="optionText8" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>" placeholder="Enter Your Text..." class="form-control max-textarea" >value="<?php echo $getnewQuestions->option_text ?>"</textarea>
                                                                                              </div>
                                                                                            </div>
                                                                                          </div>
                                                                                          
                                                                                          <div class="row">
                                                                                            <div class="col-1">
                                                                                              <br><lable class="float-left">Explaination</lable>
                                                                                            </div>
                                                                                            <div class="col-9">
                                                                                              <br>  <textarea type="text" name="explaination" rows="4" id="explaination" placeholder="QuestionText" value="<?php echo $getnewQuestionOptions->explaination ?>" class="form-control max-textarea" ><?php echo $getnewQuestions->explaination ?></textarea>
                                                                                            </div>
                                                                                          </div>
                                                                                          
                                                                                        
                                                                                          <div class="row">
                                                                                            <div class="col-lg-11">
                                                                                              <br><button type="button" data-toggle="collapse" data-target="#editshowadvancedoptions<?=  $getnewQuestions->id ?>"  class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 ">Show Advanced Options</button>
                                                                                              <br>
                                                                                            </div>
                                                                                          </div>
                                                                                          
                                                                                          
                                                                                          <div  class="collapse"id="editshowadvancedoptions<?=  $getnewQuestions->id ?>">
                                                                                            <div class="row">
                                                                                              <div class="col-1">
                                                                                                <br> <lable class="float-left">Order</lable>
                                                                                              </div>
                                                                                              <div class="col-9">
                                                                                                <br>  <input type="number" name="Order" id="Order" placeholder="Order" value="<?php echo $getnewQuestions->order ?>" class="form-control" >
                                                                                              </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                              <div class="col-1">
                                                                                                <br> <lable class="float-left">Marks</lable>
                                                                                              </div>
                                                                                              <div class="col-9">
                                                                                                <br>  <input type="number" name="Marks" id="Marks" placeholder="Mark(s)" value="<?php echo $getnewQuestions->mark ?>" class="form-control" >
                                                                                              </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                              <div class="col-1">
                                                                                                <br><lable class="float-left">Penalty</lable> 
                                                                                              </div>
                                                                                              <div class="col-9">
                                                                                                <br>  <input type="number" name="Penalty" id="Penalty" placeholder="Penalty" value="<?php echo $getnewQuestions->penalty ?>" class="form-control" >
                                                                                              </div>
                                                                                            </div>
                                                                                          </div>
                                                                                        </div>
                                                                                        
                                                                                        <div class="row">
                                                                                          <div class="col-sm-12">
                                                                                            <input type="hidden" name="courseid" id="courseid" value="<?php echo $_GET['courseid'] ?>" class="form-control">
                                                                                          </div>
                                                                                        </div>
                                                                                        
                                                                                        <div class="row">
                                                                                          <div class="col-sm-12">
                                                                                            <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $_GET['id'] ?>" class="form-control">
                                                                                          </div>
                                                                                        </div>
                                                                                        
                                                                                        
                                                                                        <div class="modal-footer">
                                                                                          <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                                                                                          <button type="submit" class="btn btn-primary btn-outline-primary ">Save </button>
                                                                                        </div></center>
                                                                                      </form>
                                                                                    </div>
                                                                                  </div>
                                                                                </div>
                                                                            </div>
                                                                               <!---==edit==--> 
                                                                        </div>
                                                                       
                                                                        
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                </div>
                            </div>
                          </form>
                        
                      </div>
                    </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
    
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    <script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
    
    <!-- sweet alert js -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- sweet alert modal.js intialize js -->
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
    </script>
    
    <script>
    function OptionType(type) {
    if(type == 1 || type == 2){
    document.getElementById("single").style.display = "block";
    document.getElementById("blank").style.display = "none";
    document.getElementById("para").style.display = "none";
    }else if(type == 3){
    
    document.getElementById("single").style.display = "none";
    document.getElementById("blank").style.display = "block";
    document.getElementById("para").style.display = "none";
    }else if(type == 4){
    document.getElementById("single").style.display = "none";
    document.getElementById("blank").style.display = "none";
    document.getElementById("para").style.display = "block";
    }
    
    }
    </script>
    <script>
    function OptionType1(type) {
    if(type == 1 || type == 2){
    document.getElementById("editsingle").style.display = "block";
    document.getElementById("editeditblank").style.display = "none";
    document.getElementById("editpara").style.display = "none";
    }else if(type == 3){
    
    document.getElementById("editsingle").style.display = "none";
    document.getElementById("editblank").style.display = "block";
    document.getElementById("editpara").style.display = "none";
    }else if(type == 4){
    document.getElementById("editsingle").style.display = "none";
    document.getElementById("editblank").style.display = "none";
    document.getElementById("editpara").style.display = "block";
    }
    
    }
    </script>
    <script >
    
    function edititem_test(){
    $(':input').prop('readonly', false);
    document.getElementById("save_item").style.display = "block";
    document.getElementById("edit_item").style.display = "none";
    }
    
    //$(document).ready(function(){
    //var i=1;
    
   // $('#add').click(function(){
   // if(i<=5){
   // i++;
   // $('#single').append('<br><br><br><div class="col-sm-12" id="row'+i+'"><div class="row"><div class="col-md-1"> <input type="checkbox" name="option'+i+'" id="option'+i+'" value='1' class="form-control" ></div><div class="col-3">Correct Option '+i+'</div><div class="col-7"><input type="text" placeholder="Option" name="optionText'+i+'" id="optionText'+i+'" maxlength="255" rows="2"class="form-control max-textarea" ></div><div class="col-1"><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button></div></div><br>');
   // }
   // else{
   // alert('You cannot Add More than 6 Options');
   // }
  //  });
    
   // $(document).on('click', '.btn_remove', function(){
   // var button_id = $(this).attr("id");
  //  $('#row'+button_id+'').remove();
  //  });
    
   // });
    </script>
    <script>
        $(document).ready(function(){      
      var i=1;  
   
      $('#add').click(function(){  
        if(i<=5){ 
           i++;  
           $('#single').append('<br><br><div class="col-sm-12" id="row'+i+'"><div class="row"><div class="col-1"><input type="checkbox" name="option'+i+'" id="option'+i+'" value="1" class="form-control" ></div><div class="col-3">Correct Option '+i+'</div><div class="col-7"><input type="text" placeholder="Option" name="optionText'+i+'" id="optionText'+i+'" maxlength="255" rows="2"class="form-control max-textarea" ></div><div class="col-1"><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button></div></div></div><br>');  
        }
   else{
   alert('You cannot Add More than 6 Options');
    }
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
    </script>
    <script>
        $(document).ready(function(){      
      var i=1;  
   
      $('#edit').click(function(){  
        if(i<=5){ 
           i++;  
           $('#editsingle').append('<br><div class="col-sm-12" id="row'+i+'"><div class="row"><div class="col-1"><input type="checkbox" name="option'+i+'" id="option'+i+'" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?> class="form-control" ></div><div class="col-2">Correct Option '+i+'</div><div class="col-6"><input type="text" placeholder="Option" name="optionText'+i+'" id="optionText'+i+'" maxlength="255" rows="2" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>"class="form-control max-textarea" ></div><div class="col-1"><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button></div></div></div><br>');  
        }
   else{
   alert('You cannot Add More than 6 Options');
    }
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
    </script>
    
    
    <script>
		    function alert_del_msg(quiztestid,questionid,CourseId)
		    {
                swal({
                  title: "Are you sure?",
                  text: "You wants to delete this item !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willEdit) => {
                  if (willEdit) {
                    /*swal("Poof! Your Course has been deleted!", {
                      icon: "success",
                    });*/
                    location.href = '<?php echo base_url() ?>Newquiztest/deletequestion?quiztestid='+quiztestid+'&questionid='+questionid+'&courseid='+CourseId;
                  } else {
                    swal("User is not edited!");
                  }
                });
            }
		</script>
		
  </body>
</html>