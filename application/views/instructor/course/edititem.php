<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();
foreach($metadata as $getmetadata){
$meta_description = $getmetadata->description;
$meta_keywords = $getmetadata->keywords;
}
$classData = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1 AND type = 'Live Class' AND end_session = 0 ORDER BY 1 DESC")->result();
foreach($classData as $getclassData){
$classId= $getclassData->id;
}
?>
<?php
/*====================Course data===========================*/
$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
	$courseTitle = $getcourseData->title;
	$courseImage = $getcourseData->image;
	
}
/*============================================================*/
/*==================Sub Chapters Data=====================*/
$subChapterId = $_GET['itemid'];
$subChapterData = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND id= '$subChapterId' ORDER BY 1 DESC")->result();
foreach($subChapterData as $getsubChapterData){
$type = $getsubChapterData->type;
	$subChapterNameItem = $getsubChapterData->subchapter_name;
	$subtype = $getsubChapterData->sub_type;
	$path = $getsubChapterData->path;
	$availability = $getsubChapterData->availability;
	$available_from = $getsubChapterData->available_from;
	$available_to = $getsubChapterData->available_to;
	$available_date = $getsubChapterData->available_date;
	$available_time = $getsubChapterData->available_time;
	
$available_time = date('h:i:s a', strtotime($available_time));
$endsession =$getsubChapterData->end_session;
	
	$subPreClassMsg = $getsubChapterData->pre_class_msg;
	$subPostClassMsg = $getsubChapterData->post_class_msg;
	$showRecording = $getsubChapterData->show_recording;
	$emailPush = $getsubChapterData->email_push;
	$mobilePush = $getsubChapterData->mobile_push;
	$webPush = $getsubChapterData->web_push;
}
/*==========================================================*/
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Edit Item </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- sweet alert framework -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<style>
        .blink_me {
            animation: blinker 1s linear infinite;
            color: red;
        }
        @keyframes blinker {
            80% {
                opacity: 0;
            }
        }
    </style>
	</head>
	<body>
		<?php if($this->session->flashdata('endmsg')){ ?>
		<script>
		$(document).ready(function(){
			$("#myModal").modal('show');
		});
		</script>
		<div id="myModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Message</h5>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<b style="color:green"><?php echo $this->session->flashdata('endmsg') ?></b>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($this->session->flashdata('delmsg')){ ?>
		<script>
		$(document).ready(function(){
			$("#myModal").modal('show');
		});
		</script>
		<div id="myModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Message</h5>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<b style="color:green"><?php echo $this->session->flashdata('delmsg') ?></b>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($this->session->flashdata('pdfmsg')){ ?>
		<script>
		$(document).ready(function(){
			$("#myModal").modal('show');
		});
		</script>
		<div id="myModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Message</h5>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<b style="color:green"><?php echo $this->session->flashdata('pdfmsg') ?></b>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($this->session->flashdata('msg')){ ?>
		<script>
		$(document).ready(function(){
			$("#myModal").modal('show');
		});
		</script>
		<div id="myModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Message</h5>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<b style="color:green"><?php echo $this->session->flashdata('msg') ?></b>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper" style="width:100%;">
						<?php include("includes/coursesidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="row" >
									<div class="col-sm-1"></div>
									<div class="main-body col-sm-11">
										<div class="page-wrapper">
											<!-- Page-header start -->
											<div class="page-header">
												<div class="row align-items-end">
													<div class="col-lg-12">
														<div class="page-header-title">
															<div class="d-inline">
																<h4><?php echo $courseTitle ?></h4>
																<!--<span>23 Course(s)</span>-->
															</div>
														</div>
													</div>
													
													
												</div>
											</div>
											<!-- Page-header end -->
											<!-- Page-body start -->
											
											
											<div class="page-body">
												
												<div class="card">
													<div class="card-body">
														<form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off" enctype="multipart/form-data">
															
															<?php if($type == 'Pdf'){ ?>
															
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="text" name="chaptername" id="chaptername" value="<?php echo $subChapterNameItem ?>" placeholder="Title" class="form-control" readonly required>
																</div>
															</div>
															
															<br>
															<label class="radio-inline">
																<input type="radio" name="pdf_sub_type" id="upload_file" value="upload" <?php if($subtype == 'upload'){ ?> checked <?php } ?> onclick="Pdf()"> Upload
															</label>
															<label class="radio-inline">
																<input type="radio" name="pdf_sub_type" id="upload_link" value="public_url" <?php if($subtype == 'public_url'){ ?> checked <?php } ?> onclick="Pdf()"> Public URL
															</label>
															<br>
															<div class="row">
																<div class="col-sm-12">
																	<input type="file" name="pdf_file" id="pdf_file" value="<?php echo $path ?>" class="form-control" readonly style="display:none;">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="text"  name="pdf_link" id="pdf_link" value="<?php echo $path ?>" placeholder="Paste Link Here" class="form-control" style="display:none;" readonly>
																</div>
															</div>
															<br>
															<label class="radio-inline">
																<input type="radio" name="availability" id="always_available" value="1" onclick="Availability_pdfurl()" <?php if($availability == '1'){ ?> checked <?php } ?> > Always Available
															</label>
															<label class="radio-inline">
																<input type="radio" name="availability" id="time_based" value="2" onclick="Availability_pdfurl()" <?php if($availability == '2'){ ?> checked <?php } ?>> Time Based
															</label>
															<br>
															<?php if($availability == '2'){ ?>
															<div class="row">
																<div class="col-sm-6">
																	<input type="text" name="available_from" id="available_from" min="<?php echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_from ?>"  onfocus="this.type='date'" placeholder="Available From" class="form-control"  readonly>
																</div>
																<div class="col-sm-6">
																	<input type="text" name="available_to" id="available_to" min="<?php echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_to ?>" onfocus="this.type='date'" placeholder="Available To" class="form-control"  readonly>
																</div>
															</div>
															<?php } ?>
															
															
															<br>
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="pre_class_msg" id="pre_class_msg" value="<?php echo $subPreClassMsg ?>" placeholder="Pre Class Message" class="form-control" readonly>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="post_class_msg" id="post_class_msg" value="<?php echo $subPostClassMsg ?>" placeholder="Post Class Message" class="form-control" readonly>
																</div>
															</div>
															
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="type" id="type" value="Pdf" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="subchapterid" id="subchapterid" value="<?php echo $_GET['itemid'] ?>" class="form-control">
																</div>
															</div>
															<?php }else if($type == 'Heading'){ ?>
															<div class="row">
																<div class="col-sm-12">
																	<input type="text" name="chaptername" id="chaptername" value="<?php echo $subChapterNameItem ?>" placeholder="Title" class="form-control" readonly required>
																</div>
															</div>
															
															
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="type" id="type" value="Heading" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="subchapterid" id="subchapterid" value="<?php echo $_GET['itemid'] ?>" class="form-control">
																</div>
															</div>
															<?php }else if($type == 'Live Class'){ ?>
															
															<div class="row">
																<div class="col-sm-3">
																	<label class="radio-inline">
																		<input type="radio" name="sub_type" id="bbb" value="bbb" <?php if($subtype == 'bbb'){ ?> checked <?php } ?> > Inbuilt Live
																	</label>
																</div>
																<div class="col-sm-3">
																	<label class="radio-inline">
																		<input type="radio" name="sub_type" id="jitsi" value="jitsi" <?php if($subtype == 'jitsi'){ ?> checked <?php } ?>> Jitsi
																	</label>
																</div>
																<div class="col-sm-3">
																	<label class="radio-inline">
																		<input type="radio" name="sub_type" id="zoom" value="zoom" <?php if($subtype == 'zoom'){ ?> checked <?php } ?>> Zoom
																	</label>
																</div>
															</div>
															<hr>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="text" name="chaptername" id="chaptername" value="<?php echo $subChapterNameItem ?>" placeholder="Title" class="form-control" readonly required>
																</div>
															</div>
															
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label class="radio-inline">
																		<input type="radio" name="availability" id="time_based" value="2" onclick="Availability_live()" <?php if($availability == '2'){ ?> checked <?php } ?>> Time Based
																	</label>
																</div>
																<br>
																<div class="col-md-5">
																	<input type="text" name="available_date" id="available_date" min="<?php echo date('Y-m-d') ?>"value="<?php echo $available_date ?>"   onfocus="this.type='date'"  placeholder="Available From Date" class="form-control" readonly >
																</div>
																
																<div class="col-md-5">
																	<input type="text" name="available_time" id="available_time" value="<?php echo $available_time ?>"   onfocus="this.type='time'" min="09:00" max="19:00" placeholder="Available From Time" class="form-control" readonly >
																	
																</div>
																<div class="col-md-4">
																	<input type="hidden" name="available_to" id="available_to"   onfocus="this.type='date'" placeholder="Available To" class="form-control" style="display:none;" readonly>
																</div>
																<div class="col-md-4">
																	<input type="hidden" name="available_to" id="available_to"   onfocus="this.type='time'" placeholder="Available To" class="form-control" style="display:none;" readonly>
																</div>
																
															</div>
															
															<br>
															<div class="row">
																<div class="col-sm-12">
																	
																	<input type="text" name="pre_class_msg" id="pre_class_msg" value="<?php echo $subPreClassMsg ?>" placeholder="Pre Class Message" class="form-control" readonly>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-sm-12">
																	<input type="text" name="post_class_msg" id="post_class_msg" value="<?php echo $subPostClassMsg ?>" placeholder="Post Class Message" class="form-control" readonly>
																</div>
															</div>
															<br>
															<div class="row">
																<div class='col-sm-12'>
																	<b>Show Recording (if available) to Learner:</b>&nbsp;&nbsp;&nbsp;&nbsp;
																	<label class="radio-inline">
																		<input type="radio" name="is_record" id="is_record_yes" value="1" <?php if($showRecording == '1'){ ?> checked <?php } ?>> Yes
																	</label>
																	<label class="radio-inline">
																		<input type="radio" name="is_record" id="is_record_no" value="2" <?php if($showRecording == '2'){ ?> checked <?php } ?>> No
																	</label>
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<b>Notify Users on Class Start:</b>&nbsp;&nbsp;&nbsp;&nbsp;
																	<label class="radio-inline">
																		<input type="checkbox" name="email_push" id="email_push" value="1" <?php if($emailPush == '1'){ ?> checked <?php } ?>> Email
																	</label>
																	<label class="radio-inline">
																		<input type="checkbox" name="mobile_push" id="mobile_push" value="1" <?php if($mobilePush == '1'){ ?> checked <?php } ?>> Mobile Push
																	</label>
																	<label class="radio-inline">
																		<input type="checkbox" name="web_push" id="web_push" value="1" <?php if($webPush == '1'){ ?> checked <?php } ?>> Web push
																	</label>
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="type" id="type" value="Live Class" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																	<input type="hidden" name="subchapterid" id="subchapterid" value="<?php echo $_GET['itemid'] ?>" class="form-control">
																</div>
															</div>
															<br>
															
															<?php } ?>
															<br>
															
															<?php if($type == 'Live Class'){  ?>
															<?php if($getsubChapterData->end_session == 0){ ?>
															<?php if($subtype == 'zoom'){
															$zoomData = $this->db->query("SELECT * FROM zoom_live_class WHERE status = 1  AND sub_chapter_id= '".$_GET['itemid']."'
															AND course_id = '".$_GET['courseid']."' AND chapter_id = '".$_GET['chapterid']."'  ORDER BY 1 DESC")->result();
															foreach($zoomData as $getZoomData){
															$zoomstartlink = $getZoomData->start_url;
															}
															?>
															<a href="<?php echo $zoomstartlink ?>" target="_blank"><button type="button" name="launch" id="launch" class="btn btn-primary btn-outline-primary" >Launch</button></a>
															<?php }else if($subtype == 'bbb'){?>
															 <a href="<?php echo base_url() ?>Live?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $_GET['chapterid'] ?>&itemid=<?php echo $_GET['itemid'] ?>&type=<?php echo $subtype ?>&sub_chapter_id=<?php echo $subChapterId ?>" target="_blank"><button type="button" name="launch" id="lanch" class="btn btn-primary btn-outline-primary" >Launch</button></a>
															<?php }else{?>
															<a href="<?php echo base_url() ?>Live?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $_GET['chapterid'] ?>&itemid=<?php echo $_GET['itemid'] ?>" target="_blank"><button type="button" name="launch" id="launch" class="btn btn-primary btn-outline-primary" >Launch</button></a>
															<?php }?>
															<button type="button" name="edit_item" id="edit_item" onclick="edititem_live(this)"  class="btn btn-primary btn-outline-primary">Edit</button>
															<button type="submit" name="save_item" id="save_item" onclick="alert('Are you sure, You want to Modify Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															<button onclick ="alert_del_msg('<?= $subChapterId ?>','<?= $CourseId ?>')"value="<?= $subChapterId ?>" type="button" name="deleteitem" id="deleteitem"  class="btn btn-primary btn-outline-primary">Delete</button>
															<button onclick ="alert_end_msg('<?= $subChapterId ?>','<?= $CourseId ?>')"value="<?= $subChapterId ?>" type="button" name="endclass" id="endclass"  class="btn btn-primary btn-outline-primary">End Class</button>
															<?php }else{ ?>
															<?php
															$subChapterData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$subChapterId' ORDER BY 1 DESC")->result();
															foreach($subChapterData as $getsubChapterData){
															$existinglink = $getsubChapterData->link;
															}
															if(count($subChapterData) > 0){ ?>
															<a href="<?php echo base_url() ?>Recording?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $_GET['chapterid']?>&itemid=<?php echo $_GET['itemid']?>"><button class="btn btn-primary btn-outline-primary" type="button" >View Recording</button></a>
															<button class="btn btn-primary btn-outline-primary" type="button" data-toggle="modal" data-target="#recording_link">Edit Recording</button>
															<?php }else{ ?>
															<button class="btn btn-primary btn-outline-primary" type="button" data-toggle="modal" data-target="#recording_link">Upload recorded Video</button>
															<p class="blink_me">Your Live Class has been ended. Please check assets library to watch its recording.!</p>
															<?php } ?>
															<?php } ?>
															
															<?php }else{ ?>
															<button type="button" name="edit_item" id="edit_item" onclick="edititem_pdfurl(this)"  class="btn btn-primary btn-outline-primary">Edit</button>
															<button type="submit" name="save_item" id="save_item" onclick="alert('Are you sure, You want to Modify Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															<button onclick ="alert_del_msg('<?= $subChapterId ?>','<?= $CourseId ?>')"value="<?= $subChapterId ?>" type="button" name="deleteitem" id="deleteitem"  class="btn btn-primary btn-outline-primary">Delete</button>
															<?php } ?>
														</form>
													</div>
												</div>
												
											</div>
											
											<!-- Page-body end -->
										</div>
									</div>
								</div>
							</div>
							<!-- Main-body end -->
							<!-- <div id="styleSelector">
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Recorded Vdo Link -->
		<div class="modal fade" id="recording_link" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Paste Your Link Here</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form method="post" action="<?php echo base_url(); ?>Newchapter/addrecordinglink">
						<div class="modal-body">
							<div class="row">
								<input type="hidden" name="itemid" id="itemid" value="<?php echo $_GET['itemid'] ?>">
								<input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>">
								<input type="hidden" name="courseid" id="courseid" value="<?php echo $_GET['courseid'] ?>">
								
								<div class="col-sm-12">
									<input type="text" name="recorded_link" id="recorded_link" value="<?php echo $existinglink ?>" placeholder="Paste your Link Here" class="form-control"  required>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary btn-outline-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary btn-outline-primary">Save and Submit</button>
						</div>
					</form>
					
				</div>
			</div>
		</div>
		
		<!-- Warning Section Starts -->
		<!-- Older IE warning message -->
		
		<!-- Warning Section Ends -->
		<!-- Required Jquery -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
		<!-- jquery slimscroll js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
		<!-- modernizr js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
		<!-- data-table js -->
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
		<!-- Bootstrap date-time-picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Date-dropper js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
		<!-- Color picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
		<!-- Mini-color js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
		<!-- i18next.min.js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
		<!-- Custom js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
		
		<!-- sweet alert js -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<!-- sweet alert modal.js intialize js -->
		<!-- modalEffects js nifty modal window effects -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-23581568-13');
		</script>
		<script>
		
		if ($("#upload_file").is(":checked")) {
		// do something
		$('#pdf_file').prop('required',true);
		document.getElementById("pdf_link").style.display = "none";
		document.getElementById("pdf_file").style.display = "block";
		}else if ($("#upload_link").is(":checked")) {
		// do something
		$('#pdf_link').prop('required',true);
		document.getElementById("pdf_link").style.display = "block";
		document.getElementById("pdf_file").style.display = "none";
		}
		
		if ($("#always_available").is(":checked")) {
		// do something
		/*document.getElementById("available_from").style.display = "none";*/
		document.getElementById("available_date").style.display = "none";
		document.getElementById("available_time").style.display = "none";
		document.getElementById("available_to").style.display = "none";
		}else if ($("#time_based").is(":checked")) {
		// do something
		/*  $('#available_from').prop('required',true);*/
		$('#available_date').prop('required',true);
		$('#available_time').prop('required',true);
		$('#available_to').prop('required',true);
		/* document.getElementById("available_from").style.display = "block";*/
		document.getElementById("available_date").style.display = "block";
		document.getElementById("available_time").style.display = "block";
		document.getElementById("available_to").style.display = "block";
		}
		
		function Pdf(){
		if ($("#upload_file").is(":checked")) {
		// do something
		$('#pdf_file').prop('required',true);
		document.getElementById("pdf_link").style.display = "none";
		document.getElementById("pdf_file").style.display = "block";
		}else if ($("#upload_link").is(":checked")) {
		// do something
		$('#pdf_link').prop('required',true);
		document.getElementById("pdf_link").style.display = "block";
		document.getElementById("pdf_file").style.display = "none";
		}
		}
		
		function Availability_live(){
		
		if ($("#always_available").is(":checked")) {
		// do something
		
		
		document.getElementById("available_date").style.display = "none";
		document.getElementById("available_time").style.display = "none";
		}else if ($("#time_based").is(":checked")) {
		
		// do something
		$('#available_date').prop('required',true);
		$('#available_time').prop('required',true);
		document.getElementById("available_date").style.display = "block";
		document.getElementById("available_time").style.display = "block";
		}
		}
		
		function Availability_pdfurl(){
		
		if ($("#always_available").is(":checked")) {
		// do something
		
		document.getElementById("available_from").style.display = "none";
		document.getElementById("available_to").style.display = "none";
		}else if ($("#time_based").is(":checked")) {
		
		// do something
		$('#available_from').prop('required',true);
		$('#available_to').prop('required',true);
		document.getElementById("available_from").style.display = "block";
		document.getElementById("available_to").style.display = "block";
		}
		}
		
		/*=========remove readonly attribute ===============*/
		function edititem_live (edititemid) {
		edititemid.style.display = "none";
		document.getElementById('save_item').style.display = "block";
		document.getElementById('chaptername').removeAttribute('readonly');
		document.getElementById('available_date').removeAttribute('readonly');
		document.getElementById('available_time').removeAttribute('readonly');
		document.getElementById('available_to').removeAttribute('readonly');
		
		
		document.getElementById('pre_class_msg').removeAttribute('readonly');
		document.getElementById('post_class_msg').removeAttribute('readonly');
		document.getElementById('pdf_file').removeAttribute('readonly');
		document.getElementById('pdf_link').removeAttribute('readonly');
		
		/*	document.getElementById('available_from').removeAttribute('readonly');*/
		
		};
		
		function edititem_pdfurl (edititemid) {
		edititemid.style.display = "none";
		document.getElementById('save_item').style.display = "block";
		document.getElementById('chaptername').removeAttribute('readonly');
		
		document.getElementById('available_to').removeAttribute('readonly');
		
		document.getElementById('pdf_file').removeAttribute('readonly');
		document.getElementById('pdf_link').removeAttribute('readonly');
		
		document.getElementById('available_from').removeAttribute('readonly');
		
		};
		/*=================================================*/
		
		</script>
		<script>
		function alert_del_msg(subChapterId,courseId)
		{
		swal({
		title: "Are you sure?",
		text: "You wants to delete this item !",
		icon: "warning",
		buttons: true,
		dangerMode: true,
		})
		.then((willEdit) => {
		if (willEdit) {
		/*swal("Poof! Your Course has been deleted!", {
		icon: "success",
		});*/
		location.href = '<?php echo base_url() ?>EditItem/edititem?itemid='+subChapterId+'&courseid='+courseId;
		} else {
		swal("User is not edited!");
		}
		});
		}
		</script>
		<script>
		function alert_end_msg(subChapterId,courseId)
		{
		swal({
		title: "Are you sure?",
		text: "You wants to End this class !",
		icon: "warning",
		buttons: true,
		dangerMode: true,
		})
		.then((willEdit) => {
		if (willEdit) {
		/*swal("Poof! Your Course has been deleted!", {
		icon: "success",
		});*/
		location.href = '<?php echo base_url() ?>EditItem/endclass?itemid='+subChapterId+'&courseid='+courseId;
		} else {
		swal("User is not edited!");
		}
		});
		}
		</script>
		<script>
		
          jQuery( function() {
            jQuery( '#available_date' ).datepicker({ 
                format: 'yyyy-mm-dd'
            });
            
        
          } );
          </script>
	</body>
</html>