<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$edit=$_GET['edit_id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || Edit User </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="description" content="<?php echo $meta_description?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
   <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
 
</head>

<body>
    <?php if($this->session->flashdata('lmsg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('lmsg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/learnersidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-9">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Users</h4>
                                                    <?php $record = $this->session->userdata('record'); ?>
                                                    <span><?php echo $record; ?> Admins</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-header end -->
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                       
                                            <div id="updateProfile" class="container col-9 col-md-12">
                                                <!--<center><b style="color:green;"><?php echo $this->session->flashdata('lmsg'); ?></b></center>-->
                                                </div>
		                                    <h4 class="s-title">Profile Details</h4>
		                                    <?php foreach($user as $u){?>
		                                    <div class="row">
		                                        <div class=col-md-4></div>
		                                         <div class=col-md-4>
		                                             
		                                         </div>
		                                          <div class=col-md-4></div>
		                                        </div>
		                                     <form method="post" action="<?php base_url('Users/edit_user') ?>">
                                                <div class="form-row">
                                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Name:</h4>
                                                <input type="text" class="form-control" name="name" value="<?php echo $u['username'] ?>" placeholder="User Name" required>
                                            </div>
                                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Email:</h4>
                                                <input type="email" disabled class="form-control" name="email" value="<?php echo $u['email_id'] ?>" placeholder="Email Id">
                                            </div>
					                        </div>
					                        <div class="form-row">
					                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Date of Birth:</h4>
                                                <input type="date" class="form-control" name="d_o_b" value="<?php echo $u['dob'] ?>" max="<?php echo date('Y-m-d'); ?>" required >
                                            </div>
					                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Mobile:</h4>
                                                <input type="tel" class="form-control" name="mobile" value="<?php echo $u['mobile'] ?>" maxlength='10' placeholder="Mobile" required>
                                                </div>
                                                </div>
                                                <div class="form-row">
                                                <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">City:</h4>
                                                <input type="text" class="form-control" name="city" value="<?php echo $u['city'] ?>" placeholder="City" required>
                                                </div>
                                                
                                                <div class="form-group col-sm-12 col-xl-6 m-b-30">
						                        <h4 class="sub-title">Role</h4>
						                        <select  class="form-select form-control" id="role" required name="role" data-value="<?php echo $u['role'] ?>" required>
    						                      <option selected value="3">Learner</option>
    						                      <option value="2">Instructor</option>
    						                      <option value="1">Admin</option>
						                        </select>
					                            </div>
					                            
					                            </div>
					                            <div class ="form-row">
					                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
						                        
						                        <h4 class="sub-title">Profile Picture:</h4>
                                                      <input type='file' name='picture' id='file' class='form-control btn btn-light'  >
					                            </div>
					                            </div>
					                        </div>
					                        <div class="from-group">
					                            <input type="submit" class="btn btn-primary btn-outline-primary" name="submit">
                                            </div>
                                    </form>
                                    </div>
                                    <br>
                                     <div id="loginDetails" class="container">
		                                <h4 class="s-title">Login Details</h4>
		                               <?php $user_info = $this->session->userdata($session_data);
		                               $login_count = $user_info['login_count'];
		                               ?>
		                               </div>
		                               <br>
		                               <div class="row">
		                                <div class="col-md-3">Number of Logins</div><div class=" col-md-9 text-primary"><?php echo $u['login_count']; ?></div>
		                                </div>
		                                <div class="row">
		                                <div class="col-md-3">Last Login Date</div><div class="column col-9 text-primary"><?= date ( "d-m-Y" , strtotime ($u['last_login']))?></div>
		                             </div>
		                             <div class="row">
		                             <div class="col-md-12">Logged devices</div>
		                             </div>
		                             <br>
		                             <div class="row">
		                                <?php if(count($device) > 0 ) { ?>
		                                <?php foreach($device as $d) { ?>
		                                
				                     <div class="column col-6 col-md-6 mb-2 deviceCont">
			 	                     <div class="tile bg-primary" style="padding:10px;border:1px solid #ccc;border-radius: 10px;">
			 	                         <?php $id = $d['id']; ?>
			 	                         
				                     <button class="btn-primary circle "  onclick ="alert_blk_msg('<?= $id ?>','<?= $edit ?>')" value="<?= $id ?>" name="block" id="block" data-toggle="tooltip" data-placement="top" title="Block">&times;</button>
				                     <div class="tile-icon"><i class="material-icons avatar avatar-xl deviceType" data-name="Realme RMX1851 29" data-id="v1-507f70dfe320dcfd"></i></div>
              		                 <div class="tile-content">
                			         <p class="tile-title"> <b class="text-info"><?=$d['login_count']. '   Times' ?></b><br><small class="text-italic"><?= $d['device_info'] ?></small></p>
                			         <p class="tile-subtitle text-gray mb-0">Last login : <?= date ( "d-m-Y" , strtotime ($u['last_login']))?> </p>
              		                 </div>
            		                 </div>
				                     </div>
				                     
				                     <?php } ?>
		                             </div>
		                      <?php  } else{ ?>
				                    <div class="column col-9 text-primary">No logged device</div>
				                    <?php } ?>
		                             <br>
		                             <div id="loginDetails" class="container">
		                                <h4 class="s-title">Change Password</h4>
		                                <br>
		                                <form method="post" action="<?php base_url('Users/edit_user') ?>">
                                                <div class="form-row">
                                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">New Password:</h4>
                                                <input type="text" class="form-control" name="new_pass" placeholder="New Password" required>
                                            </div>
                                            <div class="form-group col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Confirm Password:</h4>
                                                <input type="text" class="form-control" name="conf_pass" placeholder="Confirm Password">
                                            </div>
					                        </div>
					                        <div class="from-group">
					                            <input type="submit" class="btn btn-primary btn-outline-primary" name="passubmit">
                                            </div>
                                        </form>
					                        
		                             </div>
		                             <br>
		                             <div id="loginDetails" class="container">
		                                <h4 class="s-title">Referral Details</h4>
		                                <div class="row">
		                                <div class="col-md-3">Refer Code:</div><div class=" col-md-9 text-primary"><?php echo $u['unique_id']; ?></div>
		                                <?php }?>
		                                </div>
		                             </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
            <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>	
<script>
function alert_blk_msg(Id,edit){
		        swal({
                  title: "Are you sure?",
                  text: "You wants to delete this Device !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willBlock) => {
                  if (willBlock) {
                
                 /** const edit = urlParams.get('edit_id');
                    /*swal("Poof! Your Course has been deleted!", {
                      icon: "success",
                    });*/
                    location.href = '<?php echo base_url() ?>Users/edit_user/?dblockid='+Id+'&edit_id='+edit;
                
                  } else {
                    swal("Device is safe!");
                  }
                });
		    }
</script>
</body>

</html>
