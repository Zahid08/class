<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();
foreach($metadata as $getmetadata){
$meta_description = $getmetadata->description;
$meta_keywords = $getmetadata->keywords;
}
?>
<?php
date_default_timezone_set("Asia/Calcutta");
$ItemId = $_GET['itemid'];
$classData = $this->db->query("SELECT * FROM sub_chapters s INNER JOIN course c ON c.course_id = s.course_id WHERE s.status = 1 AND s.type = 'Live Class' ORDER BY 1 DESC")->result();
foreach($classData as $getclassData){
}
$ItemId = $_GET['itemid'];
$subChapterData = $this->db->query("SELECT * FROM live_class_recordings WHERE sub_chapter_id=  $getclassData->id ORDER BY 1 DESC")->result();
foreach($subChapterData as $getsubChapterData){
}
$totalclassData = $this->db->query("SELECT count(1) as totalclassData FROM sub_chapters s INNER JOIN course c ON c.course_id = s.course_id WHERE s.status = 1 AND s.type = 'Live Class' ORDER BY 1 DESC")->result();
foreach($totalclassData as $gettotalclassData){
$totalclassData = $gettotalclassData->totalclassData;
}




if(isset($_POST['liveclass_search'])){
    $where = "where 1 = 1 ";
    if(!empty($_POST['course_title'])){
        $courseTitle = $_POST['course_title'];
        $where .= " AND c.title = '$courseTitle'";
    }
    
    if(!empty($_POST['chapter_title'])){
        $chapter_title = $_POST['chapter_title'];
        $where .= " AND s.subchapter_name = '$chapter_title'";
    }
    
    
    
    
   $classData = $this->db->query("SELECT * FROM sub_chapters s INNER JOIN course c ON c.course_id = s.course_id $where AND s.status = 1 AND s.type = 'Live Class'
    ORDER BY 1 DESC")->result();

} 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Competitive Exam Guide || Live Class </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?php echo $meta_description?>">
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="author" content="#">
        <!-- Favicon icon -->
        <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
        <!-- Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
        <!-- Required Fremwork -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
        <!-- themify-icons line icon -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
        <!-- ico font -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
        <!-- feather Awesome -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
        <!-- Date-time picker css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
        <!-- Date-range picker css  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
        <!-- Date-Dropper css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
        <!-- Color Picker css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
        <!-- Mini-color css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
        <!-- Data Table Css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
        <!-- Style.css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    </head>
    <body>
        <!-- Pre-loader start -->
        <div class="theme-loader">
            <div class="ball-scale">
                <div class='contain'>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                    <div class="ring">
                        <div class="frame"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pre-loader end -->
        <div id="pcoded" class="pcoded">
            <div class="pcoded-overlay-box"></div>
            <div class="pcoded-container navbar-wrapper">
                <?php include("includes/header.php"); ?>
                <!-- Sidebar inner chat end-->
                <div class="pcoded-main-container">
                    <div class="pcoded-wrapper">
                        <?php include("includes/sidenav.php"); ?>
                        <div class="pcoded-content">
                            <div class="pcoded-inner-content">
                                <!-- Main-body start -->
                                <div class="main-body">
                                    <div class="page-wrapper">
                                        <!-- Page-header start -->
                                        <div class="page-header">
                                            <div class="row align-items-end">
                                                <div class="col-lg-9">
                                                    <div class="page-header-title">
                                                        <div class="d-inline">
                                                            <h4>Live Classes (<?php echo $totalclassData ?>)</h4>
                                                            <!--<span>Live Classes[<b><?php echo $totalclassData ?></b>]</span>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Page-header end -->
                                        <!-- Page-body start -->
                                        <div class="page-body">
                                            <form method="post">
                                            <div class="row">
                                                
                                                <div class="col-sm-12 col-xl-6 m-b-30">
                                                    <h4 class="sub-title">Course Title:</h4>
                                                    <input type="text" class="form-control" name="course_title" placeholder="Course Title">
                                                </div>
                                                
                                                <div class="col-sm-12 col-xl-6 m-b-30">
                                                    <h4 class="sub-title">Chapter Title:</h4>
                                                    <input type="text" class="form-control" name="chapter_title" placeholder="Chapter Title">
                                                </div>
                                                
                                                <div class="col-sm-1">
                                                    <button type="submit" name="liveclass_search" id="liveclass_search" class="btn btn-primary btn-outline-primary">Search</button>
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="reset" class="btn btn-primary btn-outline-primary">Clear</button>
                                                </div>
                                                <div class="col-sm-12">
                                                    &nbsp;
                                                </div>
                                                
                                            </div>
                                            </form>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <!-- HTML5 Export Buttons table start -->
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <div class="dt-responsive table-responsive">
                                                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                    <thead>
                                                                        <tr>
                                                                            <!--<th>Created On</th>-->
                                                                            <th>Course</th>
                                                                            <th>Chapter</th>
                                                                            <th>Start Time</th>
                                                                            <th>Learners Watched</th>
                                                                            <th>Status</th>
                                                                            <th>Actions</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        foreach($classData as $getclassData){
                                                                        
                                                                        $liveRecordingData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$getclassData->id' ORDER BY 1 DESC")->result();
                                                                        foreach($liveRecordingData as $getliveRecordingData){
                                                                        $existinglink = $getliveRecordingData->link;
                                                                        }
                                                                        $subChapterType=$getclassData->sub_type;
                                                                        ?>
                                                                        <tr>
                                                                            <!--<td><?php echo $getclassData->created_date ?></td>-->
                                                                            <td><?php echo $getclassData->title ?></td>
                                                                            <td><?php echo $getclassData->subchapter_name ?></td>
                                                                            <td><?php echo $getclassData->available_date ." ". $getclassData->available_time ?></td>
                                                                            <td><?php
                                                                                echo !empty($getclassData->learner_watched)?$getclassData->learner_watched:0;
                                                                                
                                                                                ?>
                                                                            </td>
                                                                            <td><?php if($getclassData->available_date == date('Y-m-d') && $getclassData->available_time >= date('H:i') && $getclassData->end_session == 0) {?>
                                                                                <p>In Progress</p>
                                                                                <?php  }else if($getclassData->available_date < date('Y-m-d') && $getclassData->end_session == 0){ ?>
                                                                                <p>Upcoming</p>
                                                                                <?php  }else if($getclassData->end_session == 1 ){ ?>
                                                                                <p>Completed</p>
                                                                                <?php  }?>
                                                                            </td>
                                                                            <td>
                                                                                <?php if($getclassData->available_date <= date('Y-m-d') && $getclassData->end_session == 0) {?>
                                                                                <a href="<?php echo base_url() ?>Live?courseid=<?php echo $getclassData->course_id ?>&chapterid=<?php echo $getclassData->chapterid ?>&itemid=<?php echo $getclassData->id?>" target="_blank">
                                                                                    <button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Stream"><i class="icofont icofont-video"></i></button>
                                                                                </a>
                                                                                <a href="https://competitiveexamguide.com/zoom" target="_blank">
                                                                                    <button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Zoom Stream"><i class="icofont icofont-video"></i></button>
                                                                                </a>
                                                                                <?php  }else if($getclassData->end_session == 1 ){ ?>
                                                                                
                                                                                <!-- <button class="btn btn-inverse btn-outline-inverse btn-icon" data-target="#details_<?php echo $getclassData->id?>"  data-toggle="modal" data-placement="top" title="Download"><i class="icofont icofont-download"></i></button> -->
                                                                                <button class="btn btn-inverse btn-outline-inverse btn-icon" data-target="#details_<?php echo $getclassData->id?>"  data-toggle="modal" data-placement="top" title="Download"><i class="icofont icofont-download"></i></button>
                                                                                <?php  }?>
                                                                                <!-- <div class="modal fade" id="details_<?php echo $getclassData->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"> -->
                                                                                <div class="modal fade" id="details_<?php echo $getclassData->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
             <div class="modal-content">
<div class="modal-header">
                                        </div>
    <div class="modal-body">
                       <?php
if ($subChapterType=='bbb'){
        if ($existinglink && count($liveRecordingData) > 0){
                                ?>
    <iframe width="700" height="500" src="<?= $existinglink  ?> " frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <?php }else{ ?>
        <img class="img-fluid" src="<?php echo base_url() ?>assets/landing/images/blankVideo.png" alt="Theme-Logo">
                                <?php  } }else{ ?>
                <?php if($getrecordings->link==''){ ?>
         <center>
         <iframe width="700" height="500" src="<?= $getrecordings->path ?> " frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                 </center>
    <?php }else{ ?>
         <center>
      <iframe width="700" height="500" src="<?= $getrecordings->link ?> " frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
                <?php } ?>
        <?php } ?>                                                                    
    </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- HTML5 Export Buttons end -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Page-body end -->
                                    </div>
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <!-- <div id="styleSelector">
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Warning Section Starts -->
        <!-- Older IE warning message -->
        
        <!-- Warning Section Ends -->
        <!-- Required Jquery -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
        <!-- jquery slimscroll js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
        <!-- modernizr js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
        <!-- data-table js -->
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
        <!-- Bootstrap date-time-picker js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
        <!-- Date-range picker js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
        <!-- Date-dropper js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
        <!-- Color picker js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
        <!-- Mini-color js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
        <!-- i18next.min.js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
        <!-- Custom js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
        <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
        
        <script src="../files/assets/pages/sortable-custom.js"></script>
        <!-- Date-range picker js -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-23581568-13');
        </script>
    </body>
</html>