<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
   
?>
<?php 




$questions=$this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 " )->result(); 
foreach($questions as $getquestions){
    $questionsid=$getquestions->id;
   
}
 

$totalquestions=$this->db->query("SELECT count(1) as totalquestion FROM quiz_test_questions  ")->result();
foreach($totalquestions as $gettotalquestions){
$total_question = $gettotalquestions->totalquestion;
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || Quiz</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
	<meta name="keywords" content="<?php echo $meta_keywords ?>">
	<meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <style>
        .modal-lg {
         max-width: 90%;
         
         
         }
         
         .modal-body{
              max-width: 80%;
              margin:2%;
             
            
         }
    </style>
</head>

<body>
    <?php if($this->session->flashdata('delmsg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('delmsg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/sidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-4">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Quiz</h4>
                                                    <span>Quiz[<b><?php echo $total_question ?></b>]</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
													<button type="button" class="btn btn-primary btn-outline-primary" data-toggle="collapse" data-target="#demo">Advanced Search</button>
										</div>
                                        
                                        <div class="col-lg-3">

                                            <button type="button" name="addnewquestion" id="addnewquestion" data-toggle="modal" data-target="#addnewquestion_modal"  class="btn btn-primary btn-outline-primary "><i class="icofont icofont-plus"></i>Add New Question</button>
                                        </div>
                                        <div class="col-lg-2">
                                            <button class="btn btn-primary btn-outline-primary"><i class="icofont icofont-upload-alt"></i>Import Quiz/quiz Class</button>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- Page-header end -->

                                <div class="modal fade" id="addnewquestion_modal" tabindex="-1" role="dialog" >
                                                                              <div class="modal-dialog modal-lg" role="document" >
                                                                                <div class="modal-content">
                                                                                  <div class="modal-header">
                                                                                    <h4 class="modal-title">New Quiz Question</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                  </div>
                                                                                  <form  method="post" action="<?php echo base_url(); ?>Quiz/addquiztestQuestion" autocomplete="off" enctype="multipart/form-data">
                                                                                    <center><div class="modal-body" >
                                                                                      <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                          <div class="row">
                                                                                            <div class="col-2 float-right">
                                                                                                <lable class="float-left">Type</lable>
                                                                                              
                                                                                            </div>
                                                                                            <div class="col-10">
                                                                                              <select class="form-control" name="type" id="type" onchange="OptionType(this.value)" required>
                                                                                                <option name="type" value="1">Single Correct Option</option>
                                                                                                <option name="type" value="2">Multiple Correct Options</option>
                                                                                                <option name="type" value="3">Fill in the Blank</option>
                                                                                                <option name="type" value="4">Paragraph</option>
                                                                                                
                                                                                              </select>
                                                                                              
                                                                                            </div>
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>
                                                                                      
                                                                                      <div class="row" >
                                                                                        <div class="col-2">
                                                                                          <br> <lable class="float-left">Subject</lable>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                          <br>  <input type="text" name="subjectname" id="subjectname" placeholder="subject" class="form-control" required>
                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="row">
                                                                                        <div class="col-2">
                                                                                          <br><lable class="float-left">Topic</lable> 
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                          <br>  <input type="text" name="Topic" id="Topic" placeholder="Topic" class="form-control" required>
                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="row">
                                                                                        <div class="col-2">
                                                                                          <br> <lable class="float-left">Question Text</lable>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                          <br>  <textarea type="text" name="QuestionText" rows="4"id="QuestionText" placeholder="QuestionText" class="form-control max-textarea" required></textarea>
                                                                                        </div>
                                                                                      </div>
                                                                                      
                                                                                      <div id="single" class="row"  >
                                                                                        
                                                                                        <div class="col-lg-12">
                                                                                          <br><h4 class="btn btn-primary  btn-md btn-block   text-left m-b-20 ">Options</h4>
                                                                                        </div>
                                                                                        <div  class=" row col-sm-12" >
                                                                                          <div class="col-1 ">
                                                                                            <br><input type="checkbox" name="option1" id="option1" value="1" class="form-control " >
                                                                                          </div>
                                                                                          <div class="col-3">
                                                                                            <br> <lable class="float-left">Correct Option 1</lable>
                                                                                          </div>
                                                                                          <div class="col-7">
                                                                                            <br> <input type="text" name="optionText1" id="optionText1" placeholder="Option" class="form-control max-textarea" >
                                                                                            <br>
                                                                                          </div>
                                                                                          <div class="col-1" id="single" >
                                                                                            <br>   <button type="button" name="add" id="add" class="btn btn-primary btn-outline-primary btn-icon" title="Add Options"><h3>+</h3></button><br>
                                                                                          </div>
                                                                                        </div>
                                                                                        
                                                                                      </div>
                                                                                      <!--for Blank-->
                                                                                      <div id="blank" class="row" style="display:none;">
                                                                                        <div  class=" row col-sm-12" >
                                                                                          <div class="col-2">
                                                                                            <br><lable class="float-left">Answer</lable> 
                                                                                          </div>
                                                                                          <div class="col-10">
                                                                                            <br>  <input type="text" name="optionText7" id="optionText7" placeholder="Answer" class="form-control" >
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>
                                                                                      <!--for Paragraph-->
                                                                                      <div class="row " id="para" style="display:none">
                                                                                        <div  class=" row col-sm-12" >
                                                                                          <div class="col-2">
                                                                                            <br> <lable class="float-left">Answer</lable>
                                                                                          </div>
                                                                                          <div class="col-10">
                                                                                            <br>  <textarea type="text" name="optionText8" rows="4" id="optionText8" placeholder="Enter Your Text..." class="form-control max-textarea" ></textarea>
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>
                                                                                      
                                                                                      <div class="row">
                                                                                        <div class="col-2">
                                                                                          <br><lable class="float-left">Explaination</lable> 
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                          <br>  <textarea type="text" name="explaination" rows="4" id="explaination" placeholder="QuestionText" class="form-control max-textarea" required></textarea>
                                                                                        </div>
                                                                                      </div>
                                                                                      <div class="row">
                                                                                        <div class="col-lg-12">
                                                                                          <br><button type="button" data-toggle="collapse" data-target="#showadvancedoptions"  class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 " >Show Advanced Options</button>
                                                                                          <br>
                                                                                        </div>
                                                                                      </div>
                                                                                      
                                                                                      
                                                                                      <div  class="collapse"id="showadvancedoptions">
                                                                                        <div class="row">
                                                                                          <div class="col-2">
                                                                                            <br> <lable class="float-left">Order</lable>
                                                                                          </div>
                                                                                          <div class="col-10">
                                                                                            <br>  <input type="number" name="Order" id="Order" value="<?php echo $total_question+1 ?>"   class="form-control" >
                                                                                          </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                          <div class="col-2">
                                                                                            <br><lable class="float-left">Marks</lable>
                                                                                          </div>
                                                                                          <div class="col-10">
                                                                                            <br>  <input type="number" name="Marks" id="Marks" placeholder="Mark(s)"  class="form-control" >
                                                                                          </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                          <div class="col-2">
                                                                                            <br><lable class="float-left">Penalty</lable> 
                                                                                          </div>
                                                                                          <div class="col-10">
                                                                                            <br>  <input type="number" name="Penalty" id="Penalty" placeholder="Penalty" class="form-control" >
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>
                                                                                    </div>
                                                                                    
                                                                                    <div class="row">
                                                                                      <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                      </div>
                                                                                    </div>
                                                                                    
                                                                                    <div class="row">
                                                                                      <div class="col-sm-12">
                                                                                        <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $quizTestid ?>" class="form-control">
                                                                                      </div>
                                                                                    </div>
                                                                                    
                                                                                    
                                                                                    <div class="modal-footer">
                                                                                      <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                                                                                      <button type="submit" class="btn btn-primary btn-outline-primary ">Save </button>
                                                                                    </div></center>
                                                                                  </form>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                    <!-- Page-body start -->
                                <div class="page-body">
                                 <div id="demo" class="collapse">
									<form method="post">
                                        <div class="row">
                                            <div class="col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Created Date:</h4>
                                                <div id="reportrange" name="creted_date" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                                    <span></span> <b class="caret"></b>
                                                </div>
                                            </div> 
                                            <div class="col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Modified Date:</h4>
                                                <div id="reportrange" name="modified_date" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                                    <span></span> <b class="caret"></b>
                                                </div>
                                            </div> 
                                            
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Question Type:</h4>
                                                <select name="question_type" class="form-control">
                                                    <option value='1'>Single Correct Option</option>
                                                    <option value='2'>Multi  Correct Option</option>
                                                    <option value='3'>Fill in the Blank</option>
                                                    <option value='4'>Paragraph</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Topic:</h4>
                                                <input class="form-control" name="topic" id="topic" placeholder="Topic">
                                            </div>
                                            
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Subject:</h4>
                                                <input class="form-control" name="subject" id="subject" placeholder="Subject">
                                            </div>
                                            
                                            <div class="col-sm-12">
                                                <h4 class="sub-title">Question Text:</h4>
                                                <input class="form-control" name="question_text" id="question_text" placeholder="Question Text">
                                            </div>
                                            
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary btn-outline-primary">Search</button>
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary btn-outline-primary">Clear</button>
                                            </div>
                                            <div class="col-sm-12">
                                                &nbsp;
                                            </div>
                                            </div>
                                        </form>
										</div>
										<div>
                                             <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Created Date</th>
                                                                        <th>Question Text</th>
                                                                        <th>Subject</th>
                                                                        <th>Topic</th>
                                                                        <th>Used In</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                        foreach($questions as $getquestions){
                                                                        ?>
                                                                    <tr>
                                                                       
                                                                        <td><?php echo date('Y-m-d H:i:s', strtotime($getquestions->created_date)) ?></td>
                                                                        <td><?php echo $getquestions->question_text ?></td>
                                                                        <td><?php echo $getquestions->subject ?></td>
                                                                        <td><?php echo $getquestions->topic ?></td>
                                                                        <td>1</td>
                                                                        <td>
                                                                            <?php $questionsid=$getquestions->id?>
                                                                        <button type="button" onclick=alert('<?php echo $questionsid ?>') name="edit" id="edit" class="btn btn-inverse btn-outline-inverse btn-icon "  title="Add Question"><i class="feather icon-file-plus"></i></button>
                                                                        <nutton onclick = "alert('<?=  $questionsid?>') "value="<?=$questionsid?>"  name="delete" id="delete" class="btn btn-inverse btn-outline-inverse btn-icon "  title="Delete Question"><i class="feather icon-delete"></i></a>
  
                                                                         </td>
                                                                        
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<!-- sweet alert modal.js intialize js -->
		<!-- modalEffects js nifty modal window effects -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script>
		    function alert(questionsid)
		    {
                swal({
                  title: "Are you sure?",
                  text: "You wants to delete this  Question !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willEdit) => {
                  if (willEdit) {
                    /*swal("Poof! Your Course has been deleted!", {
                      icon: "success",
                    });*/
                    location.href = '<?php echo base_url() ?>Quiz/deletequestion?questionsid='+questionsid;
                  } else {
                    swal("User is not deleted any  Question!");
                  }
                });
            }
		</script>
<script>
        $(document).ready(function(){      
      var i=1;  
   
      $('#add').click(function(){  
        if(i<=5){ 
           i++;  
           $('#single').append('<br><br><div class="col-sm-12" id="row'+i+'"><div class="row"><div class="col-1"><input type="checkbox" name="option'+i+'" id="option'+i+'" value="1" class="form-control" ></div><div class="col-3"><lable class="float-left">Correct Option '+i+'</lable></div><div class="col-7"><input type="text" placeholder="Option" name="optionText'+i+'" id="optionText'+i+'" maxlength="255" rows="2"class="form-control max-textarea" ></div><div class="col-1"><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button></div></div></div><br>');  
        }
   else{
   alert('You cannot Add More than 6 Options');
    }
      });
  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
  
    });  
    </script>
     <script>
    function OptionType(type) {
    if(type == 1 || type == 2){
    document.getElementById("single").style.display = "block";
    document.getElementById("blank").style.display = "none";
    document.getElementById("para").style.display = "none";
    }else if(type == 3){
    
    document.getElementById("single").style.display = "none";
    document.getElementById("blank").style.display = "block";
    document.getElementById("para").style.display = "none";
    }else if(type == 4){
    document.getElementById("single").style.display = "none";
    document.getElementById("blank").style.display = "none";
    document.getElementById("para").style.display = "block";
    }
    
    }
    </script>
    
</body>

</html>
