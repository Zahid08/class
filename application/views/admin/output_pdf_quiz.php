<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    .list-group-item{
        margin-left: 10px;
    }
    .greeen_questions{
        background-color:#32B643;color:white;
    }
    .centerImage{
        display: block;
        float: left;
        margin-top: 10px;
        width: 25%;
    }
    .centerContentFooter{
        margin-top: 10px;
        margin-bottom: 50px;
    }
    .question_options_div{
       margin-left: 0px;
    }
    .column {
        float: left;
        padding-left: 25px;
        width: 40%;
    }
    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .margin-bottom-options{
        margin-bottom: 8px;
    }
    .middlePOintContetn {
        text-align: center;
        float: left;
        width: 35%;
    }
    .websiteLeft{
        float: left;
    }
</style>
<div class="container">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="centerImage" >
                <img src="http://localhost/class/assets/landing/logos/competetiveexamguidelogo.png" alt="Comptetive Exam Guide Logo">
            </div>

            <div class="middlePOintContetn">
                <b><?=$quiztest->title?></b>
            </div>

            <div class="rightSideContetn">
                <span>Total Questions : <?=count($newQuestions)?> ,</span>
                <span>Total Time : <?=$quiztest->time_limit?> minute</span>
            </div>
        </div>
    </div>

    <?php
    if ($newQuestions){
    ?>
    <?php
    $i = 0;
    $editIndex = 0;
    $questionNumber=1;
    foreach($newQuestions as $key=>$getnewQuestions){
        $key++;
        $editIndex++;
        $questionid = $getnewQuestions->id;
        ?>
            <div class="row">
                <br/>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="mb-2">
                            <span class="chip">[ Subject: &nbsp;<b class="subject"><?=$getnewQuestions->subject?></b>  &nbsp;]</span>
                            <span class="chip">[ Mark: &nbsp;<b class="subject"><?=$getnewQuestions->mark?></b>  &nbsp;] </span>
                            <span class="chip">[ Penalty: &nbsp;<b class="subject"><?=$getnewQuestions->penalty?></b>  &nbsp;] </span>
                        </div>

                    </div>

                    <div class="row col-md-12 question_options_div">
                        <div class="mb-2">
                            <b class="chip">Question : &nbsp;<b class="subject"><?=$questionNumber?></b></b>
                            <?php echo $getnewQuestions->question_text;?>
                        </div>
                    </div>

                    <?php
                    $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                    if ($newQuestionOptions){
                     ?>
                    <?php  $f = 'A'; foreach($newQuestionOptions as $getnewQuestionOptions){?>
                       <div class="column margin-bottom-options"><b><?=$f?>. </b><span><?=strip_tags($getnewQuestionOptions->option_text)?></span></div>
                    <?php  $f++; }?>
                    <?php }?>

                </div>
            </div>
        <?php $questionNumber++; } ?>
    <?php } ?>

</div>