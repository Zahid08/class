<?php
/*========================Pau U Info===============================*/
$payuMerchantKey = $this->db->query("SELECT * FROM setting WHERE name = 'PayU_MerchantKey' ORDER BY 1 DESC")->result();
foreach($payuMerchantKey as $payuMerchantKeyData){
$MerchantKey = $payuMerchantKeyData->value;
}
$payuMerchantSalt = $this->db->query("SELECT * FROM setting WHERE name = 'PayU_MerchantSalt' ORDER BY 1 DESC")->result();
foreach($payuMerchantSalt as $payuMerchantKeySalt){
$MerchantSalt = $payuMerchantKeySalt->value;
}
/*===================================================================*/
?>
<?php
$amt = $_GET['amt'];
$currentDate = date('Y-m-d');
/*==========================User Info=============================*/
$unique_id = $_SESSION['unique_id'];
$userData = $this->db->query("SELECT * FROM users WHERE unique_id = '$unique_id'  ORDER BY 1 DESC")->result();

foreach($userData as $getuserData){
$userId = $getuserData->id;
$userEmail = $getuserData->email_id;
$userName = $getuserData->username;
$userMobile = $getuserData->mobile;

}
/*=================================================================*/


/*========================Course Info===============================*/
$courseId = $_GET["courseid"];
$courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1 AND c.course_id = '$courseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
$courseTitle = $getcourseData->title;
$coursePayableAmount = $getcourseData->payable_price;
}
/*===================================================================*/
if(isset($_GET['amt'])){
    $coursePayableAmount = $amt;
}else{
    $coursePayableAmount = $getcourseData->payable_price;
}



// Merchant key here as provided by Payu
$MERCHANT_KEY = $MerchantKey;

// Merchant Salt as provided by Payu
$SALT = $MerchantSalt;

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
    
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
 
}

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
    
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
          || empty($posted['udf1'])
          
      || empty($posted['service_provider'])
  ) {
      
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
  $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';  
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;
    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
    
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';

}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Check Out</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Landing page template for creative dashboard">
    <meta name="keywords" content="Landing page template">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  <style>
    .form-control {
    font-size: 14px;
    border-radius: 2px;
    border: 1px solid #ccc;
    }
  </style>
</head>

<body onload="submitPayuForm()">

    <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
        <?php include("includes/header.php"); ?>
        <div class="main" id="main">
            <!-- Subscribe Form -->
            <div class="cta-sub  no-color">
                <div class="container">
                    
                            
                            
                    <h1 class="wow fadeInUp" data-wow-delay="0s"><?php echo $courseTitle ?></h1>
                    <br>
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                         <form action="<?php echo $action; ?>" method="post" name="payuForm">
                   <div class="row">
                       <div class="col-sm-3">
                           <p>&nbsp;</p>
                       </div>
                       
    <div class="col-sm-6">          
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
    <input type="hidden" name="hash_abc" value="<?php echo $hash_string ?>"/>
    <input type="hidden"  name="udf1" id="udf1" value="<?php echo $userId ?>" />
    <b>Amount:</b>
    <input class="form-control" name="amount" value="<?php echo $coursePayableAmount ?>" readonly/>
    <b>Name:</b>
    <input class="form-control" name="firstname" id="firstname" value="<?php echo $userName ?>" readonly/>
    <b>Email:</b>
    <input class="form-control" name="email" id="email" value="<?php echo $userEmail ?>" readonly/>
    <b>Mobile:</b>
       <input class="form-control" name="phone" value="<?php echo $userMobile ?>" readonly/>
      <!-- <b>Course:</b>-->
       <input class="form-control" type="hidden" name="productinfo" value="<?php echo $courseId ?>" readonly>
         <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
          <input type="hidden" name="furl" value="<?php echo base_url() ?>PaymentSuccess" />
          <input type="hidden" name="surl" value="<?php echo base_url() ?>PaymentSuccess" />
          <!--Please change this parameter value with your failure page absolute url like http://mywebsite.com/response.php. -->
          <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
          <?php if(!$hash) { ?>
           <input type="submit" class="btn btn-primary btn-action btn-fill" value="Checkout" />
          <?php } ?>
           
      </div>
      </div>
    </form>
                    </div>
                </div>
            </div>
            <!-- Footer Section -->
            <?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
    </div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
</body>

</html>
