<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Home</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    
</head>

<body>

<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">

<?php include("includes/header.php"); ?>
<div class="main" id="main">
            <!-- Main Section-->
             
   <div class="feature_huge" style="background-color: rgb(240, 255, 250);">
                 
        <div class="container container">
                   
            <div class="feature_list"> 
        
                <div class="row " >
				
					<div class="col-md-12" autocomplete="off">
					<center><h1 >FAQs</h1></center>
					</div>
				
					<div class="col-md-6">
						<h1>How do I create an account?</h1>
						<p>You need to create an account before taking a course. Click on Menu Icon (Three horizontal lines) present on the top left of the Store Screen. From the opened menu, click on Login/Register button. Click on Sign Up. Enter your Email Id &amp; Password to register for an account.</p>
					</div>
					<div class="col-md-6" autocomplete="off">
						<h1>How to use Access Codes?</h1>
						<p>You need to create an account on the app to use the access codes. After Successful login, click on the Menu Icon present on the top left of the Store Screen. From the opened menu, click on My Courses Button. On the opened screen, click on the Gift Icon present on the Top Right of the Screen. Enter your Access Code here. Respective Course will be added to your My Courses section.</p>
					</div>
				
					<div class="col-md-6">
						<h1>How to take a Course?</h1>
						<p>After you have purchased the Course or used the access code of the course, the course would be added to your My Course Section. Click on the Course image to start the course.</p>
					</div>
					<div class="col-md-6">
					<h1>I have issues with Login, what should I do?</h1>
						<p>You can click on Forgot Password link to reset your password from the login screen. We will send the new password to your registered email address. Try to login with the new password.<br>If the issue still persists, drop an email to {support-email} with the issue details.</p>
					</div>
				
					<div class="col-md-6">
					<h1>I am unable to access the Course, what should I do?</h1>
						<p>Please check whether you are using the same email, which was used for purchasing/using the access code. Check for the course in your My Course section. If the course is still not available, please drop an email to {support-email}</p>
					</div>
				
					<div class="col-md-6" >
						<h1>How to report an issue on the app?</h1>
					
						<p>For any other issues, please drop an email to {support-email} with the complete issue details.</p>
					</div>
				</div>
		    </div>
		</div>
	</div>
</div>


            <!-- Footer Section -->
            <?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
</div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
</body>

</html>
