<div class="footer">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <img src="<?php echo base_url() ?>assets/landing/logos/competetiveexamguidelogo.png" alt="Adminty Logo">
                        <ul class="footer-menu">
                          <li><a href="<?php echo base_url() ?>Faqs" target="_blank">FAQs</a></li> 
				          <li><a href="<?php echo base_url() ?>Aboutus" target="_blank">About Us</a></li> 
			              <li><a href="<?php echo base_url() ?>Contactus" target="_blank">Contact Us</a></li> 
			              <li><a href="<?php echo base_url() ?>Termsofuse" target="_blank">Terms of Use</a></li> 
                          <li><a href="<?php echo base_url() ?>Privacypolicy" target="_blank">Privacy Policy</a></li> 
                          <li><a href="<?php echo base_url() ?>Refundpolicy" target="_blank">Refund Policy</a></li> 
			            </ul>
                        <div class="footer-text">
                            <p>
                              Copyright  © 2021 Competitive Exam Guide
                            </p>
                        </div>
                    </div>
                </div>
</div>