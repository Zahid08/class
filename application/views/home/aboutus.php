<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Home</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    
</head>

<body>
<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">

<?php include("includes/header.php"); ?>
<div class="main" id="main">
            <!-- Main Section-->
             
    <div class="feature_huge" style="background-color: rgb(240, 255, 250);">
                 
        <div class="container container">
                   
            <div class="feature_list"> 
        
                <div class="row justify-content-md-center" >
			
					<div class="col-md-12 col-sm-12 " >
					<center><h1>About Us</h1></center>
					</div>
			
				    <div class="col-md-12 col-sm-12 ">
				    <p style="text-align: justify;">Competitive Exam Guide (CEG) is Brand Name of Keshri EduTech Private Limited. CEG is an online educational portal which aims to motivate and guide future aspirants who are going to appear in different competitive exams. On this platform, guidance is provided by the alumni of India’s top premier institutions like IITs, IIMs and other renowned universities of India who themselves have cleared the Competitive Exams and achieved Top Position. To continue with this journey of success, we have come up with All India test series which will be conducted both in online and offline mode. These mock tests include the exams like JEE, NEET, IBPS, SSC CGLE, and SBI-PO etc. The sole purpose of our test series is to make your preparation robust and let you know where do you stand among the lakhs of aspirants in this tough competition. Each mock test has been designed in accordance with the syllabus of respective exams and is based on the pattern of previous year questions which is followed by these exams. This will not only help you to streamline your preparation but also you will be able to monitor your progress. Ultimately, our aim is to instill confidence in you so that you can excel in the real exam.</p>
			       
			        </div>
			    
			   
		        </div>
		    </div>      
	    </div>
	</div>    



            <!-- Footer Section -->
            <?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            <!-- Scroll To Top Ends-->
</div>
        <!-- Main Section -->
</div>    
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
</body>

</html>
