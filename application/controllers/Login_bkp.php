<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
	public function index()
	{
		$this->load->view('basic/login');
	}
	
	 public function loginUser()  
      {  
          
           {  
               
                //true  
                $email = $this->input->post('email');  
                $decode_password = md5($this->input->post('password')); 
                
                
                $getId = $this->db->query("SELECT id,currently_logged FROM `users` WHERE email_id = '$email'");
                foreach($getId->result() as $getuserId){
                        $Id = $getuserId->id ; 
                        $currentlyLogged = $getuserId->currently_logged; 
                        
                }
                
                
                $deviceInfo = $_SERVER['HTTP_USER_AGENT'] . "\n\n";
                
                
                $getDeviceInfo = $this->db->query("SELECT * FROM `user_devices` WHERE user_id = '$Id' AND status = 1")->result();
                
                /*=============Get Setting Value=============*/
                /*========================Device Count Info===============================*/
                $devices = $this->db->query("SELECT value FROM setting WHERE name = 'Devices_Value' ORDER BY 1 DESC")->result();
                foreach($devices as $devicesData){
                $devicesValue = $devicesData->value;
                }
                /*===================================================================*/
                /*=============================================*/
                
                if($currentlyLogged == 0){
                    
                    
               
                if(count($getDeviceInfo) < $devicesValue){
                 //model function  
                $this->load->model('Login_model'); 
                if($this->Login_model->can_login($email, $decode_password))  
                {  
                    $this->db->where('email_id', $email);
                    $query_role = $this->db->get('users');
                    $result_role = $query_role->row_array(); // get the row first
                    $role = $result_role['role'];
                    $status = $result_role['status'];
                    $username = $result_role['username'];
                    $unique_id = $result_role['unique_id'];
                    $user_id = $result_role['id'];
                    $login_count = $result_role['login_count'];
                    $profile_image = $result_role['profile_picture'];
                    
                    
                     $session_data = array( 
                          'user_id' => $user_id,
                          'username' => $username,
                          'email'  =>  $email,
                          'role' => $role,
                          'status' => $status,
                          'unique_id' => $unique_id,
                          'login_count'=> $login_count,
                          'profile_picture'=> $profile_image
                     );
                     $now =date("Y-m-d");
                     $this->Login_model->update_logincount($email,$login_count);
                     $this->Login_model->update_lastlogin($email);
                     $this->Login_model->deviceinfo($deviceInfo,$user_id);
                    
                    $this->db->query("UPDATE users SET currently_logged = 1 WHERE email_id = '".$email."'  ");
                     $this->session->set_userdata($session_data); 
                    redirect(base_url() . 'Home'); 
                    
                }  
                else  
                {  
                     $this->session->set_flashdata('msg', 'Invalid Email and Password');  
                     redirect(base_url() . 'Login'); 
                }   
                }else  
                {  
                     $this->session->set_flashdata('msg', 'You logged in Maximum No of Devices,Please Contact Admin');  
                     redirect(base_url() . 'Login'); 
                }
                
                }else
                {
                    $this->session->set_flashdata('msg', 'You have logged in some other machine,please logout from that and try here... ');  
                   /** $this->db->query("UPDATE users SET currently_logged = 0 WHERE email_id = '".$email."' ");**/
                     redirect(base_url() . 'Login'); 
                }
                  
           }
      }  
    
     
    public function logoutuser()  
      {  
          
            $this->db->query("UPDATE users SET currently_logged = 0 WHERE email_id = '".$_SESSION['email']."' ");
        $this->session->unset_userdata('user_id');  
          $this->session->unset_userdata('username');  
          $this->session->unset_userdata('email');  
          $this->session->unset_userdata('role');  
          $this->session->unset_userdata('status');   
          $this->session->unset_userdata('unique_id');
           
           redirect(base_url() . 'Home'); 
      }  
}
