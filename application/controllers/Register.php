<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
		$this->load->model('Register_model');
	}
	public function index()
	{
		$this->load->view('basic/register');
		
	}
	public function registerUser()
	{
	        $userName 	= $this->security->xss_clean($this->input->post('username'));
			$email 		= $this->security->xss_clean($this->input->post('email'));
			$mobile 	= $this->security->xss_clean($this->input->post('mob'));
			$dob 	= $this->security->xss_clean($this->input->post('dob')); 
			$city 	= $this->security->xss_clean($this->input->post('city')); 
			$password 	= md5($this->security->xss_clean($this->input->post('password')));
			
			
			
		    if($this->security->xss_clean($this->input->post('referralcode')) =='')
			{
			  $parent_id  = NULL;
			}
			else
			{
			    $parent_id  = $this->security->xss_clean($this->input->post('referralcode'));
			   
			}
			
			    
			
			
			$now = date('Y-m-d');
			
			/*===================Referrer Credits Signup info=====================*/
            $referrerCreditsSignup = $this->db->query("SELECT * FROM setting WHERE name = 'Referrer_Credits_Signup' ORDER BY 1 DESC")->result();
            foreach($referrerCreditsSignup as $referrerCreditsSignupData){
            $referrerCreditsSignup = $referrerCreditsSignupData->value;
            }
            /*======================================================================*/
            /*===================Referree Credits Signup info=====================*/
            $referreeCreditsSignup = $this->db->query("SELECT * FROM setting WHERE name = 'Referree_Credits_Signup' ORDER BY 1 DESC")->result();
            foreach($referreeCreditsSignup as $referreeCreditsSignupData){
            $referreeCreditsSignup = $referreeCreditsSignupData->value;
            }
            /*======================================================================*/
            
            /*===================Referrer Credits Signup info=====================*/
            $creditInRs = $this->db->query("SELECT * FROM setting WHERE name = 'Credit_Value' ORDER BY 1 DESC")->result();
            foreach($creditInRs as $getcreditInRs){
            $credit_Rs = $getcreditInRs->value;
            }
            /*======================================================================*/
			
        
			
			$code = 'CEG'.rand(1000,9999).rand(10,99);
			$insertData = array(
			                    
			                    'unique_id'=>$code,
			                    'email_id'=>$email,
			                    'parent_id'=>$parent_id,
			                    
			                    'username'=>$userName,
								'mobile'=>$mobile,
								'dob'=>$dob,
								'city'=>$city,
								'password'=>$password,
								'created_by'=>$email,
								'status'=>1,
								'first_login'=>2,
								'role'=>3,
								'created_datetime'=>$now);
								
								
								
			$creditData = array(
			                    'parent_id'=>$parent_id,
			                    'child_id'=>$email,
			                    'parent_credits'=>$referrerCreditsSignup,
			                    'child_credits'=>$referreeCreditsSignup,
			                    'child_credits_in_rs' => $referreeCreditsSignup * $credit_Rs,
			                    'parent_credits_in_rs' => $referrerCreditsSignup * $credit_Rs,
								'type'=>'OnRegister',
								'created_by'=>$email);
								
			$creditDataParent = array(
			                    'unique_id'=>$parent_id,
			                    'child_id'=>$code,
			                    'credits_earned'=>$referrerCreditsSignup,
			                    'credits_in_rs'=>$referrerCreditsSignup * $credit_Rs,
			                    'type'=>'OnRegister',
			                    'user_as'=>'AsParent',
			                    'created_by' => $parent_id,
			                    'created_date' => $now
								);
 
 
            $creditDataChild = array(
			                    'unique_id'=>$code,
			                    'child_id'=>NULL,
			                    'credits_earned'=>$referreeCreditsSignup,
			                    'credits_in_rs'=>$referreeCreditsSignup * $credit_Rs,
			                    'type'=>'OnRegister',
			                    'user_as'=>'AsChild',
			                    'created_by' => $code,
			                    'created_date' => $now
								);
								
			
			$checkDuplicate = $this->Register_model->checkDuplicate($email);
			
			if($checkDuplicate == 0)
			{
			    
				$insertUser = $this->Register_model->insertUser($insertData);
			
				if($insertUser)
				{
				    $insertCreditData = $this->Register_model->insertCreditData($creditData);
				    
				    $insertCreditParentData = $this->Register_model->insertCreditParentData($creditDataParent);
				    $insertCreditChildData = $this->Register_model->insertCreditChildData($creditDataChild);
				    
				    $this->session->set_flashdata('msg', 'Successfully Registered'); //set success msg if registered successfully
					redirect('Login');
				}
				else
				{
					$this->session->set_flashdata('msg','Unable to save user. Please try again');
					redirect('Register');
				}
			}
			else
			{
				$this->session->set_flashdata('msg','User email already exists');
				redirect('Register');
			}
		
	}
	
    
}
