<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resetpassword extends CI_Controller {
    
	public function index()
	{
		$this->load->view('basic/resetpassword');
	}
	
	public function forgotpassword()
	{
        $email = $this->input->post('email');  
        
        
        //model function  
        $this->load->model('Forgotpassword_model');
        $this->Forgotpassword_model->enteremail($email);
        
        
        $uniqueid = $this->db->query("SELECT unique_id FROM `users` WHERE email_id = '$email' ")->result();

        foreach($uniqueid as $getuniqueid){
            $uid = $getuniqueid->unique_id;
        }
    
        redirect("Changepassword?uid=$uid");
    }
}
