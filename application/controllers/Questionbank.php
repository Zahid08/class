<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionbank extends CI_Controller {
    function __construct()
{
parent::__construct();
$this->load->model('Newchapter_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/questionbank');
	    }else{
	        redirect('Login');
	    }
		
	}
	public function addnewquestions(){
   
$courseid = $this->security->xss_clean($this->input->post('courseid'));

$liveTestid = $this->security->xss_clean($this->input->post('liveTestid'));
$type  = $this->security->xss_clean($this->input->post('type'));
$subjectname = $this->security->xss_clean($this->input->post('subjectname'));
$Topic   = $this->security->xss_clean($this->input->post('Topic'));
$QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

$Explaination = $this->security->xss_clean($this->input->post('explaination'));




$Order = $this->security->xss_clean($this->input->post('Order'));
$Marks= $this->security->xss_clean($this->input->post('Marks'));
$Penalty = $this->security->xss_clean($this->input->post('Penalty'));

$now = date('Y-m-d H:i:s');

$insertquestionsData = array('live_test_id'=>$liveTestid,
'type' => $type,
'subject'=>$subjectname,
'topic'=>$Topic,
'question_text'=>$QuestionText,
'explaination' => $Explaination,
'order'=>$Order,
'mark'=>$Marks,
'penalty'=>$Penalty,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id']
);


$insertNewquestions=$this->Newchapter_model->insertnewquestions($insertquestionsData);
if($insertNewquestions)
{
    //after question has been inserted successfully getting if it and inserting its options
    $CreatedBy = $_SESSION['user_id'];
    $newQuestionID = $this->db->query("SELECT id FROM live_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
        foreach($newQuestionID as $getnewQuestionID){
        $QuestionID = $getnewQuestionID->id;
        }

        for($i=1;$i<=8;$i++){
        	    if(!empty($this->input->post('optionText'.$i))){
        	        $insertOptions = array('live_test_question_id' => $QuestionID
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'is_answer' => $this->input->post('option'.$i) 
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );
                $this->db->insert('live_test_question_options', $insertOptions);
        	    }  
        	}
            
     
$this->session->set_flashdata('msg', 'Question Added Successfully.');
redirect('Questionbank');


}


else 
{
$this->session->set_flashdata('amsg','Unable to Added question. Please try again.');
redirect('Questionbank');
}
}
public function deletequestion()
	{
	    
	    $questionID = $_GET['questionid'];
	    
	     $this->db->query("UPDATE live_test_question_options SET status = 2 WHERE live_test_question_id = $questionID");
	    $this->db->query("UPDATE live_test_questions SET status = 2 WHERE id = $questionID ");
	   
	     $this->session->set_flashdata('delmsg','Deleted Question Successfully .');
	     redirect('Questionbank');
         
	     
	}
}
