<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendingcredits extends CI_Controller {
   function __construct()
	{
		parent::__construct();
		$this->load->model('Pendingcredits_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	
	public function index()
	{
	     
	       	$this->load->view('admin/pendingcredits');
	     
	     
	}
	public function requestcredit()
	{
			$creditID = $this->security->xss_clean($this->input->get('credit_id'));
			$uniqueID= $this->security->xss_clean($this->input->get('id'));
			$now = date('Y-m-d');
	     	$updateData = array(
	     	                    'id'=>$creditID,
								'status'=>2,
								'paid_date'=>$now,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
								
			
								
			$redeemCredit = $this->Pendingcredits_model->redeemcredit($updateData,$creditID,$uniqueID);
			
		   
			
				if($redeemCredit)
				{
				    
    				    $this->session->set_flashdata('rd_msg','Credits has been placed successfully' ); //set success msg if registered successfully
    				   /* echo $creditID;*/
    				 	redirect('Pendingcredits');
				    
				}
				else
				{
					$this->session->set_flashdata('rd_msg','Unable to Place Request. Please try again ');
					redirect('Pendingcredits');
				}
		
	}
}