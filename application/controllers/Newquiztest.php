<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Newquiztest extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Newquiztest_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('Newchapter_model');
    }
    public function index()
    {
        if(($_SESSION['role'] == 1 ) && $_SESSION['status'] == 1){

            $this->load->view('admin/course/newquiztest');
        }
        else if(($_SESSION['role'] == 2) && $_SESSION['status'] == 1){
            $this->load->view('instructor/course/newquiztest');
        }

        else{
            redirect('Login');
        }
    }

    public function importQuiz()
    {
        $questionsList=$_REQUEST['questionList'];
        $questionsList = explode (",", $questionsList);

        $quizTestid = $this->security->xss_clean($_REQUEST['quezTestId']);
        foreach ($questionsList as $key=>$item){
            $singelQuestions=$this->db->select('*')->from('question_bank')->where('id',$item)->get()->row_array();

            if ($singelQuestions){
                $quizType=$singelQuestions['quiz_type'];
                $insertquestionsData = array('quiz_test_id'=>$quizTestid,
                    'type' => $singelQuestions['type'],
                    'import_bank_id' =>$item,
                    'subject'=>$singelQuestions['subject'],
                    'topic'=>$singelQuestions['topic'],
                    'question_text'=>$singelQuestions['question_text'],
                    'question_text_lang'=>$singelQuestions['question_text_lang'],
                    'explaination' =>!empty($singelQuestions['explaination'])?$singelQuestions['explaination']:'No explanations',
                    'explaination_lang' => $singelQuestions['explaination_lang'],
                    'order'=>$singelQuestions['order'],
                    'mark'=>$singelQuestions['mark'],
                    'penalty'=>$singelQuestions['penalty'],
                    'quiz_type'=>$quizType,
                    'created_date'=>date('Y-m-d'),
                    'created_by'=>$_SESSION['user_id'],
                    'updated_date'=>date('Y-m-d'),
                    'updated_by'=>$_SESSION['user_id'],
                    'status'=>1
                );
                $insertNewquestions=$this->Newquiztest_model->insertquiztestquestions($insertquestionsData);

                $getOptionsAll=$this->db->select('*')->from('question_bank_options')->where('qsn_bank_id',$singelQuestions['id'])->get()->result_array();

                if ($getOptionsAll && $insertNewquestions){
                    $CreatedBy = $_SESSION['user_id'];
                    $newQuestionID = $this->db->query("SELECT id FROM quiz_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
                    foreach($newQuestionID as $getnewQuestionID){
                        $QuestionID = $getnewQuestionID->id;
                    }

                    foreach ($getOptionsAll as $key=>$optionsItem){
                        $insertOptions = array('quiz_test_question_id' => $QuestionID
                        ,'option_no' => $optionsItem['option_no']
                        ,'option_text' =>$optionsItem['option_text']
                        ,'option_text_lang' =>$optionsItem['option_text_lang']
                        ,'is_answer' =>$optionsItem['is_answer']
                        ,'status' => 1 //byDefault Active
                        ,'created_date'=>date('Y-m-d')
                        ,'created_by'=>$_SESSION['user_id']
                        ,'updated_date'=>date('Y-m-d')
                        ,'updated_by'=>$_SESSION['user_id']
                        );

                        $this->db->insert('quiz_test_question_options', $insertOptions);
                    }

                }
            }
        }
        echo 1;
        exit();
    }
    public function addquiztest()
    {

        $chapterid = $this->security->xss_clean($this->input->post('chapterid'));
        $chaptername = $this->security->xss_clean($this->input->post('chaptername'));
        $availableFrom = $this->security->xss_clean($this->input->post('available_from'));
        $availableTill = $this->security->xss_clean($this->input->post('available_till'));
        $Msg = $this->security->xss_clean($this->input->post('message'));
        $courseid   = $this->security->xss_clean($this->input->post('courseid'));
        $quizTestid = $this->security->xss_clean($this->input->post('quizTestid'));
        $ShowDefaultInstruction = $this->security->xss_clean($this->input->post('ShowDefaultInstruction'));
        $ShowScientificCalculator = $this->security->xss_clean($this->input->post('ShowScientificCalculator'));
        $postMsg = $this->security->xss_clean($this->input->post('post_message'));
        $timeLimit = $this->security->xss_clean($this->input->post('time_limit'));
        $retake_attempts = $this->security->xss_clean($this->input->post('re_take_attempts'));
        $availableTime = $this->security->xss_clean($this->input->post('available_time'));
        $GenerateResults = $this->security->xss_clean($this->input->post('GenerateResults'));
        $ShowLeaderboard = $this->security->xss_clean($this->input->post('ShowLeaderboard'));
        $ShowRank = $this->security->xss_clean($this->input->post('ShowRank'));
        $Arrangequestionsbytopic = $this->security->xss_clean($this->input->post('Arrangequestionsbytopic'));
        $now = date('Y-m-d');

        $supportLanguage = $this->security->xss_clean($this->input->post('another-language'));
        $lanugageId = $this->security->xss_clean($this->input->post('language'));
        $automicaalyRandomChose = $this->security->xss_clean($this->input->post('random-chose'));

        $ShowSolutionsLearners = $this->security->xss_clean($this->input->post('ShowSolutionsLearners'));
        $ShowSolutionsAfterAttempt = $this->security->xss_clean($this->input->post('ShowSolutionsAfterAttempt'));

        $quizType =$_REQUEST['quizType'];

        $emailPush = $this->security->xss_clean($this->input->post('email_push'));
        $mobilePush = $this->security->xss_clean($this->input->post('mobile_push'));
        $webPush = $this->security->xss_clean($this->input->post('web_push'));


        if ($ShowDefaultInstruction=='' || $ShowScientificCalculator=='' || $Arrangequestionsbytopic=='' || $ShowRank=='' || $ShowLeaderboard=='' || $ShowScientificCalculator=='' || $ShowSolutionsLearners==''){
            $ShowDefaultInstruction=$ShowScientificCalculator=$Arrangequestionsbytopic=$ShowRank=$ShowLeaderboard=$ShowScientificCalculator=$ShowSolutionsLearners=1;
        }

        $insertData = array('course_id'=> $courseid,
            'title' => $chaptername,
            'time_limit'=>$timeLimit,
            're_take_attempts'=>$retake_attempts,
            'available_from'=>$availableFrom,
            'available_till'=>$availableTill,
            'instructions'=>$Msg,
            'is_instructions'=>$ShowDefaultInstruction,
            'is_scientific_calc'=>$ShowScientificCalculator,
            'show_leaderboard'=>$ShowLeaderboard,
            'show_rank'=>$ShowRank,
            'results_mode'=> $GenerateResults,
            'arrange_by_topic'=>$Arrangequestionsbytopic,
            'post_msg'=>$postMsg ,
            'min_time_pre_submit'=>$availableTime,
            'quiz_type'=>$quizType,
            'created_date'=>$now,
            'support_lang'=>$supportLanguage,
            'support_lang_id'=>$lanugageId,
            'random_chose'=>$automicaalyRandomChose,
            'created_by'=>$_SESSION['user_id'],
            'updated_date'=>$now,
            'updated_by'=>$_SESSION['user_id']
        );
        $updateData = array('course_id'=> $courseid,
            'title' => $chaptername,
            'time_limit'=>$timeLimit,
            're_take_attempts'=>$retake_attempts,
            'available_from'=>$availableFrom,
            'available_till'=>$availableTill,
            'instructions'=>$Msg,
            'is_instructions'=>$ShowDefaultInstruction,
            'is_scientific_calc'=>$ShowScientificCalculator,
            'show_leaderboard'=>$ShowLeaderboard,
            'show_rank'=>$ShowRank,
            'results_mode'=> $GenerateResults,
            'arrange_by_topic'=>$Arrangequestionsbytopic,
            'post_msg'=>$postMsg ,
            'min_time_pre_submit'=>$availableTime,
            'quiz_type'=>$quizType,
            'created_date'=>$now,
            'show_solutions_for_learner'=>$ShowSolutionsLearners,
            'show_solutions_after_attempt'=>$ShowSolutionsAfterAttempt,
            'created_by'=>$_SESSION['user_id'],
            'updated_date'=>$now,
            'updated_by'=>$_SESSION['user_id']
        );
        if($chapterid == 'nil'){
            $insertsubData = array(
                'course_id'=> $courseid,
                'chapterid' => '0',
                'type' => 'Live Quiz',
                'chapter_name'=> $chaptername,
                'available_from' => $availableFrom,
                'available_to' => $availableTill,
                'pre_class_msg' =>$Msg,
                'post_class_msg' =>$postMsg,
                'created_by'=> $_SESSION['user_id'],
                'created_date'=>$now);
        }else{
            $insertsubData = array(
                'course_id'=> $courseid,
                'chapterid'=> $chapterid,
                'type' => 'Live Quiz',
                'subchapter_name' => $chaptername,
                'available_from' => $availableFrom,
                'available_to' => $availableTill,
                'pre_class_msg' =>$Msg,
                'post_class_msg' =>$Msg,
                'created_by'=> $_SESSION['user_id'],
                'email_push' =>$emailPush,
                'mobile_push' =>$mobilePush,
                'web_push' =>$webPush,
                'created_date'=>$now);
        }

        $insertquiztests = $this->Newquiztest_model->insertquiztest($insertData,$insertsubData,$updateData,$quizTestid,$courseid);

        if($insertquiztests)
        {

            $userid = $_SESSION['user_id'];
            $quizTest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1  AND created_by = '$userid' ORDER BY 1 DESC  ")->result();

            $this->session->set_flashdata('msg', 'Successfully Updated.');
            redirect('Newquiztest?courseid='.$courseid.'&id='.$insertquiztests);

        }
        else
        {
            $this->session->set_flashdata('msg','Unable to save course. Please try again.');
            redirect('Newquiztest?courseid='.$courseid.'&id='.$quizTestid);
        }

    }

    public function editquiztestquestions(){
        $courseid = $this->security->xss_clean($this->input->post('courseid'));

        $quizTestid = $this->security->xss_clean($this->input->post('quizTestid'));
        $type  = $this->security->xss_clean($this->input->post('type'));

        $subjectname = $this->security->xss_clean($this->input->post('subjectname'));
        $Topic   = $this->security->xss_clean($this->input->post('Topic'));
        $QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

        $Explaination = $this->security->xss_clean($this->input->post('explaination'));




        $Order = $this->security->xss_clean($this->input->post('Order'));
        $Marks= $this->security->xss_clean($this->input->post('Marks'));
        $Penalty = $this->security->xss_clean($this->input->post('Penalty'));

        $newQuestionId = $this->security->xss_clean($this->input->post('newQuestionId'));

        $now = date('Y-m-d H:i:s');

        $QuestionTextlang = $this->security->xss_clean($this->input->post('QuestionTextLang'));
        $ExplainationLang = $this->security->xss_clean($this->input->post('explainationLang'));

        $updateQuestionData = array('quiz_test_id'=>$quizTestid,
            'type' => $type,
            'subject'=>$subjectname,
            'topic'=>$Topic,
            'question_text'=>$QuestionText,
            'question_text_lang'=>$QuestionTextlang,
            'explaination' => $Explaination,
            'explaination_lang' => $ExplainationLang,
            'order'=>$Order,
            'mark'=>$Marks,
            'penalty'=>$Penalty,
            'created_date'=>$now,
            'created_by'=>$_SESSION['user_id'],
            'updated_date'=>$now,
            'updated_by'=>$_SESSION['user_id']
        );

        $updateQuestionsId=$this->Newquiztest_model->updatequiztestquestions($updateQuestionData,$newQuestionId);

        if ($updateQuestionsId){

            for($i=1;$i<=8;$i++){
                if(!empty($this->input->post('optionText'.$i))){
                    $updateQuestionOpitonId= $this->input->post('questionoptionId'.$i);
                    $updateQuestionsOptionns = array('quiz_test_question_id' => $newQuestionId
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'option_text_lang' => $this->input->post('optionText'.$i.'Lang')
                    ,'is_answer' => $this->input->post('option'.$i)
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );

                    $this->db->where('id', $updateQuestionOpitonId);
                    $this->db->update('quiz_test_question_options', $updateQuestionsOptionns);
                }
            }
        }

        $this->session->set_flashdata('msg', 'Question Added Successfully.');
        redirect('Newquiztest?courseid='.$courseid.'&id='.$quizTestid);

    }

    public function addquiztestQuestion(){

        $courseid = $this->security->xss_clean($this->input->post('courseid'));

        $quizTestid = $this->security->xss_clean($this->input->post('quizTestid'));
        $type  = $this->security->xss_clean($this->input->post('type'));

        $subjectname = $this->security->xss_clean($this->input->post('subjectname'));
        $Topic   = $this->security->xss_clean($this->input->post('Topic'));
        $QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

        $Explaination = $this->security->xss_clean($this->input->post('explaination'));




        $Order = $this->security->xss_clean($this->input->post('Order'));
        $Marks= $this->security->xss_clean($this->input->post('Marks'));
        $Penalty = $this->security->xss_clean($this->input->post('Penalty'));

        $now = date('Y-m-d H:i:s');

        $QuestionTextlang = $this->security->xss_clean($this->input->post('QuestionTextLang'));
        $ExplainationLang = $this->security->xss_clean($this->input->post('explainationLang'));

        $quizType= $this->security->xss_clean($this->input->post('quizType'));

        $insertquestionsData = array('quiz_test_id'=>$quizTestid,
            'type' => $type,
            'subject'=>$subjectname,
            'topic'=>$Topic,
            'question_text'=>$QuestionText,
            'question_text_lang'=>$QuestionTextlang,
            'explaination' => $Explaination,
            'explaination_lang' => $ExplainationLang,
            'order'=>$Order,
            'mark'=>$Marks,
            'penalty'=>$Penalty,
            'quiz_type'=>$quizType,
            'created_date'=>$now,
            'created_by'=>$_SESSION['user_id'],
            'updated_date'=>$now,
            'updated_by'=>$_SESSION['user_id']
        );



        $insertNewquestions=$this->Newquiztest_model->insertquiztestquestions($insertquestionsData);

        if($insertNewquestions)
        {
            //after question has been inserted successfully getting if it and inserting its options
            $CreatedBy = $_SESSION['user_id'];
            $newQuestionID = $this->db->query("SELECT id FROM quiz_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
            foreach($newQuestionID as $getnewQuestionID){
                $QuestionID = $getnewQuestionID->id;
            }

            for ($i = 1; $i <= 8; $i++) {
                if (!empty($this->input->post('optionText' . $i))) {
                    $insertOptions = array('quiz_test_question_id' => $QuestionID
                    , 'option_no' => $i
                    , 'option_text' => $this->input->post('optionText' . $i)
                    , 'option_text_lang' => $this->input->post('optionText' . $i . 'Lang')
                    , 'is_answer' => $this->input->post('option' . $i)
                    , 'status' => 1 //byDefault Active
                    , 'created_date' => $now
                    , 'created_by' => $_SESSION['user_id']
                    , 'updated_date' => $now
                    , 'updated_by' => $_SESSION['user_id']
                    );
                    $this->db->insert('quiz_test_question_options', $insertOptions);
                }
            }


            //Save For QUestion Bank
            $insertquestionsBankData = array('live_test_id'=>0,
                'type' => $type,
                'subject'=>$subjectname,
                'topic'=>$Topic,
                'question_text'=>$QuestionText,
                'order'=>'',
                'mark'=>$Marks,
                'penalty'=>$Penalty,
                'quiz_type'=>$quizType,
                'created_date'=>$now,
                'created_by'=>$_SESSION['user_id'],
                'updated_date'=>$now,
                'updated_by'=>$_SESSION['user_id'],
                'status'=>1,
            );

            $insertNewquestionsBank=$this->Newchapter_model->insertnewquestions($insertquestionsBankData);

            if ($insertNewquestionsBank){
                //after question has been inserted successfully getting if it and inserting its options
                $CreatedBy = $_SESSION['user_id'];
                $newQuestionID = $this->db->query("SELECT id FROM question_bank WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
                foreach($newQuestionID as $getnewQuestionID){
                    $QuestionBankId = $getnewQuestionID->id;
                }

                for ($i = 1; $i <= 8; $i++) {
                    if (!empty($this->input->post('optionText' . $i))) {
                        $insertOptions = array('qsn_bank_id' => $QuestionBankId
                        , 'option_no' => $i
                        , 'option_text' => $this->input->post('optionText' . $i)
                        , 'option_text_lang' => $this->input->post('optionText' . $i . 'Lang')
                        , 'is_answer' => $this->input->post('option' . $i)
                        , 'status' => 1 //byDefault Active
                        , 'created_date' => $now
                        , 'created_by' => $_SESSION['user_id']
                        , 'updated_date' => $now
                        , 'updated_by' => $_SESSION['user_id']
                        );
                        $this->db->insert('question_bank_options', $insertOptions);
                    }
                }

            }

            $this->session->set_flashdata('msg', 'Question Added Successfully.');
            redirect('Newquiztest?courseid='.$courseid.'&id='.$quizTestid);


        }


        else
        {
            $this->session->set_flashdata('msg','Unable to Added question. Please try again.');
            redirect('Newquiztest?courseid='.$courseid.'&id='.$quizTestid);
        }
    }
    public function deletequestion()
    {
        $CourseId = $_GET['courseid'];
        $quizTestid =$_GET['quiztestid'];
        $questionID = $_GET['questionid'];

        $this->db->query("UPDATE quiz_test_question_options SET status = 2 WHERE quiz_test_question_id = $questionID");
        $this->db->query("UPDATE quiz_test_questions SET status = 2 WHERE id = $questionID ");

        $this->session->set_flashdata('delmsg','Deleted Question Successfully .');
        redirect('Newquiztest?courseid='.$CourseId.'&id='.$quizTestid);


    }

    public function importquestions(){

        if(!empty($_FILES["file_upload"]["name"])) {
            $file_data = fopen($_FILES["file_upload"]["tmp_name"], 'r');
            fgetcsv($file_data);
            $incree = 0;
            while ($row = fgetcsv($file_data)) {

                $serialId           =$this->security->xss_clean($row[0]);
                $subject            =$this->security->xss_clean($row[1]);
                $topics             =$this->security->xss_clean($row[2]);
                $tags               =$this->security->xss_clean($row[3]);
                $questionsType      =$this->security->xss_clean($row[4]);
                $questionsText      =$this->security->xss_clean($row[5]);

                $options1           =$this->security->xss_clean($row[6]);
                $options2           =$this->security->xss_clean($row[7]);
                $options3           =$this->security->xss_clean($row[8]);
                $options4           =$this->security->xss_clean($row[9]);

                $isAnswer           = $this->security->xss_clean($row[10]);
                $explanations       = $this->security->xss_clean($row[11]);
                $postvMark          = $this->security->xss_clean($row[12]);
                $negtvMark          = $this->security->xss_clean($row[13]);

                $now = date('Y-m-d H:i:s');

                //Save For QUestion Bank
                $insertquestionsData = array('live_test_id'=>0,
                    'type' => $questionsType,
                    'subject'=>$subject,
                    'topic'=>$topics,
                    'question_text'=>$questionsText,
                    'explaination' => $explanations,
                    'order'=>'',
                    'mark'=>$postvMark,
                    'penalty'=>$negtvMark,
                    'created_date'=>$now,
                    'created_by'=>$_SESSION['user_id'],
                    'updated_date'=>$now,
                    'updated_by'=>$_SESSION['user_id'],
                    'status'=>1,
                );

                $optiosnList=array(
                    '1'=>$options1,
                    '2'=>$options2,
                    '3'=>$options3,
                    '4'=>$options4,
                );

                $insertNewquestions=$this->Newchapter_model->insertnewquestions($insertquestionsData);
                if($insertNewquestions)
                {
                    //after question has been inserted successfully getting if it and inserting its options
                    $CreatedBy = $_SESSION['user_id'];
                    $newQuestionID = $this->db->query("SELECT id FROM question_bank WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
                    foreach($newQuestionID as $getnewQuestionID){
                        $QuestionID = $getnewQuestionID->id;
                    }

                    foreach ($optiosnList as $key=>$item){
                        $isAnserValue='';
                        if ($key==$isAnswer){
                            $isAnserValue=1;
                        }

                        $insertOptions = array('qsn_bank_id' =>$QuestionID
                        ,'option_no' => $key
                        ,'option_text' =>$item
                        ,'is_answer' =>$isAnserValue
                        ,'status' => 1 //byDefault Active
                        ,'created_date'=>$now
                        ,'created_by'=>$_SESSION['user_id']
                        ,'updated_date'=>$now
                        ,'updated_by'=>$_SESSION['user_id']
                        );

                        $this->db->insert('question_bank_options', $insertOptions);
                    }
                }

                //
                //Save Question For Specific Qz
                $insertQuizQuestions= array('quiz_test_id'=>$_POST['quizTestId'],
                    'type' => $questionsType,
                    'subject'=>$subject,
                    'topic'=>$topics,
                    'question_text'=>$questionsText,
                    'explaination' => $explanations,
                    'mark'=>$postvMark,
                    'penalty'=>$negtvMark,
                    'created_date'=>$now,
                    'created_by'=>$_SESSION['user_id'],
                    'updated_date'=>$now,
                    'updated_by'=>$_SESSION['user_id']
                );

                $insertSingleQuizQuestions=$this->Newquiztest_model->insertquiztestquestions($insertQuizQuestions);
                if($insertSingleQuizQuestions)
                {
                    //after question has been inserted successfully getting if it and inserting its options
                    $CreatedBy = $_SESSION['user_id'];
                    $newQuestionIDList = $this->db->query("SELECT id FROM quiz_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
                    foreach($newQuestionIDList as $newgetnewQuestionID){
                        $NewQuestionID = $newgetnewQuestionID->id;
                    }

                    foreach ($optiosnList as $key=>$item){
                        $isAnserValue='';
                        if ($key==$isAnswer){
                            $isAnserValue=1;
                        }

                        $insertOptions = array('quiz_test_question_id' =>$NewQuestionID
                        ,'option_no' => $key
                        ,'option_text' =>$item
                        ,'is_answer' =>$isAnserValue
                        ,'status' => 1 //byDefault Active
                        ,'created_date'=>$now
                        ,'created_by'=>$_SESSION['user_id']
                        ,'updated_date'=>$now
                        ,'updated_by'=>$_SESSION['user_id']
                        );

                        $this->db->insert('quiz_test_question_options', $insertOptions);
                    }
                }

            }

            $this->session->set_flashdata('delmsg','Deleted Question Successfully .');
            redirect('Newquiztest?courseid='.$_POST['courseId'].'&id='.$_POST['quizTestId']);
        }
    }
}