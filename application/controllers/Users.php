<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->model('User_model', 'USM');
		$this->load->model('Coursedetails_model','CSM');
		
	}
	
    
	public function learners()
	{
	    $user_info = $this->session->userdata($session_data);
	    $user_id = $user_info['user_id'];
	    $email=""; 
	    $name="";
	    $blockid = $_GET['blockid'];
	    if(!empty($blockid)){
	    if($blockid == $user_id){
	        $this->session->set_flashdata('msg', 'You cannot block Yourself!');
	    }
	    if($blockid!=$user_id){
	         $this->USM->block_users($blockid);
	    }
	    }
	    if(!empty($_GET['unblockid'])){
	        $unblockid = $_GET['unblockid'];
	         $this->USM->unblock_users($unblockid);
	         
	    }
	    if(isset($_POST['submit'])){
	    if(isset($_POST['name'])){
	        $name= $_POST['name'];
	    }
	    if(isset($_POST['email'])){
	        $email= $_POST['email'];
	    }
	    }
	    $data['users'] = $this->USM->get_learner_users($name,$email);
	    
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		$this->load->view('admin/learners',$data);
	    }else{
	        redirect('Login');
	    }
	}
	public function admins()
	{
	    
	    $user_info = $this->session->userdata($session_data);
	    $user_id = $user_info['user_id'];
	    $email=""; 
	    $name="";
	    
	    if(!empty($_GET['blockid'])){
	        $blockid = $_GET['blockid'];
	        if($user_id == $blockid){
	        
	        $this->session->set_flashdata('msg', 'You cannot block Yourself!');
	        }
	    
	    if($blockid!=$user_id){
	         $this->USM->block_users($blockid);
	         
	    }
	    }
	    
	    
	    if(!empty($_GET['unblockid'])){
	        $unblockid = $_GET['unblockid'];
	         $this->USM->unblock_users($unblockid);
	         
	    }
	    
	    if(isset($_POST['submit'])){
	    if(isset($_POST['name'])){
	        $name= $_POST['name'];
	    }
	    if(isset($_POST['email'])){
	        $email= $_POST['email'];
	    }
	    }
	    $data['users'] = $this->USM->get_admin_users($name,$email);
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		    $this->load->view('admin/admins',$data);
	    }else{
	        redirect('Login');
	    }
	}
	public function instructors()
	{
	    $user_info = $this->session->userdata($session_data);
	    $user_id = $user_info['user_id'];
	    $email=""; 
	    $name="";
	    $id = $_GET['blockid'];
	    if($id == $user_id){
	        $this->session->set_flashdata('msg', 'You cannot block Yourself!');
	    }
	    if(!empty($id)){
	    if($id!=$user_id){
	         $this->USM->block_users($id);
	    }
	    }
	    if(!empty($_GET['unblockid'])){
	        $unblockid = $_GET['unblockid'];
	         $this->USM->unblock_users($unblockid);
	         
	    }
	    if(isset($_POST['submit'])){
	    if(isset($_POST['name'])){
	        $name= $_POST['name'];
	    }
	    if(isset($_POST['email'])){
	        $email= $_POST['email'];
	    }
	    }
	    $data['users'] = $this->USM->get_instructor_users($name,$email);
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		$this->load->view('admin/instructors',$data);
	    }else{
	        redirect('Login');
	    }
	}
	public function edit_user(){
	    $user_info = $this->session->userdata($session_data);
	    $email = $user_info['email'];
	    $today = date("Y-m-d H:i:s");
	    if(!empty($_GET['edit_id'])){
	       $id = $_GET['edit_id'];
	       $data['user'] = $this->USM->get_edit_user($id);
	       
	       $data['device'] = $this->USM->get_all_device($id);
	    }
	    if(isset($_POST['submit'])){
	        
	        $username= $_POST['name'];
	        $mobile= $_POST['mobile'];
	        $city= $_POST['city'];
	        $dob= $_POST['d_o_b'];
	        $role= $_POST['role'];
	         $config['upload_path'] = './assets/profile_image/';
	        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		    $config['encrypt_name'] = true;
		    $this->load->library('upload',$config);
		    if(!$this->upload->do_upload('picture'))
	     	{
		    $error =array('error'=>$this->upload->display_errors());
	    	}
	    	 else{
	    	        $uploaddata=$this->upload->data();
		    	    $filename = base_url('./assets/profile_image/') . $uploaddata['file_name'];
		    	    
	              $this->USM->insert_edit_user_profile($username,$mobile,$city,$dob,$role,$email,$id,$today,$filename);
	    	 }
	    	 $this->USM->insert_edit_user($username,$mobile,$city,$dob,$role,$email,$id,$today);
	    }
	    if($_POST['passubmit']){
	        $new_pass =  $_POST['new_pass'];
	        $conf_pass = $_POST['conf_pass'];
	        if($new_pass == $conf_pass){
	            
	            $this->USM->update_pass($id,$new_pass,$conf_pass);
	        }
	        else{
	            $this->session->set_flashdata('lmsg', 'You entered wrong credientials.');
	        }
	    }
	    $blockid = $_GET['dblockid'];
	    if(!empty($blockid)){
	         $this->USM->block_device($blockid);
	    }
	    $this->load->view('admin/edit_user',$data);
	}
	public function edit_instructor_user(){
	    $user_info = $this->session->userdata($session_data);
	    $email = $user_info['email'];
	    $user_id = $user_info['user_id'];
	    $today = date("Y-m-d H:i:s");
	    
	    $data['course'] = $this->CSM->get_all_course();
	     if(!empty($_GET['edit_id'])){
	       $id = $_GET['edit_id'];
	       $data['user'] = $this->USM->get_edit_user($id);
	    }
	    if(isset($_POST['submit'])){
	        $username= $_POST['name'];
	        $mobile= $_POST['mobile'];
	        $city= $_POST['city'];
	        $dob= $_POST['d_o_b'];
	        $role= $_POST['role'];
	        $assign_course = $_POST['assign_course'];
	        //echo $assign_course;
	        $this->USM->insert_edit_inst_user($username,$mobile,$city,$dob,$role,$assign_course,$email,$user_id,$id,$today);
	      
	    }
	    if($_POST['passubmit']){
	        $new_pass =  $_POST['new_pass'];
	        $conf_pass = $_POST['conf_pass'];
	        if($new_pass == $conf_pass){
	            
	            $this->USM->update_pass($id,$new_pass,$conf_pass);
	        }
	        else{
	            $this->session->set_flashdata('lmsg', 'You entered wrong credientials.');
	        }
	    }
	    
	    $this->load->view('admin/edit_instructor_user',$data);
	}
}