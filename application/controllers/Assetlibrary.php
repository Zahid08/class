<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assetlibrary extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Newchapter_model');
        $this->load->model('Bigbluebutton_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('zoom/Zoom_Api');
        $this->load->library('mpdf/mpdf.php');
    }

	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/assetlibrary');
	    }else{
	        redirect('Login');
	    }
		
	}
	public function pdf_quiz(){
        $getid=$_REQUEST['quiz_id'];
        $quiztest = $this->db->query("SELECT * FROM quiz_test WHERE id='$getid'")->row();
        $newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$getid." ORDER BY id ASC ")->result();
        $this->load->library('mpdf');
        $this->load->library('mpdf/mPDF');
        $pdf=new mPDF();

        $footer = $this->load->view('admin/output_pdf_quiz_footer','',true);
        $pdf->setHTMLFooter($footer);

        $pdf->setAutoTopMargin = 'stretch';
        $pdf->setAutoBottomMargin = 'stretch';
        $pdf->autoScriptToLang = true;
        $pdf->autoLangToFont = true;

        $html=$this->load->view('admin/output_pdf_quiz',array('newQuestions'=>$newQuestions,'quiztest'=>$quiztest),true);
        $pdf->AddPage('', // L - landscape, P - portrait
            '', '', '', '',
            5, // margin_left
            5, // margin right
            5, // margin top
            0, // margin bottom
            0, // margin header
            8
        ); // margin footer

//        echo "<pre>";
//        print_r($html);
//        exit();


        $pdf->WriteHTML($html);
        $output = 'quiz.pdf'; //You can give a name of your generated pdf file or you can create it auto on timestamp by using $output = time().'.pdf'.
        $pdf->Output("$output", 'I');
    }
}
