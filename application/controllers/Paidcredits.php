<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paidcredits extends CI_Controller {
   function __construct()
	{
		parent::__construct();
		$this->load->model('Paidcredits_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	
	public function index()
	{
	     
	       	$this->load->view('admin/paidcredits');
	     
	     
	}
	public function requestcredit()
	{	$creditID = $this->security->xss_clean($this->input->get('credit_id'));
			
			$now = date('Y-m-d');
	     	$updateData = array(
	     	                    'id'=>$creditID,
								'status'=>2,
								'paid_date'=>$now,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
								
							
			$redeemCredit = $this->Paidcredits_model->redeemcredit($updateData,$creditID);
			
		   
			
				if($redeemCredit)
				{
				    
    				    $this->session->set_flashdata('rd_msg', 'Your Request has been placed successfully'); //set success msg if registered successfully
    					redirect('Paidcredits');
				    
				}
				else
				{
					$this->session->set_flashdata('rd_msg','Unable to Place Request. Please try again');
					redirect('Paidcredits');
				}
		
	}
}