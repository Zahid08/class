<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Newlivetest extends CI_Controller {
function __construct()
{
parent::__construct();
$this->load->model('Newchapter_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
public function index()
{
if(($_SESSION['role'] == 1 ) && $_SESSION['status'] == 1){
$this->load->view('admin/course/newlivetest');
}
else if(($_SESSION['role'] == 2) && $_SESSION['status'] == 1){
$this->load->view('instructor/course/newlivetest');
}
else{
redirect('Login');
}
}
public function addlivetest(){
$chapterid = $this->security->xss_clean($this->input->post('chapterid'));
$chaptername = $this->security->xss_clean($this->input->post('chaptername'));
$availableFrom = $this->security->xss_clean($this->input->post('available_from'));
$availableTill = $this->security->xss_clean($this->input->post('available_till'));
$Msg = $this->security->xss_clean($this->input->post('message'));
$courseid   = $this->security->xss_clean($this->input->post('courseid'));
$liveTestid = $this->security->xss_clean($this->input->post('liveTestid'));
$ShowDefaultInstruction = $this->security->xss_clean($this->input->post('ShowDefaultInstruction'));
$ShowScientificCalculator = $this->security->xss_clean($this->input->post('ShowScientificCalculator'));
$postMsg = $this->security->xss_clean($this->input->post('post_message'));
$timeLimit = $this->security->xss_clean($this->input->post('time_limit'));
$availableTime = $this->security->xss_clean($this->input->post('available_time'));
$GenerateResults = $this->security->xss_clean($this->input->post('GenerateResults'));
$ShowLeaderboard = $this->security->xss_clean($this->input->post('ShowLeaderboard'));
$ShowRank = $this->security->xss_clean($this->input->post('ShowRank'));
$Arrangequestionsbytopic = $this->security->xss_clean($this->input->post('Arrangequestionsbytopic'));
$now = date('Y-m-d');

$insertData = array('course_id'=> $courseid,
'title' => $chaptername,
'time_limit'=>$timeLimit,
'available_from'=>$availableFrom,
'available_till'=>$availableTill,
'instructions'=>$Msg,
'is_instructions'=>$ShowDefaultInstruction,
'is_scientific_calc'=>$ShowScientificCalculator,
'show_leaderboard'=>$ShowLeaderboard,
'show_rank'=>$ShowRank,
'results_mode'=> $GenerateResults,
'arrange_by_topic'=>$Arrangequestionsbytopic,
'post_msg'=>$postMsg ,
'min_time_pre_submit'=>$availableTime,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id']
);
$updateData = array('course_id'=> $courseid,
'title' => $chaptername,
'time_limit'=>$timeLimit,
'available_from'=>$availableFrom,
'available_till'=>$availableTill,
'instructions'=>$Msg,
'is_instructions'=>$ShowDefaultInstruction,
'is_scientific_calc'=>$ShowScientificCalculator,
'show_leaderboard'=>$ShowLeaderboard,
'show_rank'=>$ShowRank,
'results_mode'=> $GenerateResults,
'arrange_by_topic'=>$Arrangequestionsbytopic,
'post_msg'=>$postMsg ,
'min_time_pre_submit'=>$availableTime,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id']
);
if($chapterid == 'nil'){
$insertsubData = array(
'course_id'=> $courseid,
'chapterid' => '0',
'type' => 'Live Test',
'chapter_name'=> $chaptername,
'available_from' => $availableFrom,
'available_to' => $availableTill,
'pre_class_msg' =>$Msg,
'post_class_msg' =>$postMsg,
'created_by'=> $_SESSION['user_id'],
'created_date'=>$now);
}else{
$insertsubData = array(
'course_id'=> $courseid,
'chapterid'=> $chapterid,
'type' => 'Live Test',
'subchapter_name' => $chaptername,
'available_from' => $availableFrom,
'available_to' => $availableTill,
'pre_class_msg' =>$Msg,
'post_class_msg' =>$Msg,
'created_by'=> $_SESSION['user_id'],
'created_date'=>$now);
}
$insertLivetest = $this->Newchapter_model->insertlivetest($insertData,$insertsubData,$updateData,$liveTestid,$courseid);
if($insertLivetest)
{
    
    

$this->session->set_flashdata('msg', 'Successfully  Saved.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);
}
else
{
$this->session->set_flashdata('msg','Unable to save course. Please try again.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);
}
$insertLivetests = $this->Newchapter_model->insertlivetest($insertData,$insertsubData,$updateData,$liveTestid,$courseid);
if($insertLivetests)
{

$userid = $_SESSION['user_id'];
$liveTest = $this->db->query("SELECT * FROM live_test WHERE status = 1  AND created_by = '$userid' ORDER BY 1 DESC  ")->result();

$this->session->set_flashdata('msg', 'Successfully Updated.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);

}
else
{
$this->session->set_flashdata('msg','Unable to save course. Please try again.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);
}

}
public function addnewquestions(){
   
$courseid = $this->security->xss_clean($this->input->post('courseid'));

$liveTestid = $this->security->xss_clean($this->input->post('liveTestid'));
$type  = $this->security->xss_clean($this->input->post('type'));

$subjectname = $this->security->xss_clean($this->input->post('subjectname'));
$Topic   = $this->security->xss_clean($this->input->post('Topic'));
$QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

$Explaination = $this->security->xss_clean($this->input->post('explaination'));




$Order = $this->security->xss_clean($this->input->post('Order'));
$Marks= $this->security->xss_clean($this->input->post('Marks'));
$Penalty = $this->security->xss_clean($this->input->post('Penalty'));

$now = date('Y-m-d H:i:s');

$insertquestionsData = array('live_test_id'=>$liveTestid,
'type' => $type,
'subject'=>$subjectname,
'topic'=>$Topic,
'question_text'=>$QuestionText,
'explaination' => $Explaination,
'order'=>$Order,
'mark'=>$Marks,
'penalty'=>$Penalty,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id']
);


$insertNewquestions=$this->Newchapter_model->insertnewquestions($insertquestionsData);

if($insertNewquestions)
{
    //after question has been inserted successfully getting if it and inserting its options
    $CreatedBy = $_SESSION['user_id'];
    $newQuestionID = $this->db->query("SELECT id FROM live_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
        foreach($newQuestionID as $getnewQuestionID){
        $QuestionID = $getnewQuestionID->id;
        }

        for($i=1;$i<=8;$i++){
        	    if(!empty($this->input->post('optionText'.$i))){
        	        $insertOptions = array('live_test_question_id' => $QuestionID
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'is_answer' => $this->input->post('option'.$i) 
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );
                $this->db->insert('live_test_question_options', $insertOptions);
        	    }  
        	}
            
    
$this->session->set_flashdata('msg', 'Question Added Successfully.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);


}


else 
{
$this->session->set_flashdata('msg','Unable to Added question. Please try again.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);
}
}
public function deletequestion()
	{
	    $CourseId = $_GET['courseid'];
	    $liveTestid =$_GET['livetestid'];
	    $questionID = $_GET['questionid'];
	    
	     $this->db->query("UPDATE live_test_question_options SET status = 2 WHERE live_test_question_id = $questionID");
	    $this->db->query("UPDATE live_test_questions SET status = 2 WHERE id = $questionID ");
	   
	     $this->session->set_flashdata('delmsg','Deleted Question Successfully .');
	     redirect('Newlivetest?courseid='.$CourseId.'&id='.$liveTestid);
         
	     
	}
public function editquestions(){
//Deleting existing question and adding it newly on clicking save
$liveTestQuestionID = $this->security->xss_clean($this->input->post('liveTestQuestionid'));
//===========================================================
   
$courseid = $this->security->xss_clean($this->input->post('courseid'));
$liveTestid = $this->security->xss_clean($this->input->post('liveTestid'));
$type  = $this->security->xss_clean($this->input->post('type'));

$Topic   = $this->security->xss_clean($this->input->post('Topic'));
$QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));
$Explaination = $this->security->xss_clean($this->input->post('explaination'));




$Order = $this->security->xss_clean($this->input->post('Order'));
$Marks= $this->security->xss_clean($this->input->post('Marks'));
$Penalty = $this->security->xss_clean($this->input->post('Penalty'));

$now = date('Y-m-d H:i:s');

$insertquestionsData = array('live_test_id'=>$liveTestid,
'type' => $type,
'subject'=>$subjectname,
'topic'=>$Topic,
'question_text'=>$QuestionText,
'explaination' => $Explaination,
'order'=>$Order,
'mark'=>$Marks,
'penalty'=>$Penalty,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id']
);


$insertDeleteNewquestions=$this->Newchapter_model->deleteinsertnewquestions($insertquestionsData,$liveTestQuestionID);

if($insertDeleteNewquestions)
{
    //after question has been inserted successfully getting if it and inserting its options
    $CreatedBy = $_SESSION['user_id'];
    $newQuestionID = $this->db->query("SELECT id FROM live_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
        foreach($newQuestionID as $getnewQuestionID){
        $QuestionID = $getnewQuestionID->id;
        }

        for($i=1;$i<=8;$i++){
        	    if(!empty($this->input->post('optionText'.$i))){
        	        $insertOptions = array('live_test_question_id' => $QuestionID
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'is_answer' => $this->input->post('option'.$i) 
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );
                $this->db->insert('live_test_question_options', $insertOptions);
        	    }  
        	}
            
    
$this->session->set_flashdata('msg', 'Question Updated Successfully.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);


}


else 
{
$this->session->set_flashdata('msg','Unable to Update question. Please try again.');
redirect('Newlivetest?courseid='.$courseid.'&id='.$liveTestid);
}

}
}