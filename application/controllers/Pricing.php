<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Coursedetails_model');
	}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		    $this->load->view('admin/pricing');
	    }else{
	        redirect('Login');
	    }
	}
	public function addpricing()
	{
		$courseId 	= $this->security->xss_clean($this->input->post('courseid'));
		$plan_name 	= $this->security->xss_clean($this->input->post('plan_name'));
		$no_of_days = $this->security->xss_clean($this->input->post('no_of_days'));
		$original_price = $this->security->xss_clean($this->input->post('original_price'));
		$discount = $this->security->xss_clean($this->input->post('discount'));
		$tax 	= $this->security->xss_clean($this->input->post('tax')); 
		
		$now = date('Y-m-d');
		
		/*------------payable price calculation---------*/
		$payable_price = $original_price;
		$payable_price = $payable_price - ($discount/100) * $payable_price; //deduct discount amt from original price
		$payable_price = $payable_price + ($tax/100) * $payable_price; //add tax to original price
		/*------------------------------------------------*/
		
		$insertData = array(
			                    'course_id'=> $courseId,
			                    'plan_name'=> $plan_name,
			                    'no_of_days'=>$no_of_days,
			                    'original_price'=>$original_price,
								'discount'=>$discount,
								'tax'=>$tax,
								'payable_price'=>$payable_price ,
								'status'=>1,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
				
			$insertPrice = $this->Coursedetails_model->insertprice($insertData);
			$InsertId = $_SESSION['user_id'];
			
			if($insertPrice)
			{
			    $getmaxId = $this->db->query("SELECT max(course_id) as id FROM course where created_by = '$InsertId' ");
                foreach($getmaxId->result() as $maxId){
                        $Id = $maxId->id ;  
                }
			    $this->session->set_flashdata('price_msg', 'Price Successfully Added'); //set success msg if registered successfully
				redirect('Marketing?courseid='.$Id);
			}
			else
			{
				$this->session->set_flashdata('price_msg','Unable to save course. Please try again');
				redirect('Pricing?courseid='.$Id);
			}
	}
	
	public function deleteprice()
	{
			$courseid = $this->security->xss_clean($this->input->get('courseid'));
			$priceid = $this->security->xss_clean($this->input->get('priceid'));
			
			$now = date('Y-m-d');
			$deleteData = array(
								'status'=>2,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$deletePrice = $this->Coursedetails_model->deleteprice($deleteData,$courseid,$priceid);
			
		    $InsertId = $_SESSION['user_id'];
			
				if($deletePrice)
				{
				    
    				    $this->session->set_flashdata('ps_msg', 'Price Data Successfully Deleted'); //set success msg if registered successfully
    					redirect('Pricing?courseid='.$courseid);
				    
				}
				else
				{
					$this->session->set_flashdata('pf_msg','Unable to Delete Price Data. Please try again');
					redirect('Pricing?courseid='.$courseid);
				}
		
	}
	
}
