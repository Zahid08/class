<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newchapter extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Newchapter_model');
		$this->load->model('Bigbluebutton_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('zoom/Zoom_Api');
	}
	public function index()
	{
	     if(($_SESSION['role'] == 1 ) && $_SESSION['status'] == 1){
		    $this->load->view('admin/course/newchapter');
	     }
	     else if(($_SESSION['role'] == 2) && $_SESSION['status'] == 1){
		    $this->load->view('instructor/course/newchapter');
	     }
	     else{
	         redirect('Login');
	     }
	}
	public function addchapter()
	{
	        $heading = $this->security->xss_clean($this->input->post('heading'));
	    
	        $subchapterid = $this->security->xss_clean($this->input->post('subchapterid'));
	        $chapterid = $this->security->xss_clean($this->input->post('chapterid'));
	        $courseid 	= $this->security->xss_clean($this->input->post('courseid'));
	        $chaptername = $this->security->xss_clean($this->input->post('chaptername'));
			$type = $this->security->xss_clean($this->input->post('type'));
			$subtype = $this->security->xss_clean($this->input->post('sub_type'));
			
			
		
			
			if($subtype == 'upload' && $config['max_size']<=40){
			    
			$path = $this->security->xss_clean($this->input->post('pdf_file'));
			$config['upload_path']   = './assets/uploads/';
	        $config['allowed_types'] = '*';
	        $config['max_size']      = 40;
            
		    $config['encrypt_name']  = true;
		    $this->load->library('upload',$config);
		    if(!$this->upload->do_upload('pdf_file'))
	     	{
		    $error =array('error'=>$this->upload->display_errors());
	    	}
	    	 else{
	    	        $uploaddata=$this->upload->data();
		    	    $filename = './assets/uploads/' . $uploaddata['file_name'];
		    	     $this->session->set_flashdata('pdfmsg','File upload Successfully .');
	    	     }
	               
			     
			}
			else if($config['max_size']>40){
			    $this->session->set_flashdata('pdfmsg','File your upload is out of limit .It should be 0 to 40 kb only');
			   
			}
			else if($subtype == 'public_url'){
			    $path = $this->security->xss_clean($this->input->post('pdf_link'));
			    $filename = $path;
			    
			}else{
			    $subtype = $subtype;
			}
			
			
			$availability = $this->security->xss_clean($this->input->post('availability'));
			$availableFrom = $this->security->xss_clean($this->input->post('available_from'));
			$availableDate = $this->security->xss_clean($this->input->post('available_date'));
			$availableTo = $this->security->xss_clean($this->input->post('available_to'));
			$availableTime = $this->security->xss_clean($this->input->post('available_time'));
			$preClassMsg = $this->security->xss_clean($this->input->post('pre_class_msg'));
			$postClassMsg = $this->security->xss_clean($this->input->post('post_class_msg'));
			$showRecording = $this->security->xss_clean($this->input->post('is_record'));
			$emailPush = $this->security->xss_clean($this->input->post('email_push'));
			$mobilePush = $this->security->xss_clean($this->input->post('mobile_push'));
			$webPush = $this->security->xss_clean($this->input->post('web_push'));
			
		
                
		
			$now = date('Y-m-d');
			
			
			
			if($chapterid == 'nil'){
			    $insertData = array(
			                    'course_id'=> $courseid,
			                    'chapter_name'=> $chaptername,
			                    'type' => $type,
			                    'sub_type' =>$subtype,
			                    'path' => $filename,
			                    'availability' => $availability,
			                    'available_from' => $availableFrom,
			                    
			                    'available_to' => $availableTo,
			                    'pre_class_msg' =>$preClassMsg,
			                    'post_class_msg' =>$postClassMsg,
			                    'show_recording' =>$showRecording,
			                    'email_push' =>$emailPush,
			                    'mobile_push' =>$mobilePush,
			                    'web_push' =>$webPush,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
			}else{
			    $insertData = array(
			                    'course_id'=> $courseid,
			                    'chapterid' => $chapterid,
			                    'subchapter_name' => $chaptername,
			                    'type' => $type,
			                    'sub_type' =>$subtype,
			                    'path' =>$filename,
			                    'availability' => $availability,
			                     'available_from' => $availableFrom,
			                    'available_date' => $availableDate,
			                    'available_time' => $availableTime,
			                    'available_to' => $availableTo,
			                    'pre_class_msg' =>$preClassMsg,
			                    'post_class_msg' =>$postClassMsg,
			                    'show_recording' =>$showRecording,
			                    'email_push' =>$emailPush,
			                    'mobile_push' =>$mobilePush,
			                    'web_push' =>$webPush,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
			}
 
			
			$insertCourse = $this->Newchapter_model->insertchapter($insertData,$chapterid,$subchapterid,$courseid,$subtype,$chaptername,$availableFrom);
			
				if($insertCourse)
				{
				   
				        $userid = $_SESSION['user_id'];
				       $subChapterId=$this->db->query("SELECT id as subChapterId  FROM sub_chapters where created_by = '$userid' and status = 1 ORDER BY 1 DESC LIMIT 1")->result();
				       foreach($subChapterId as $getsubChapterId){
				           $subChapterId=$getsubChapterId->subChapterId;
				       }
                        
    				    if($subChapterId == $_GET['itemid']){
    				        	redirect('Coursebuilder?courseid='.$courseid);
    				    }else{
    				        $this->session->set_flashdata('msg', 'Successfully Chapter Saved.');
    				        redirect('EditItem?courseid='.$courseid.'&chapterid='.$chapterid.'&itemid='.$subChapterId);
    				         
    				    }
    					
				    
				}
				else
				{
					$this->session->set_flashdata('msg','Unable to save course. Please try again.');
					redirect('Coursebuilder?courseid='.$courseid);
				}
	      
		
	}
	public function addrecordinglink()
	{
	        $subChapterId = $this->security->xss_clean($this->input->post('itemid'));
	        $courseid = $this->security->xss_clean($this->input->post('courseid'));
	        $chapterid = $this->security->xss_clean($this->input->post('chapterid'));
	    
	        $link = $this->security->xss_clean($this->input->post('recorded_link'));
	        $now = date('Y-m-d H:i:s');
			$insertData = array(
			                    'sub_chapter_id'=> $subChapterId,
			                    'link'=> $link,
			                    'status' => 1,
								'created_by'=> $_SESSION['user_id'],
								'created_datetime'=>$now);
		    $updateData = array(
			                    'sub_chapter_id'=> $subChapterId,
			                    'link'=> $link,
			                    'status' => 1,
								'updated_by'=> $_SESSION['user_id'],
								'updated_datetime'=>$now);
	
 
			
			$insertRecording = $this->Newchapter_model->insertrecording($insertData,$updateData,$subChapterId);
			
				if($insertRecording)
				{
				        
    				    $this->session->set_flashdata('msg', 'Successfully  Saved.');
    				    redirect('EditItem?courseid='.$courseid.'&chapterid='.$chapterid.'&itemid='.$subChapterId);
				    
				}
				else
				{
					$this->session->set_flashdata('msg','Unable to save course. Please try again.');
					redirect('Coursebuilder?courseid='.$courseid);
				}
			
	  	
	}
	
	
}

 