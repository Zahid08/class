<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Coursedetails_model');
	}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		$this->load->view('admin/marketing');
	    }else{
	        redirect('Login');
	    }
	}
	public function addmarketing()
	{
	    $courseId = $this->security->xss_clean($this->input->post('courseid'));
		$course_page_keywords	= $this->security->xss_clean($this->input->post('course_page_keywords'));
		$course_page_description 	= $this->security->xss_clean($this->input->post('course_page_description'));
		
		$now = date('Y-m-d');
		
		
		$updateData = array(
			                    'course_id'=> $courseId,
			                    'metadata_keywords'=> $course_page_keywords,
			                    'metadta_desc'=>$course_page_description,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
				
			$updateMarketing = $this->Coursedetails_model->updatemarketing($updateData);
			$InsertId = $_SESSION['user_id'];
			
			if($updateMarketing)
			{
			    $getmaxId = $this->db->query("SELECT max(course_id) as id FROM course where created_by = '$InsertId' ");
                foreach($getmaxId->result() as $maxId){
                        $Id = $maxId->id ;  
                }
			    $this->session->set_flashdata('marketing_msg', 'Marketing Data Successfully Added'); //set success msg if registered successfully
				redirect('Courses');
			}
			else
			{
				$this->session->set_flashdata('marketing_msg','Unable to save course. Please try again');
				redirect('Marketing?courseid='.$Id);
			}
	}
}
