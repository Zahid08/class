<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lepurchasehistory extends CI_Controller {
     function __construct()
	{
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->model('User_model', 'USM');
		
		
	}
	public function index()
	{
	    
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		    $this->load->view('admin/lpurchasehistory');
	    }else{
	        redirect('Login');
	    }
	}
}