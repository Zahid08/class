<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LPurchasehistory extends CI_Controller {
     function __construct()
	{
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->model('User_model', 'USM');
		$this->load->model('Coursedetails_model','CSM');
		
	}
	public function index()
	{
	    $user_info = $this->session->userdata($session_data);
	    $user_id = $user_info['user_id'];
	    $data['transection'] = $this->USM->get_trnasection_history($user_id);
	    if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
		    $this->load->view('learner/purchasehistory',$data);
	    }else{
	        redirect('Login');
	    }
	}
}
