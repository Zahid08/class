<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recording extends CI_Controller {
    
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/course/recordingvdo.php');
	    }
	     else if(($_SESSION['role'] == 2) && $_SESSION['status'] == 1 ){
		    $this->load->view('instructor/course/recordingvdo.php');
	     }
	     else if(($_SESSION['role'] == 3) && $_SESSION['status'] == 1 ){
		    $this->load->view('learner/recordingvdo.php');
	     }
	     else{
	        redirect('Login');
	    }
		
	}
}
