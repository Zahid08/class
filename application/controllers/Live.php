<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Live extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Newchapter_model');
        $this->load->model('Bigbluebutton_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}

	//Join Moderator Or Teacher Or Admin
	public function index()
	{
	     if(($_SESSION['role'] == 1 && $_SESSION['status'] == 1) || ($_SESSION['role'] == 2 && $_SESSION['status'] == 1) || ($_SESSION['role'] == 3 && $_SESSION['status'] == 1)){
	         $type=$_REQUEST['type'];
	         if ($type=='bbb'){

                 $subChapterId   = $_REQUEST['sub_chapter_id'];
                 $courseId       = $_REQUEST['courseid'];
                 $chapterid      = $_REQUEST['courseid'];


                 if ($subChapterId) {
                     $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();
                     
                     $userName =$_SESSION['username'];
                     $userId  = $_SESSION['unique_id'];
                     foreach ($bbsettingData as $item) {
                         $mettingId = $item->meetingId;
                     }

                     //Check Meeting Are exist or not on server if not exist then create meeting again
                     $infoParams = array(
                         'meetingId' =>$mettingId, 		// REQUIRED - We have to know which meeting.
                         'password' => 'mp',			// REQUIRED - Must match moderator pass for meeting.

                     );
                     $CheckMeetingsAllGood = true;
                     try {$result = $this->Bigbluebutton_model->getMeetingInfoWithXmlResponseArray($infoParams);}
                     catch (Exception $e) {
                         echo 'Caught exception: ', $e->getMessage(), "\n";
                         $CheckMeetingsAllGood = false;
                     }

                     if ($CheckMeetingsAllGood == true) {
                         // If it's all good, then we've interfaced with our BBB php api OK:
                         if ($result == null) {
                             // If we get a null response, then we're not getting any XML back from BBB.
                             echo "Failed to get any response. Maybe we can't contact the BBB server.";
                         }
                         else {
                             if (!isset($result['messageKey'])) {
                                 // Then do stuff ...
                                 //echo "<p>Meeting info was found on the server.</p>";
                                 $joinParams = array(
                                     'meetingId' =>$mettingId, 				    // REQUIRED - We have to know which meeting to join.
                                     'username' =>$userName,		            // REQUIRED - The user display name that will show in the BBB meeting.
                                     'password' => 'mp',					    // REQUIRED - Must match either attendee or moderator pass for meeting.
                                     'createTime' => '',					    // OPTIONAL - string
                                     'userId' =>$userId,						// OPTIONAL - string
                                     'webVoiceConf' => ''				        // OPTIONAL - string
                                 );

                                 // Get the URL to join meeting:
                                 $JoinMeetingAsModeratorAllGood = true;
                                 try {$result = $this->Bigbluebutton_model->getJoinMeetingURL($joinParams);}
                                 catch (Exception $e) {
                                     echo 'Caught exception: ', $e->getMessage(), "\n";
                                     $JoinMeetingAsModeratorAllGood = false;
                                 }

                                 if ($JoinMeetingAsModeratorAllGood == true) {
                                     redirect($result); // Redirect User to Logout Page to destroy session
                                 }

                             }
                             else {
                                 $backUrl=base_url().'EditItem/endbbbclass?courseid='.$courseId.'&chapterid='.$chapterid.'&itemid='.$subChapterId.'';  //Return Back url after close meeting

                                 $subChapterData = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$courseId' AND id= '$subChapterId' ORDER BY 1 DESC")->result();
                                 foreach($subChapterData as $getsubChapterData){
                                     $subChapterNameItem = $getsubChapterData->subchapter_name;
                                 }
                                 $creationParams = array(
                                     'meetingId' =>$mettingId, 					        // REQUIRED
                                     'meetingName' => ''.$subChapterNameItem.'', 	// REQUIRED
                                     'attendeePw' => 'ap', 					            // Match this value in getJoinMeetingURL() to join as attendee.
                                     'moderatorPw' => 'mp', 					            // Match this value in getJoinMeetingURL() to join as moderator.
                                     'welcomeMsg' => '', 					            // ''= use default. Change to customize.
                                     'dialNumber' => '', 					            // The main number to call into. Optional.
                                     'voiceBridge' => '', 					            // PIN to join voice. Optional.
                                     'webVoice' => '', 						            // Alphanumeric to join voice. Optional.
                                     'logoutUrl' =>$backUrl, 						    // Default in bigbluebutton.properties. Optional.
                                     'maxParticipants' => '-1', 				            // Optional. -1 = unlimitted. Not supported in BBB. [number]
                                     'record' => 'true', 					            // New. 'true' will tell BBB to record the meeting.
                                     'publish' => 'true',
                                     'duration' =>0, 						            // Default = 0 which means no set duration in minutes. [number]
                                     //'meta_category' => '', 				            // Use to pass additional info to BBB server. See API docs.
                                 );


                                 $createNewMettingAgainStatus = true;
                                 try {
                                     $result = $this->Bigbluebutton_model->createMeetingWithXmlResponseArray($creationParams);}
                                 catch (Exception $e) {
                                     echo 'Caught exception: ', $e->getMessage(), "\n";
                                     $createNewMettingAgainStatus = false;
                                 }

                                 if ($createNewMettingAgainStatus == true) {
                                     // If it's all good, then we've interfaced with our BBB php api OK:
                                     if ($result == null) {
                                         // If we get a null response, then we're not getting any XML back from BBB.
                                         echo "Failed to get any response. Maybe we can't contact the BBB server.";
                                         return false;
                                     }
                                     else {
                                         if ($result['returncode'] == 'SUCCESS') {
                                             // Then do stuff ...
                                             //echo "<p>Meeting info was found on the server.</p>";
                                             $joinParams = array(
                                                 'meetingId' =>$result['meetingId'], 				    // REQUIRED - We have to know which meeting to join.
                                                 'username' =>$userName,		            // REQUIRED - The user display name that will show in the BBB meeting.
                                                 'password' => 'mp',					    // REQUIRED - Must match either attendee or moderator pass for meeting.
                                                 'createTime' => '',					    // OPTIONAL - string
                                                 'userId' =>$userId,						// OPTIONAL - string
                                                 'webVoiceConf' => ''				        // OPTIONAL - string
                                             );

                                             // Get the URL to join meeting:
                                             $JoinMeetingAsModeratorAllGood = true;
                                             try {$result = $this->Bigbluebutton_model->getJoinMeetingURL($joinParams);}
                                             catch (Exception $e) {
                                                 echo 'Caught exception: ', $e->getMessage(), "\n";
                                                 $JoinMeetingAsModeratorAllGood = false;
                                             }

                                             if ($JoinMeetingAsModeratorAllGood == true) {
                                                 redirect($result); // Redirect User to Logout Page to destroy session
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }

                     exit();
                 }
             }else {
                 $this->load->view('admin/course/liveclass');
             }
	     }else{
	         redirect('Login');
	     }
	}


	//Join Student
    public function joinStudent()
    {
        if(1 == 1){
        /*if(($_SESSION['role'] == 1 && $_SESSION['status'] == 1) || ($_SESSION['role'] == 2 && $_SESSION['status'] == 1) || ($_SESSION['role'] == 3 && $_SESSION['status'] == 1)){*/
            $type=$_REQUEST['type'];
            if ($type=='bbb'){
                $subChapterId   = $_REQUEST['sub_chapter_id'];
                $courseId       = $_REQUEST['courseid'];
                $chapterid      = $_REQUEST['courseid'];

                $subChapterData = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$courseId' AND id= '$subChapterId' ORDER BY 1 DESC")->result();
                foreach($subChapterData as $getsubChapterData){
                    $learNerWatch = $getsubChapterData->learner_watched;
                }

                if ($subChapterId) {
                    $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();

                    /*$userName =$_SESSION['username'];
                    $userId  = $_SESSION['unique_id'];*/
                    
                    $userName =$_REQUEST['username'];
                    $userId  = $_REQUEST['unique_id'];
                    
                    
                    /*$userName ='Kavya';
                    $userId  = 'CEG123456';*/
                    foreach ($bbsettingData as $item) {
                        $mettingId = $item->meetingId;
                    }

                    //Check That Meeitng Are Running Get the URL to join meeting:
                    $meetingId = $mettingId;
                    $meetingRunningCheckStatus = true;
                    try {$resultMeetingRunningCheck = $this->Bigbluebutton_model->isMeetingRunningWithXmlResponseArray($meetingId);}
                    catch (Exception $e) {
                        echo 'Caught exception: ', $e->getMessage(), "\n";
                        $meetingRunningCheckStatus = false;
                    }

                    if ($meetingRunningCheckStatus == true) {
                        //Output results to see what we're getting:
                        if ($resultMeetingRunningCheck['running']=='true'){
                             //If Meeting Running Then Join
                            $joinParams = array(
                                'meetingId' =>$mettingId, 				// REQUIRED - We have to know which meeting to join.
                                'username' =>$userName,		            // REQUIRED - The user display name that will show in the BBB meeting.
                                'password' => 'ap',					    // REQUIRED - Must match either attendee or moderator pass for meeting.
                                'createTime' => '',					    // OPTIONAL - string
                                'userId' =>$userId,						// OPTIONAL - string
                                'webVoiceConf' => ''				    // OPTIONAL - string
                            );

                            // Get the URL to join meeting:
                            $itsAllGood = true;
                            try {$result = $this->Bigbluebutton_model->getJoinMeetingURL($joinParams);}
                            catch (Exception $e) {
                                echo 'Caught exception: ', $e->getMessage(), "\n";
                                $itsAllGood = false;
                            }
                            if ($itsAllGood == true) {
                                $_SESSION['setmeetingMessage']='';

                                //Update Watched Value
                                $data = array(
                                    'learner_watched' =>$learNerWatch+1
                                );

                                $this->db->set($data);
                                $this->db->where('id',$subChapterId);
                                $this->db->update('sub_chapters');



                                redirect($result); // Redirect User to Logout Page to destroy session
                            }
                        }else{
                            $redirectUrl=base_url().'CourseLearners?courseid='.$courseId.'&itemid='.$subChapterId.'';
                            redirect($redirectUrl);
                        }
                    }
                    exit();
                }
            }else {
                $this->load->view('learner/coursedetails');
            }
        }else{
            redirect('Login');
        }
    }
	
}
