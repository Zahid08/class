<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storedetails extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	    $this->load->model('Coursedetails_model', 'CSM');
	}
	public function index()
	{   
	    
	   
	     $data['luser'] = $this->CSM->get_luser_course();
	     
	    $data['sales_report']= $this->CSM->get_all_course();
	    $data['course_price']= $this->CSM->get_all_price();
	    if(isset($_POST['comm_submit'])){
	        $comment = $_POST['comment'];
	        $id =  $_GET['id'];
	        if(!empty($id)){
	        $this->CSM->insert_comment($comment,$id);
	    }
	    }
	    
	    
	    if(isset($_POST['submit'])){
	        
	        $user_info = $this->session->userdata($session_data);
	        $user_id = $user_info['user_id'];
	        $email = $_POST['email_id'];
	        $course_id = $_POST['course_id'];
	        $payment_mode = $_POST['payment_mode'];
	        $amount = $_POST['amount'];
	        $transactioncode = $_POST['tran_code'];
	        $user['info']=$this->CSM->get_user_info($email);
	        $t = $user['info'];
	        if(count($user) > 0){
	            foreach($t as $user)
	            $learner_id = $user['id'];
	            if(!empty($learner_id)){
	            $this->CSM->insert_transaction_detail($user_id,$learner_id,$course_id,$payment_mode,$amount,$transactioncode);
	        }else{
	            $this->session->set_flashdata('smsg', 'Your Email-ID is incorrect');
	        }
	        }
	        
	        }
	            
	        if(isset($_POST['search'])){
  	      
	            $type = $_POST['trans_type'];
	            $status = $_POST['status'];
	           $data['search']= $this->CSM->get_searched_data($status,$type); 
	        }/*else{
	        $data['transaction'] = $this->CSM->get_all_transaction();
	        }*/

	        
	        $data['user']= $this->CSM->get_all_users();
	     /*if(($_SESSION['role'] == 3) && $_SESSION['status'] == 1){*/
	         
		    $this->load->view('home/storedetails');
	    /* }else{
	         redirect('Login');
	     }*/
	}


}
