<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Setting_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	public function index()
	{
	     if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		    $this->load->view('admin/setting');
	     }else{
	         redirect('Login');
	     }
	}
	public function payu()
	{
	        $key = $this->security->xss_clean($this->input->post('merchantkey'));
			$salt = $this->security->xss_clean($this->input->post('merchantsalt'));
			
			
			
			$now = date('d-m-y');
 
			$updateKey = array(
			                    'value'=> $key,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$updateSalt = array(
			                    'value'=> $salt,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			
			
			$updatePayment = $this->Setting_model->payu($updateKey,$updateSalt);
			
				if($updatePayment)
				{
				    $this->session->set_flashdata('payment_msg','PayU Details Modified Successfully.');
					redirect('Setting');
				}
				
		
	}
	
	public function credit()
	{
	        $credit = $this->security->xss_clean($this->input->post('credit'));
			
			
			
			$now = date('d-m-y');
 
			$updateCredit = array(
			                    'value'=> $credit,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			
			
			$updateCreditData = $this->Setting_model->credit($updateCredit);
			
				if($updateCreditData)
				{
				    $this->session->set_flashdata('credit_msg','Credit Details Modified Successfully.');
					redirect('Setting');
				}
				
		
	}
	public function devices()
	{
	        $devices = $this->security->xss_clean($this->input->post('devices'));
			
			
			
			$now = date('d-m-y');
 
			$updateDevices = array(
			                    'value'=> $devices,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			
			
			$updateDevicesData = $this->Setting_model->devices($updateDevices);
			
				if($updateDevicesData)
				{
				    $this->session->set_flashdata('devices_msg','Devices Count Details Modified Successfully.');
					redirect('Setting');
				}
				
		
	}
	public function referandEarn()
	{
	        
			$referredCreditsValidity = $this->security->xss_clean($this->input->post('referredCreditsValidity'));
			$maxReferralCount = $this->security->xss_clean($this->input->post('maxReferralCount'));
			$referrerCreditsSignup = $this->security->xss_clean($this->input->post('referrerCreditsSignup'));
			$referreeCreditsSignup = $this->security->xss_clean($this->input->post('referreeCreditsSignup'));
			$referrerCreditsFirstTxn = $this->security->xss_clean($this->input->post('referrerCreditsFirstTxn'));
			$referreeCreditsFirstTxn = $this->security->xss_clean($this->input->post('referreeCreditsFirstTxn'));
			$now = date('Y-m-d');
            $updateReferredCreditsValidity = array(
			                    'value'=> $referredCreditsValidity,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$updateMaxReferralCount = array(
			                    'value'=> $maxReferralCount,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
	    	$updateReferrerCreditsSignup = array(
			                    'value'=> $referrerCreditsSignup,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
		    $updateReferreeCreditsSignup = array(
			                    'value'=> $referreeCreditsSignup,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);	
			$updateReferrerCreditsFirstTxn = array(
			                    'value'=> $referrerCreditsFirstTxn,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$updateReferreeCreditsFirstTxn = array(
			                    'value'=> $referreeCreditsFirstTxn,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);					
			$updateReferandEarn = $this->Setting_model->referandEarn($updateReferredCreditsValidity,$updateMaxReferralCount,$updateReferrerCreditsSignup, $updateReferreeCreditsSignup,$updateReferrerCreditsFirstTxn,$updateReferreeCreditsFirstTxn);
			
				if($updateReferandEarn)
				{
				    $this->session->set_flashdata('referandEarn_msg','Refer & Earn Details Modified Successfully.');
					redirect('Setting');
				}
				
		
	}
		
	
}
