<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accesscodes extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->model('Promocode_model', 'PSM');
		
	}
    
	public function index()
	{
	   /**$user_info = $this->session->userdata($session_data);
	    $user_id = $user_info['user_id'];
	    if(isset($_POST['submit'])){

	            $code= $_GET['code'];
	            
	            $data['users'] = $this->PSM->get_searched_promocode($code);
	        }
	        else{
	            $data['users'] = $this->PSM->get_all_promocode();
	        }
	        **/
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		$this->load->view('admin/accesscodes');
	    }else{
	        redirect('Login');
	    }
	}
}
