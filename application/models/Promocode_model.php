<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Promocode_model extends CI_Model {
	
	function promocode($insertData,$courseData)
	{
	       if($courseData==0){
	          if($this->db->insert('wishlist', $insertData))
    		{
    		    
    		    $this->session->set_flashdata('cm_msg', 'Course has been added Successfully..click Home and u can check in My Courses ');
    		    redirect('home');
    		    return true;
    		}
    		else
    		{
    			return false;
    		} 
	       }
	       else{
	    
	       if($this->db->insert('wishlist', $insertData))
    		{
    		    
    		    
    		    $this->load->view('home/promocode');
    		    return true;
    		}
    		else
    		{
    			return false;
    		}
	       }
			
	   }
	function insertpromocode($data,$promocode_id)
	{
	    if($promocode_id == 0){
	        if($this->db->insert('promo_codes', $data))
    		{
    		    return true;
    		}
    		else
    		{
    			return false;
    		}
	    }else{
	        $now = date('Y-m-d');
	        
	        
	        $updatedata = array(
    		'code'=> $data['code'],
			'count'=> $data['count'],
			'discount'=>$data['discount'],
			'max_date' => $data['max_date'],
			'min_cart' => $data['min_cart'],
			'status'=>1,
			'created_by'=> $_SESSION['user_id'],
			'created_date'=>$now
    		);
		
	        
	        
	        $this->db->where('id', $promocode_id);
    		if($this->db->update('promo_codes', $updatedata))
    		{
    		    return true;
    		}
    		else
    		{
    			return false;
    		}
	    }	
		
	}
	
	function insertcourseprmocode($data)
	{
	    		
		if($this->db->insert('course_promo_codes', $data))
		{
		    return true;
		}
		else
		{
			return false;
		}
	}

	function deletecourse($deleteData,$courseid)
	{
	    
		$status = $deleteData['status'];
		$updated_by = $deleteData['updated_by'];
		$updated_date = $deleteData['updated_date'];
	    
	    $DeleteCourse = $this->db->query("UPDATE course SET status = '$status',updated_by = '$updated_by', updated_date = '$updated_date'  WHERE course_id = '$courseid' ");
	    
		if($DeleteCourse)
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	function deletecodecourse($deleteData,$coursecodeid)
	{
	    
		$status = $deleteData['status'];
		$updated_by = $deleteData['updated_by'];
		$updated_date = $deleteData['updated_date'];
	    
	    $DeleteCoursecode = $this->db->query("UPDATE course_promo_codes SET status = '$status',updated_by = '$updated_by', updated_date = '$updated_date'  WHERE id = '$coursecodeid' ");
	    
		if($DeleteCoursecode)
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	function get_all_promocode(){
	    $query = $this->db->query("SELECT * FROM promo_codes p inner join course c  ON p.course_id = c.course_id WHERE p.status= '1' ");
	    if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('precord',$record);
            return $query->result_array();
        } else {
            return array();
        }
	}
	function delete_course($deleteid){
	    $this->db->query("UPDATE promo_codes SET status = '2' WHERE id = '".$deleteid."' ");
        
        if($this->db->trans_status()){
              $this->session->set_flashdata('msg', 'Course Deleted !');
            return 1;    
        }else{
            return 0;
        }
	}
	function get_searched_promocode($code,$package){
	    if(!empty($code)){
	    $query = $this->db->query("SELECT * FROM promo_codes WHERE code = '".$code."' ");
	    if ($query->num_rows() > 0) {
	        $record = $query->num_rows();
	        $this->session->set_userdata('precord',$record);
            return $query->result_array();
        } else {
           return array();
	}
	    }
	   }
	   function get_all_credits($user_id,$email){
	       $query = $this->db->query("SELECT * FROM credits WHERE child_id = '".$email."' ");
	    if ($query->num_rows() > 0) {
	        $record = $query->num_rows();
	        $this->session->set_userdata('crecord',$record);
            return $query->result_array();
        } else {
           return array();
	}
	   }
}