<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form','url');
    }
    function get_admin_users($name,$email){
        if(!empty($name) || !empty($email)){
            if(empty($name)){
                $name = "";
            }
            if(empty($email)){
                $email = "";
            }
        $where = ""; 
        $first = 0;
           if(!empty($name)){
               if($first == 0){
                   $where .= "AND (username LIKE '%".$name."%'";
                   $first = 1;
               }
               
           } 
           if(!empty($email)){
               if($first == 0){
                   $where .= "AND (email_id LIKE '%".$email."%'";
                   $first = 1;
               }else if($first == 1){
                   $where .= "OR email_id LIKE '%".$email."%'";
               }
               
           } 
           $where .= ")"; 
            $query = $this->db->query("SELECT * FROM users WHERE role = 1  $where ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
        }
        else{
        $query = $this->db->query("SELECT * FROM users WHERE role= '1' ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
        }
    }
    function get_instructor_users($name,$email){
        if(!empty($name) || !empty($email)){
            if(empty($name)){
                $name = "";
            }
            if(empty($email)){
                $email = "";
            }
            $where = ""; 
        $first = 0;
           if(!empty($name)){
               if($first == 0){
                   $where .= "AND (username LIKE '%".$name."%'";
                   $first = 1;
               }
               
           } 
           if(!empty($email)){
               if($first == 0){
                   $where .= "AND (email_id LIKE '%".$email."%'";
                   $first = 1;
               }else if($first == 1){
                   $where .= "OR email_id LIKE '%".$email."%'";
               }
               
           } 
           $where .= ")"; 
            $query = $this->db->query("SELECT * FROM users WHERE role = 2 $where ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
        }
        else{
        $query = $this->db->query("SELECT * FROM users WHERE role= '2' ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
        }
        
    }
    function get_learner_users($name,$email){
            if(!empty($name) || !empty($email)){
            if(empty($name)){
                $name = "";
            }
            if(empty($email)){
                $email = "";
            }
            $where = ""; 
        $first = 0;
           if(!empty($name)){
               if($first == 0){
                   $where .= "AND (username LIKE '%".$name."%'";
                   $first = 1;
               }
               
           } 
           if(!empty($email)){
               if($first == 0){
                   $where .= "AND (email_id LIKE '%".$email."%'";
                   $first = 1;
               }else if($first == 1){
                   $where .= "OR email_id LIKE '%".$email."%'";
               }
               
           } 
           $where .= ")"; 
            $query = $this->db->query("SELECT * FROM users WHERE role = 3 $where ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
        }
        else{
        $query = $this->db->query("SELECT * FROM users WHERE role= '3' ");
        if ($query->num_rows() > 0) {
             $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
        }
        }
    function get_all_active_users(){
        $query = $this->db->query("SELECT * FROM users WHERE status = '1' ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            $this->session->set_userdata('record',$record);
            return $query->result_array();
        } else {
            return array();
        }
    }
    function block_users($id){
        $this->db->query("UPDATE users SET status = '2' WHERE id = '".$id."' ");
        
        if($this->db->trans_status()){
              $this->session->set_flashdata('msg', 'User Blocked !');
            return 1;    
        }else{
            $this->session->set_flashdata('msg', 'Unable to Block please try again later !');
            return 0;
        }
    }
        function unblock_users($unblockid){
        $this->db->query("UPDATE users SET status = '1' WHERE id = '".$unblockid."' ");
        
        if($this->db->trans_status()){
           $this->session->set_flashdata('msg', 'User Unblocked!');
            return 1;    
        }else{
            return 0;
        }
    }
    function get_edit_user($id){
        $query = $this->db->query("SELECT * FROM users WHERE id = '".$id."' ");
        if ($query->num_rows() > 0) {
            $record = $query->num_rows();
            return $query->result_array();
        } else {
            return array();
        }
    }
    function insert_edit_user($username,$mobile,$city,$dob,$role,$email,$id,$today){
        $this->db->query("UPDATE users SET  username = '".$username."' , mobile = '".$mobile."' , dob = '".$dob."' ,profile_picture='".$filename."', city = '".$city."' , role = '".$role."' , created_datetime = '".$today."' , created_by = '".$email."'   WHERE id = '".$id."' ");
        
        if($this->db->trans_status()){
           $this->session->set_flashdata('lmsg', 'User updated succesfully!');
            return 1;    
        }else{
            return 0;
        }
    }
    function insert_edit_user_profile($username,$mobile,$city,$dob,$role,$email,$id,$today,$filename){

       $data = array(
            	'username' => $username,
            	'profile_picture' => $filename,
            	'mobile' => $mobile,
            	'dob' => $dob,
            	'city' => $city,
            	'role' =>$role,
            	'updated_datetime' => $today,
            	'updated_by' => $email
            	);
            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update('users');
             $this->session->set_flashdata('lmsg', 'User profile updated succesfully!');
    }
    function insert_edit_inst_user($username,$mobile,$city,$dob,$role,$assign_course,$email,$user_id,$id,$today){
        $this->db->query("UPDATE users SET  username = '".$username."' , mobile = '".$mobile."' , dob = '".$dob."' , city = '".$city."' , role = '".$role."' , created_datetime = '".$today."' , created_by = '".$email."'   WHERE id = '".$id."' ");
        
        if($this->db->trans_status()){
           $this->session->set_flashdata('lmsg', 'User updated succesfully!');
               
        }else{
            $this->session->set_flashdata('lmsg', 'Error in updating user!');
        }
        $query = $this->db->query("SELECT * FROM course_instructor WHERE course_id = '".$assign_course."' AND instructor_id = '".$id."' ");
        if ($query->num_rows() > 0) {
            $this->session->set_flashdata('lmsg', 'Course is already assigned to this User!');
        } else {
            $data = array(
            	'instructor_id' => $id,
            	'course_id' => $assign_course,
            	'status' => '1',
            	'created_by' => $user_id,
            	'created_date' => $today
            	);
            $this->db->insert('course_instructor',$data);
        }
    }
    
    function update_pass($id,$new_pass,$conf_pass){
         if($new_pass == $conf_pass){
              
              $data = array(
            	'password' => md5($new_pass)
            	);
            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update('users');
            /*print_r($data);
            exit;*/
              
			$this->session->set_flashdata('lmsg', 'Password has been Changed Successfully.'); //set success msg
             return true;
          }else{
               //set success msg
              return false; 
          }

    }
    function insert_edit_l_user($username,$mobile,$city,$dob,$email,$id,$today){
        $data = array(
            	'username' => $username,
            	
            	'mobile' => $mobile,
            	'dob' => $dob,
            	'city' => $city,
            	'updated_datetime' => $today,
            	'updated_by' => $email
            	);
            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update('users');
             $this->session->set_flashdata('lmsg', 'User updated succesfully!');
    }
    function insert_edit_profile_user($username,$mobile,$city,$dob,$email,$id,$today,$filename){
        $data = array(
            	'username' => $username,
            	'profile_picture' => $filename,
            	'mobile' => $mobile,
            	'dob' => $dob,
            	'city' => $city,
            	'updated_datetime' => $today,
            	'updated_by' => $email
            	);
            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update('users');
             $this->session->set_flashdata('lmsg', 'User updated succesfully!');
    }
    function update_learner_pass($currentpass,$newpass,$confpass,$user_pass,$id){
        $currentpass = md5($currentpass);
        if($confpass == $newpass && $currentpass == $user_pass){
            $data = array(
            	'password' => md5($newpass)
            	);
            $this->db->set($data);
            $this->db->where('id', $id);
            $this->db->update('users');
			$this->session->set_flashdata('lmsg', 'Password has been Changed Successfully.'); 
             return true;
          }else{
               	$this->session->set_flashdata('lmsg', 'credential are not correct.');
              return false; 
          }
        }
        function get_trnasection_history($user_id){
            $query = $this->db->query("SELECT * FROM transactions t LEFT JOIN course c ON t.course_id = c.course_id WHERE t.user_id = '".$user_id."'  ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
    function get_all_device($id){
        //echo $id ;
         $query = $this->db->query("SELECT * FROM user_devices WHERE user_id = '".$id."' AND status = '1' ");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
    }
        
    }
    function block_device($blockid){
       
         $this->db->query("UPDATE user_devices SET status = '2' WHERE id = '".$blockid."' ");
        
        if($this->db->trans_status()){
              $this->session->set_flashdata('lmsg', 'Device Deleted Successfully ! Please Refresh The Page');
            return 1;    
            
        }else{
            $this->session->set_flashdata('lmsg', 'Unable to Block please try again later !');
            return 0;
        }
    }
        

}