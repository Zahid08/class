<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Setting_model extends CI_Model {
	
	
	function payu($key,$salt)
	{
	    $this->db->where('name', 'PayU_MerchantKey');
	    $key = $this->db->update('setting', $key);
	    
	    $this->db->where('name', 'PayU_MerchantSalt');
	    $key = $this->db->update('setting', $salt);
	    return true;
    		
	}
	function credit($credit)
	{
	    $this->db->where('name', 'Credit_Value');
	    $key = $this->db->update('setting', $credit);
	    return true;
    		
	}
	function devices($devices)
	{
	    $this->db->where('name', 'Devices_Value');
	    $key = $this->db->update('setting', $devices);
	    return true;
    		
	}
	function referandEarn($referredCreditsValidity,$maxReferralCount,$referrerCreditsSignup,$referreeCreditsSignup, $referrerCreditsFirstTxn,$referreeCreditsFirstTxn)
	{
	    $this->db->where('name', 'Referred_Credits_Validity');
	    $key = $this->db->update('setting', $referredCreditsValidity);
	    $this->db->where('name', 'Max_Referral_Count');
	    $key = $this->db->update('setting', $maxReferralCount);
	    $this->db->where('name', 'Referrer_Credits_Signup');
	    $key = $this->db->update('setting', $referrerCreditsSignup);
	    $this->db->where('name', 'Referree_Credits_Signup');
	    $key = $this->db->update('setting', $referreeCreditsSignup);
	    $this->db->where('name', 'Referrer_Credits_First_Txn');
	    $key = $this->db->update('setting', $referrerCreditsFirstTxn);
	    $this->db->where('name', 'Referree_Credits_First_Txn');
	    $key = $this->db->update('setting', $referreeCreditsFirstTxn);
	    return true;
    		
	}
	
}	